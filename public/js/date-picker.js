$(function(){
    $('.datetimepicker1').datetimepicker();
    
    
    $('input[name=titulo]').blur(function(){

    });
    $('input[name=inicio_inscripcion]').blur(function(){
        var valor = $(this).val();
        var error = $('#inicio_inscripcion');
        error.removeClass('has-error');
        error.find('span').html('');
        if(! moment(valor).isValid()){
            error.addClass('has-error')
            error.find('span').html('formato invalido de fecha');
             return;
        }
        if( moment().isAfter(valor)) {
            error.addClass('has-error')
            error.find('span').html('La fecha tiene que ser mayor o igual a la actual');
            return;
        }
        var fin_inscripcion =  $('input[name=fin_inscripcion]');
        if( moment(valor).isAfter(fin_inscripcion.val())) {
            error.addClass('has-error')
            error.find('span').html('La fecha tiene que ser menor a la fecha fin de inscripcion');
            return;
        }
        var inicio_examen =  $('input[name=inicio_examen]');
        if( moment(valor).isAfter(inicio_examen.val())) {
            error.addClass('has-error')
            error.find('span').html('La fecha de periodo de inicio de inscripcion tiene que ser menor a la fecha de inicio de examen');
            return;
        }
    });
    $('input[name=fin_inscripcion]').blur(function(){
        var valor = $(this).val();
        var error = $('#fin_inscripcion');
        error.removeClass('has-error');
        error.find('span').html('');
        if(! moment(valor).isValid()){
            error.addClass('has-error')
            error.find('span').html('formato invalido de fecha');
             return;
        }
        if( moment().isAfter(valor)) {
            error.addClass('has-error')
            error.find('span').html('La fecha tiene que ser mayor o igual a la actual');
            return;
        }
        var inicio_inscripcion =  $('input[name=inicio_inscripcion]');
        if( moment(valor).isBefore(inicio_inscripcion.val())) {
            error.addClass('has-error')
            error.find('span').html('La fecha tiene que ser mayor a la fecha inicio de inscripcion');
            return;
        }
        
    });
    
    $('input[name=inicio_examen]').blur(function(){
        var valor = $(this).val();
        var error = $('#inicio_examen');
        error.removeClass('has-error');
        error.find('span').html('');
        if(! moment(valor).isValid()){
            error.addClass('has-error');
            error.find('span').html('formato invalido de fecha');
             return;
        }
        if( moment().isAfter(valor)) {
            error.addClass('has-error')
            error.find('span').html('La fecha tiene que ser mayor o igual a la actual');
            return;
        }
        var fin_examen =  $('input[name=fin_examen]');
        if( moment(valor).isAfter(fin_examen.val())) {
            error.addClass('has-error')
            error.find('span').html('La fecha tiene que ser menor a la fecha fin de examen');
            return;
        }
        var inicio_inscripcion =  $('input[name=inicio_inscripcion]');
        if( moment(valor).isBefore(inicio_inscripcion.val())) {
            error.addClass('has-error')
            error.find('span').html('La fecha tiene que ser mayor a la fecha inicio de inscripcion');
            return;
        }
    });
    
    $('input[name=fin_examen]').blur(function(){
        var valor = $(this).val();
        var error = $('#fin_examen');
        error.removeClass('has-error');
        error.find('span').html('');
        if(! moment(valor).isValid()){
            error.addClass('has-error');
            error.find('span').html('formato invalido de fecha');
            return;
        }
        if( moment().isAfter(valor)) {
            error.addClass('has-error')
            error.find('span').html('La fecha tiene que ser mayor o igual a la actual');
            return;
        }
        var inicio_examen =  $('input[name=inicio_examen]');
        if( moment(valor).isBefore(inicio_examen.val())) {
            error.addClass('has-error')
            error.find('span').html('La fecha tiene que ser mayor a la fecha inicio de examen');
            return;
        }
    });

    
    $('#examen-crear').submit(function(){
        
        if($('#inicio_inscripcion').hasClass('has-error')) {
         return false;      
        }
        if($('#fin_inscripcion').hasClass('has-error') ) {
         return false;      
        }
        if($('#inicio_examen').hasClass('has-error')) {
         return false;      
        }
        if($('#fin_examen').hasClass('has-error')) {
         return false;      
        }
        
       
    });


});