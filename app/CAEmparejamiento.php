<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;
use DB;

class CAEmparejamiento extends Model
{
    protected  $table = 'respuesta_emparejamiento';
    protected $fillable = ['preguntaorder_id','primervalor','segundovalor'];

    public static function getAll_front(){
  		return DB::select("SELECT p.id, t.tipo as tipo, t.alias as alias, p.enunciado, c.categoria, s.categoria as subcategoria, p.created_at 
						   FROM sub_categorias s, preguntaorden p, categorias c, tipos_pregunta t
					   	   WHERE s.id = p.subcategoria_id AND s.categorias_id = c.id AND t.id = p.tipos_pregunta_id");	  		
    }

}
