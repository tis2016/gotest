<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;
use DB;

class SeleccionSimpleOpciones extends Model
{
  protected $table = "seleccion_simple_opciones";
  protected $fillable = ['opcion','correcto','peso','preguntas_seleccion_simple_id'];

  public static function getOpciones($id){
    return DB::table('seleccion_simple_opciones')
           ->select('id','opcion')
           ->where('preguntas_seleccion_simple_id','=',$id)->get();
  }
  public static function getCorrectos($id){
    return DB::table('seleccion_simple_opciones')
           ->select('correcto')
           ->where('preguntas_seleccion_simple_id','=',$id)->get();
  }
  public static function getCorrectos_row($id){
    return DB::select("SELECT opcion
                       FROM seleccion_simple_opciones
                       WHERE correcto = 1 AND preguntas_seleccion_simple_id = ".$id);
  }
}
