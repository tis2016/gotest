<?php

namespace GoTest;
use DB;

use Illuminate\Database\Eloquent\Model;

class RespuestaEmparejamiento extends Model
{
  protected $table = 'correccion_emparejamiento';

  protected $fillable = ['primervalor_respuesta','segundovalor_respuesta','preguntaorden_id'];

  public function preguntaEmparejamiento(){

  return $this->belongsTo('GoTest\PreguntaOrden');
  }
  public static function ultimasRepuestas($id, $cantidad){
     
    return DB::select('SELECT primervalor_respuesta, segundovalor_respuesta,peso,respondida,correcto
                      FROM correccion_emparejamiento
                      WHERE preguntaorden_id ='.$id.
                      ' ORDER BY created_at desc LIMIT '.$cantidad);
  }
}
