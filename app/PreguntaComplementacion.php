<?php

namespace GoTest;
use DB;
use Illuminate\Database\Eloquent\Model;

class PreguntaComplementacion extends Model
{
    protected $table = 'pregunta_complementacion';
  protected $fillable = ['enunciado','subcategoria_id','tipos_pregunta_id','usuarios_id'];
    public function respuestas() {
        return $this->hasMany('GoTest\RespuestaComplementacion', 'pregunta_complementacion_id');
    }

    public function subcategoria(){
        return $this->belongsTo('GoTest\SubCategoria');
    }

    public static function getAll_front(){
		return DB::select("SELECT p.id, t.tipo as tipo, t.alias as alias, p.enunciado, c.categoria, s.categoria as subcategoria, p.created_at 
					   FROM sub_categorias s, pregunta_complementacion p, categorias c, tipos_pregunta t
				   	   WHERE s.id = p.subcategoria_id AND s.categorias_id = c.id AND t.id = p.tipos_pregunta_id");	  		
    }
      public static function getRows_count(){
        return DB::table("pregunta_complementacion")->count();         
    }
}
