<?php

namespace GoTest;
use DB;
use Illuminate\Database\Eloquent\Model;

class PreguntaSeleccionMultiple extends Model
{
  protected $table = "preguntas_seleccion_multiple";
  protected $fillable = ['enunciado','categorias_id','sub_categorias_id','usuarios_id'];

  public static function getAll(){
    return DB::table('preguntas_seleccion_multiple')
          ->join('categorias','preguntas_seleccion_multiple.categorias_id','=','categorias.id')
          ->join('sub_categorias','preguntas_seleccion_multiple.sub_categorias_id','=','sub_categorias.id')
          //->join('sub_categorias','sub_categorias.categorias_id','=','categorias.id')
          ->select('preguntas_seleccion_multiple.id','preguntas_seleccion_multiple.enunciado','categorias.categoria',
                   'sub_categorias.categoria as subcategoria','preguntas_seleccion_multiple.created_at')
          ->paginate(5);
  }
  public static function getAll_front(){
    return DB::table('preguntas_seleccion_multiple')
          ->join('categorias','preguntas_seleccion_multiple.categorias_id','=','categorias.id')
          ->join('sub_categorias','preguntas_seleccion_multiple.sub_categorias_id','=','sub_categorias.id')
          //->join('sub_categorias','sub_categorias.categorias_id','=','categorias.id')
          ->select('preguntas_seleccion_multiple.id','preguntas_seleccion_multiple.tipo_pregunta','preguntas_seleccion_multiple.enunciado','categorias.categoria',
                   'sub_categorias.categoria as subcategoria','preguntas_seleccion_multiple.created_at')
          ->get();
  }
  public function opciones(){
    return $this->hasMany('GoTest\SeleccionMultipleOpciones', 'preguntas_seleccion_multiple_id');
  }
  public function respuestasSelMultiple(){
    return $this->hasMany('GoTest\RespuestaSelMultiple', 'preguntas_seleccion_multiple_id');
  }
  public static function obtenerSubcategoria($id){
    return DB::select('SELECT s.categoria as subcategoria
                      FROM preguntas_seleccion_multiple p, sub_categorias s
                      WHERE p.sub_categorias_id = s.id AND p.id='.$id);

  }
  public static function obtenerCategoria($id){
    return DB::table('preguntas_seleccion_multiple')
              ->join('categorias', 'categorias.id','=','preguntas_seleccion_multiple.categorias_id')
              ->select('categorias.categoria')
              ->where('preguntas_seleccion_multiple.id', '=', $id)
              ->get();
 }
  public static function getRows_count(){
        return DB::table("preguntas_seleccion_multiple")->count();         
    }
    public static function getAll_front1(){
      return DB::select("SELECT p.id, t.tipo, t.alias, p.enunciado, c.categoria, s.categoria as subcategoria, p.created_at 
               FROM sub_categorias s, preguntas_seleccion_multiple p, categorias c, tipos_pregunta t
                 WHERE s.id = p.sub_categorias_id AND s.categorias_id = c.id AND t.id = p.tipos_pregunta_id");        
  }
}
