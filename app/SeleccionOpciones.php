<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;
use DB;

class SeleccionOpciones extends Model
{
  protected $table = "seleccion_opciones";
  protected $fillable = ['opcion','correcto','peso','preguntas_seleccion_id'];

  public static function getOpciones($id){
    return DB::table('seleccion_opciones')
           ->select('id','opcion')
           ->where('preguntas_seleccion_id','=',$id)->get();
  }
  public static function getCorrectos($id){
    return DB::table('seleccion_opciones')
           ->select('correcto')
           ->where('preguntas_seleccion_id','=',$id)->get();
  }
}
