<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;
use DB;

class RespuestaOrdenamiento extends Model
{
    protected $table = "respuestas_ordenamiento";
	protected $fillable = [
	  	'respuesta',
	  	'peso',
	  	'preguntas_ordenamiento_id'
	  ];
}
