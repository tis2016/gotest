<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;
use DB;

class FalsoVerdaderoOpciones extends Model
{
  protected $table = "falso_verdadero_opciones";
  protected $fillable = ['opcion','correcto','peso','preguntas_falso_verdadero_id'];

  public static function getOpciones($id){
    return DB::table('falso_verdadero_opciones')
           ->select('id','opcion')
           ->where('preguntas_falso_verdadero_id','=',$id)->get();
  }
  public static function getCorrectos($id){
    return DB::table('falso_verdadero_opciones')
           ->select('correcto')
           ->where('preguntas_falso_verdadero_id','=',$id)->get();
  }
  public static function getCorrectos_row($id){
    return DB::select("SELECT opcion
                       FROM falso_verdadero_opciones
                       WHERE correcto = 1 AND preguntas_falso_verdadero_id = ".$id);
  }
}
