<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;

class TipoPregunta extends Model
{
    protected $table = 'tipos_pregunta';
}
