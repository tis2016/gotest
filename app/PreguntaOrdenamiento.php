<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;
use DB;
class PreguntaOrdenamiento extends Model
{
  protected $table = "preguntas_ordenamiento";
  protected $fillable = ['enunciado','categorias_id','sub_categorias_id','usuarios_id'];

  public static function getAll(){
    return DB::table('preguntas_ordenamiento')
          ->join('categorias','preguntas_ordenamiento.categorias_id','=','categorias.id')
          ->join('sub_categorias','preguntas_ordenamiento.sub_categorias_id','=','sub_categorias.id')
          //->join('sub_categorias','sub_categorias.categorias_id','=','categorias.id')
          ->select('preguntas_ordenamiento.id','preguntas_ordenamiento.enunciado','categorias.categoria',
                   'sub_categorias.categoria as subcategoria','preguntas_ordenamiento.created_at')
          ->paginate(5);
  }
  public static function getAll_front(){
    return DB::table('preguntas_ordenamiento')
          ->join('categorias','preguntas_ordenamiento.categorias_id','=','categorias.id')
          ->join('sub_categorias','preguntas_ordenamiento.sub_categorias_id','=','sub_categorias.id')
          ->join('tipos_pregunta','preguntas_ordenamiento.tipos_pregunta_id','=','tipos_pregunta.id')
          //->join('sub_categorias','sub_categorias.categorias_id','=','categorias.id')
          ->select('preguntas_ordenamiento.id','tipos_pregunta.tipo','tipos_pregunta.alias','preguntas_ordenamiento.enunciado','categorias.categoria',
                   'sub_categorias.categoria as subcategoria','preguntas_ordenamiento.created_at')
          ->get();
  }
  
  public static function getPregunta_detalles($id){
    return DB::select('SELECT p.id,p.enunciado, c.categoria, s.categoria as subcategoria, p.created_at, p.updated_at
                      FROM sub_categorias s, preguntas_ordenamiento p, categorias c
                      WHERE p.categorias_id = c.id AND p.sub_categorias_id = s.id AND s.categorias_id = c.id AND p.id = '.$id);
  }
    public static function getRows_count(){
        return DB::table("preguntas_ordenamiento")->count();         
    }
}
