<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;

class RespuestaPreguntaAbierta extends Model
{
      protected $table = "respuestas_pregunta_abierta";
	  protected $fillable = [
	  	'respuesta',
	  	'peso',
	  	'preguntas_abiertas_id'
	  ];
}
