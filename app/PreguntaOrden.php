<?php

namespace GoTest;
use DB;
use Illuminate\Database\Eloquent\Model;

class PreguntaOrden extends Model
{
    protected $table = 'preguntaorden';
    protected $fillable = ['enunciado','subcategoria_id','tipos_pregunta_id','usuarios_id'];

    public function respuestas() {
        return $this->hasMany('GoTest\RespuestaOrden', 'preguntaorden_id');
    }
    //roger
    public function subcategoria(){
        return $this->belongsTo('GoTest\SubCategoria');
    }

    public function categoria(){
      return $this->belongsTo('GoTest\Categoria', 'categorias_id');
    }

    public function respuestasEscogidas(){
        return $this->hasMany('GoTest\RespuestaEmparejamiento', 'preguntaorden_id');
    }
    public static function obtenerSubcategoria($id){
      return DB::select('SELECT s.categoria as subcategoria
                        FROM preguntaorden p, sub_categorias s
                        WHERE p.subcategoria_id = s.id AND p.id='.$id);

    }

    public static function obtenerCategoria($id){
      return DB::table('preguntaorden')
                ->join('sub_categorias', 'preguntaorden.subcategoria_id','=','sub_categorias.id')
                ->join('categorias', 'categorias.id','=','sub_categorias.categorias_id')
                ->select('categorias.categoria')
                ->where('preguntaorden.id', '=', $id)
                ->get();
    }
    //roger
    public static function getAll_front(){
    return DB::table('preguntaorden')
          ->join('sub_categorias','preguntas_falso_verdadero.sub_categorias_id','=','sub_categorias.id')
          //->join('sub_categorias','sub_categorias.categorias_id','=','categorias.id')
          ->join('tipos_pregunta','preguntas_falso_verdadero.tipos_pregunta_id','=','tipos_pregunta.id')
          ->select('preguntas_falso_verdadero.id','tipos_pregunta.tipo','tipos_pregunta.alias','preguntas_falso_verdadero.enunciado','categorias.categoria',
                   'sub_categorias.categoria as subcategoria','preguntas_falso_verdadero.created_at')
          ->get();
  }
    public static function getRows_count(){
        return DB::table("preguntaorden")->count();         
    }
}
