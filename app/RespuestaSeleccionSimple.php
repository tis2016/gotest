<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;

class RespuestaSeleccionSimple extends Model
{
      protected $table = "respuestas_sel_simple";
	  protected $fillable = [
	  	'opcion',
	  	'correcto',
	  	'peso',
	  	'preguntas_seleccion_simple_id'
	  ];
	  
}
