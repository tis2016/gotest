<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
  protected $table = "categorias";
  protected $fillable = ['categoria','obs'];

  public function subcategorias() {
        return $this->hasMany('GoTest\SubCategoria', 'categorias_id');
  }
}
