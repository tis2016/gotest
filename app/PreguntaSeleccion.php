<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;
use DB;
//
class PreguntaSeleccion extends Model
{
  protected $table = "preguntas_seleccion";
  protected $fillable = ['tipo','enunciado','seleccion_opciones_id','categorias_id','sub_categorias_id','usuarios_id'];

  public static function getAll(){
    return DB::table('preguntas_seleccion')
          ->join('categorias','preguntas_seleccion.categorias_id','=','categorias.id')
          ->join('sub_categorias','preguntas_seleccion.sub_categorias_id','=','sub_categorias.id')
          //->join('sub_categorias','sub_categorias.categorias_id','=','categorias.id')
          ->select('preguntas_seleccion.id','preguntas_seleccion.enunciado','categorias.categoria',
                   'sub_categorias.categoria as subcategoria','preguntas_seleccion.created_at')
          ->paginate(2);
  }
  public function opciones(){
    return $this->hasMany('GoTest\SeleccionOpciones', 'preguntas_seleccion_id');
  }
  public function respuestasSelMultiple(){
    return $this->hasMany('GoTest\RespuestaSelMultiple', 'preguntas_seleccion_id');
  }
}
