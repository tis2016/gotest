<?php

namespace GoTest;
use DB;
use Illuminate\Database\Eloquent\Model;

class RespuestaSelMultiple extends Model
{
    protected $table = 'respuesta_sel_multiple';

    protected $fillable = ['respuesta','preguntas_seleccion_multiple_id'];

    public function preguntaSeleccion(){

    return $this->belongsTo('GoTest\PreguntaSeleccionMultiple');
  }
  public static function ultimasRepuestasSeleccionadas($id, $cantidad){

    return DB::select('SELECT respuesta, correcto,peso
                      FROM respuesta_sel_multiple
                      WHERE preguntas_seleccion_multiple_id ='.$id.
                      ' ORDER BY created_at desc LIMIT '.$cantidad);
  }
}
