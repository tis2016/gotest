<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;
use DB;

class PreguntaFalsoVerdadero extends Model
{
  protected $table = "preguntas_falso_verdadero";
  protected $fillable = ['enunciado','categorias_id','sub_categorias_id','tipos_pregunta_id','usuarios_id'];

  public static function getAll(){
    return DB::table('preguntas_falso_verdadero')
          ->join('categorias','preguntas_falso_verdadero.categorias_id','=','categorias.id')
          ->join('sub_categorias','preguntas_falso_verdadero.sub_categorias_id','=','sub_categorias.id')
          //->join('sub_categorias','sub_categorias.categorias_id','=','categorias.id')
          ->select('preguntas_falso_verdadero.id','preguntas_falso_verdadero.enunciado','categorias.categoria',
                   'sub_categorias.categoria as subcategoria','preguntas_falso_verdadero.created_at')
          ->paginate(5);
  }


  public static function getAll_front(){
    return DB::select('SELECT p.id,p.enunciado, c.categoria, s.categoria as subcategoria, p.created_at, t.tipo, t.alias
                       FROM sub_categorias s, preguntas_falso_verdadero p, categorias c, tipos_pregunta t
                       WHERE p.categorias_id = c.id AND p.sub_categorias_id = s.id 
                       AND s.categorias_id = c.id AND p.tipos_pregunta_id = t.id');
  }

  public static function getPregunta_detalles($id){
    return DB::select('SELECT p.id,p.enunciado, c.categoria, s.categoria as subcategoria, p.created_at, p.updated_at
                      FROM sub_categorias s, preguntas_falso_verdadero p, categorias c
                      WHERE p.categorias_id = c.id AND p.sub_categorias_id = s.id AND s.categorias_id = c.id AND p.id = '.$id);
  }

  public static function getOpciones_detalles($id){
    return DB::select('SELECT f.opcion, f.correcto, f.peso
                       FROM falso_verdadero_opciones f, preguntas_falso_verdadero p
                       WHERE f.preguntas_falso_verdadero_id = p.id AND p.id = '.$id);
  }
    public static function getRows_count(){
        return DB::table("preguntas_falso_verdadero")->count();         
    }

}
