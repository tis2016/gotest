<?php

namespace GoTest;
use DB;
use Illuminate\Database\Eloquent\Model;

class Examen extends Model
{
    protected $table='examenes';
    protected $fillable = array(
    	'titulo',
    	'materia',
    	'inicio_inscripcion',
    	'fin_inscripcion',
    	'inicio_examen',
    	'fin_examen',
    	'porcentaje_total',
    	'valor_total',
    	'numero_intentos',
        'usuarios_id'
	);
    public static $reglas = array(
        'materia'=>'required|min:5|max:255',
    	'titulo' => 'required|min:5|max:255',
    	'inicio_inscripcion' => 'required',
    	'fin_inscripcion' => 'required',
    	'inicio_examen' => 'required',
    	'fin_examen' => 'required',
    	'porcentaje_total' => 'required',
    	'valor_total' => 'required',
    	'numero_intentos' => 'required'
	);

	public static $mensajes = array(
		'required' => 'El campo :attribute es requerido'
	);
    public static function getRows_count(){
        return DB::table("examenes")->count();         
    }
}
