<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    protected $table = 'preguntas';
    public function tipo() {
        return $this->belongsTo('GoTest\TipoPregunta', 'id_tipo_pregunta');
    }
}
