<?php
use Illuminate\Http\Request;
use GoTest\PreguntaOrden;
use GoTest\RespuestaOrden;
use GoTest\PreguntaComplementacion;
use GoTest\RespuestaComplementacion;
use GoTest\Categoria;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Mauricio Trigo
Route::resource('/', 'HomeController');
Route::get('/home','HomeController@home');
Route::resource('pregunta','PreguntaFrontController');
Route::get('pregunta/subcategoria/{subcategoria}','CategoriaController@getSubCategorias');


Route::resource('abierta','PreguntaAbiertaController');
Route::get('abierta/subcategoria/{subcategoria}','CategoriaController@getSubCategorias');
Route::get('abierta/{abierta}/subcategoria/{subcategoria}','CategoriaController@getSubCategorias2');
Route::get('abierta/{abierta}/destroy',[
  'uses'=>'PreguntaAbiertaController@destroy',
  'as' => 'abierta.destroy'
]);

Route::get('subcategoria/{subcategoria}','CategoriaController@getSubCategorias');

Route::resource('seleccion-simple','PreguntaSeleccionSimpleController');
Route::get('seleccion-simple/subcategoria/{subcategoria}','CategoriaController@getSubCategorias');
Route::get('seleccion-simple/{seleccion}/subcategoria/{subcategoria}','CategoriaController@getSubCategorias2');
Route::get('seleccion-simple/{seleccion}/destroy',[
  'uses'=>'PreguntaSeleccionSimpleController@destroy',
  'as' => 'seleccion-simple.destroy'
]);

Route::resource('falso-verdadero','PreguntaFalsoVerdaderoController');
Route::get('falso-verdadero/subcategoria/{subcategoria}','CategoriaController@getSubCategorias');
Route::get('falso-verdadero/{falsoverdadero}/subcategoria/{subcategoria}','CategoriaController@getSubCategorias2');
Route::get('falso-verdadero/{falsoverdadero}/destroy',[
  'uses'=>'PreguntaFalsoVerdaderoController@destroy',
  'as' => 'falso-verdadero.destroy'
]);

Route::resource('opciones','SeleccionSimpleOpcionesController');

Route::resource('ordenamiento','PreguntaOrdenamientoController');
Route::get('ordenamiento/{ordenamiento}/edit',[
  'uses'=>'PreguntaOrdenamientoController@edit',
  'as' => 'ordenamiento.edit'
]);
Route::get('ordenamiento/{ordenamiento}/subcategoria/{subcategoria}','CategoriaController@getSubCategorias2');
Route::get('ordenamiento/subcategoria/{subcategoria}','CategoriaController@getSubCategorias');
Route::get('ordenamiento/{ordenamiento}/destroy',[
  'uses'=>'PreguntaOrdenamientoController@destroy',
  'as' => 'ordenamiento.destroy'
]);
Route::resource('opcionesorden','OrdenamientoOpcionesController');

Route::resource('preguntas/listar','PreguntaFrontController');

Route::resource('seleccion-multiple','PreguntaSeleccionMultipleController');
Route::get('seleccion-multiple/subcategoria/{subcategoria}','CategoriaController@getSubCategorias');
Route::get('seleccion-multiple/{seleccion}/subcategoria/{subcategoria}','CategoriaController@getSubCategorias2');
Route::get('seleccion-multiple/{seleccion}/destroy',[
  'uses'=>'PreguntaSeleccionMultipleController@destroy',
  'as' => 'seleccion-multiple.destroy'
]);

Route::resource('opciones-multiple','SeleccionMultipleOpcionesController');

Route::resource('correccion-falso-verdadero','RespuestasFalsoVerdaderoController');
Route::get('correccion-falso-verdadero/subcategoria/{subcategoria}','CategoriaController@getSubCategorias');
Route::get('correccion-falso-verdadero/{seleccion}/subcategoria/{subcategoria}','CategoriaController@getSubCategorias2');
Route::get('correccion-falso-verdadero/{seleccion}/destroy',[
  'uses'=>'RespuestasFalsoVerdaderoController@destroy',
  'as' => 'correccion-falso-verdadero.destroy'
]);

Route::resource('correccion-abiertas','RespuestaPreguntaAbiertaController');
Route::get('correccion-abiertas/subcategoria/{subcategoria}','CategoriaController@getSubCategorias');
Route::get('correccion-abiertas/{seleccion}/subcategoria/{subcategoria}','CategoriaController@getSubCategorias2');
Route::get('correccion-abiertas/{seleccion}/destroy',[
  'uses'=>'RespuestaPreguntaAbiertaController@destroy',
  'as' => 'correccion-abiertas.destroy'
]);

Route::resource('correccion-sel-simple','RespuestaSeleccionSimpleController');
Route::get('correccion-sel-simple/subcategoria/{subcategoria}','CategoriaController@getSubCategorias');
Route::get('correccion-sel-simple/{seleccion}/subcategoria/{subcategoria}','CategoriaController@getSubCategorias2');
Route::get('correccion-sel-simple/{seleccion}/destroy',[
  'uses'=>'RespuestaSeleccionSimpleController@destroy',
  'as' => 'correccion-sel-simple.destroy'
]);

Route::resource('correccion-ordenamiento','RespuestaOrdenamientoController');
Route::get('correccion-ordenamiento/subcategoria/{subcategoria}','CategoriaController@getSubCategorias');
Route::get('correccion-ordenamiento/{seleccion}/subcategoria/{subcategoria}','CategoriaController@getSubCategorias2');
Route::get('correccion-ordenamiento/{seleccion}/destroy',[
  'uses'=>'RespuestaOrdenamientoController@destroy',
  'as' => 'correccion-ordenamiento.destroy'
]);
Route::resource('usuarios','UsuarioController');

Route::resource('login','LoginController');
Route::get('logout','LoginController@logout');
//Mau Trigo

//Roger
Route::resource('respuestaSeleccion','RespuestaSelMultipleController');
Route::get('respuestaSelecion/{id}/create',[
  'uses' => 'RespuestaSelMultipleController@create',
  'as'   => 'respuestaSeleccion.create'
]);
Route::resource('resolver-emparejamiento','CorreccionEmparejamientoController');
Route::get('resolver-emparejamiento/{id}/create',[
  'uses' => 'CorreccionEmparejamientoController@create',
  'as'   => 'resolver-emparejamiento.create'
]);
//Roger

//Daniela
// complementacion

Route::get('/pregunta-complementacion', function(){
    $preguntas = PreguntaComplementacion::all();
    return view('preguntacomplementacion.listar', array(
        'preguntas' => $preguntas
    ));

});

Route::get('/crear-pregunta-complementacion', function( ){
    $categorias = Categoria::all();
     return view('preguntacomplementacion.create', array(
        'categorias' => $categorias
    ));
});

Route::post('/crear-pregunta-complementacion', function(Request $request){
    $rules = array(
        'enunciado' => 'required|max:255|min:6',
    );
    $mensajes = array(
        'required' => 'El campo :attribute es requerido',
        'min' => 'El campo :attribute debe ser mayor a 6 caracteres'

    );
    $validator = Validator::make($request->all(), $rules, $mensajes);
    if($validator->fails()) {
      return redirect('/crear-pregunta-complementacion')
        ->withInput()
        ->withErrors($validator);
    }
    $respuestas = $request->respuestas;
    $rules = array(
        'palabra clave' => 'required',
    );
    for ($i = 0; $i < count($respuestas) ; $i++) {
        $valores = array(
            'palabra clave' => $respuestas[$i],
        );
        $validator = Validator::make($valores, $rules, $mensajes);
        if($validator->fails()) {
            return redirect('/crear-pregunta-complementacion')
            ->withInput()
            ->withErrors($validator);
        }
    }

    $pregunta = new PreguntaComplementacion;
    $pregunta->enunciado = $request->enunciado;
    $pregunta->usuarios_id = $request->usuarios_id;
    if($request->subcategoria) {
        $pregunta->subcategoria_id = $request->subcategoria;
    } else {
        $pregunta->subcategoria_id = null;
    }
    //$pregunta->subcategoria_id = $request->subcategoria;
    $pregunta->save();
    for ($i = 0; $i < count($respuestas) ; $i++) {
        $respuesta = new RespuestaComplementacion;
        $respuesta->respuesta = $respuestas[$i];
        $respuesta->pregunta_complementacion_id = $pregunta->id;
        $respuesta->save();
    }
    return redirect('/ver-pregunta-complementacion/' . $pregunta->id);
});
Route::get('/editar-pregunta-complementacion/{pregunta}', function( PreguntaComplementacion $pregunta ){
    $categorias = Categoria::all();
    return view('preguntacomplementacion.editar', array(
        'pregunta' => $pregunta,
        'categorias' => $categorias
    ));
});

Route::post('/editar-pregunta-complementacion/{pregunta}', function(Request $request, PreguntaComplementacion $pregunta){
    $rules = array(
        'enunciado' => 'required|max:255|min:6',
    );
    $mensajes = array(
        'required' => 'El campo :attribute es requerido',
        'min' => 'El campo :attribute debe ser mayor a 6 caracteres'

    );
    $validator = Validator::make($request->all(), $rules, $mensajes);
    if($validator->fails()) {
      return redirect('/editar-pregunta-complementacion' . $pregunta->id)
        ->withInput()
        ->withErrors($validator);
    }
    $respuestas = $request->respuestas;
    $rules = array(
        'palabra clave' => 'required',
    );
    for ($i = 0; $i < count($respuestas) ; $i++) {
        $valores = array(
            'palabra clave' => $respuestas[$i],
        );
        $validator = Validator::make($valores, $rules, $mensajes);
        if($validator->fails()) {
            return redirect('/editar-pregunta-complementacion' . $pregunta->id)
            ->withInput()
            ->withErrors($validator);
        }
    }

    $pregunta->enunciado = $request->enunciado;
    //$pregunta->subcategoria_id = $request->subcategoria;
    if($request->subcategoria) {
        $pregunta->subcategoria_id = $request->subcategoria;
    } else {
        $pregunta->subcategoria_id = null;
    }
    $pregunta->save();
    foreach ($pregunta->respuestas as $respuesta) {
        $respuesta->delete();
    }

    for ($i = 0; $i < count($respuestas) ; $i++) {
        $respuesta = new RespuestaComplementacion;
        $respuesta->respuesta = $respuestas[$i];
        $respuesta->pregunta_complementacion_id = $pregunta->id;
        $respuesta->save();
    }
    return redirect('/ver-pregunta-complementacion/' . $pregunta->id);
});
Route::get('/ver-pregunta-complementacion/{pregunta}', function(PreguntaComplementacion $pregunta){
    return view('preguntacomplementacion.ver', array(
        'pregunta' => $pregunta,
        'respuestas' => $pregunta->respuestas,
    ));
});

Route::get('/eliminar-pregunta-complementacion/{pregunta}', function(PreguntaComplementacion $pregunta){
    foreach ($pregunta->respuestas as $respuesta) {
        $respuesta->delete();
    }
    $pregunta->delete();
    return redirect('/pregunta-complementacion');

});

Route::get('/pregunta-emparejamiento', function(){
    $preguntas = PreguntaOrden::all();
    return view('preguntaorden.listar', array(
        'preguntas' => $preguntas
    ));

});

Route::get('/crear-pregunta-orden', function( ){
    $categorias = Categoria::all();
    return view('preguntaorden.create', array(
        'categorias' => $categorias
    ));
});

Route::post('/crear-pregunta-orden', function(Request $request){
    $rules = array(
        'enunciado' => 'required|max:255|min:6',

    );
    $mensajes = array(
        'required' => 'El campo :attribute es requerido',
        'min' => 'El campo :attribute debe ser mayor a 6 caracteres'

    );
    $validator = Validator::make($request->all(), $rules, $mensajes);
    if($validator->fails()) {
      return redirect('/crear-pregunta-orden')
        ->withInput()
        ->withErrors($validator);
    }
    $primerValor = $request->primervalor;
    $segundoValor = $request->segundovalor;
    $rules = array(
        'primer valor' => 'required',
        'segundo valor' => 'required',
    );
    for ($i = 0; $i < count($primerValor) ; $i++) {
        $valores = array(
            'primer valor' => $primerValor[$i],
            'segundo valor' => $segundoValor[$i]
        );
        $validator = Validator::make($valores, $rules, $mensajes);
        if($validator->fails()) {
            return redirect('/crear-pregunta-orden')
            ->withInput()
            ->withErrors($validator);
        }
    }

    $pregunta = new PreguntaOrden;
    $pregunta->enunciado = $request->enunciado;
    $pregunta->usuarios_id = $request->usuarios_id;
    if($request->subcategoria) {
        $pregunta->subcategoria_id = $request->subcategoria;
    }
    $pregunta->save();
    for ($i = 0; $i < count($primerValor) ; $i++) {
        $respuesta = new RespuestaOrden;
        $respuesta->primervalor = $primerValor[$i];
        $respuesta->segundovalor = $segundoValor[$i];
        $respuesta->preguntaorden_id = $pregunta->id;
        $respuesta->save();
    }
    return redirect('/ver-pregunta-orden/' . $pregunta->id);
});
Route::get('/ver-pregunta-orden/{pregunta}', function(PreguntaOrden $pregunta){
    return view('preguntaorden.ver', array(
        'pregunta' => $pregunta,
        'respuestas' => $pregunta->respuestas,
    ));
});

Route::get('/eliminar-pregunta-emparejamiento/{pregunta}', function(PreguntaOrden $pregunta){
    foreach ($pregunta->respuestas as $respuesta) {
        $respuesta->delete();
    }
    $pregunta->delete();
    return redirect('/pregunta-emparejamiento');

});

Route::get('/editar-pregunta-emparejamiento/{pregunta}', function( PreguntaOrden $pregunta ){
    $categorias = Categoria::all();
    return view('preguntaorden.editar' , array(
        'pregunta' => $pregunta,
        'categorias' => $categorias
    ));
});

Route::post('/editar-pregunta-emparejamiento/{pregunta}', function(Request $request, PreguntaOrden $pregunta){
    $rules = array(
        'enunciado' => 'required|max:255|min:6',
    );
    $mensajes = array(
        'required' => 'El campo :attribute es requerido',
        'min' => 'El campo :attribute debe ser mayor a 6 caracteres'
    );
    $validator = Validator::make($request->all(), $rules, $mensajes);
    if($validator->fails()) {
      return redirect('/editar-pregunta-emparejamiento/'. $pregunta->id)
        ->withInput()
        ->withErrors($validator);
    }
    $primerValor = $request->primervalor;
    $segundoValor = $request->segundovalor;
    $rules = array(
        'primer valor' => 'required',
        'segundo valor' => 'required',
    );
    for ($i = 0; $i < count($primerValor) ; $i++) {
        $valores = array(
            'primer valor' => $primerValor[$i],
            'segundo valor' => $segundoValor[$i]
        );
        $validator = Validator::make($valores, $rules, $mensajes);
        if($validator->fails()) {
            return redirect('/editar-pregunta-emparejamiento/' . $pregunta->id )
            ->withInput()
            ->withErrors($validator);
        }
    }

    $pregunta->enunciado = $request->enunciado;
    if($request->subcategoria) {
        $pregunta->subcategoria_id = $request->subcategoria;
    } else {
        $pregunta->subcategoria_id = null;
    }
    $pregunta->save();

    foreach ($pregunta->respuestas as $respuesta) {
        $respuesta->delete();
    }

    for ($i = 0; $i < count($primerValor) ; $i++) {
        $respuesta = new RespuestaOrden;
        $respuesta->primervalor = $primerValor[$i];
        $respuesta->segundovalor = $segundoValor[$i];
        $respuesta->preguntaorden_id = $pregunta->id;
        $respuesta->save();
    }
    return redirect('/ver-pregunta-orden/' . $pregunta->id);

});

Route::get('/examen/crear', function(){
    $examenes = PreguntaOrden::all();
    return view('preguntaorden.listar', array(
        'preguntas' => $preguntas
    ));
});


Route::controller('/preguntas', 'PreguntasController');
Route::controller('/examenes', 'ExamenesController');
Route::controller('/correccion', 'CoreccionController');
Route::get('/subcategorias/{categoria}', function(Categoria $categoria) {
   return response()->json($categoria->subcategorias); 
});
Route::controller('/resolver','ResolverController');

//Daniela

//Ricardo
Route::get('/correccionEmp','ca_controller@index');
Route::get('/agregar-pregunta','PreguntasExamenController@index'); 
Route::post('/agregar-pregunta','PreguntasExamenController@post');
//Ricardo
