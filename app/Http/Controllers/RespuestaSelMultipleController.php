<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;


use GoTest\Http\Requests;
use GoTest\PreguntaSeleccion;
use GoTest\RespuestaSelMultiple;
use GoTest\SeleccionMultipleOpciones;
use GoTest\PreguntaSeleccionMultiple;
use DB;

class RespuestaSelMultipleController extends Controller
{
  public function index()
  {
    //return view('preguntaseleccionmultiple.correccion.resultados', ['pregunta' => $pregunta]);

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create($id)
  {
      $pregunta = PreguntaSeleccionMultiple::find($id);
      //$fila = DB::table('preguntas_seleccion')->select('select * from users where id = :id', ['id' => $id]);
      return view('preguntaseleccionmultiple.correccion.show',['pregunta'=>$pregunta]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

          $respuestas = $request->respuesta;

          $preguntaAux= PreguntaSeleccionMultiple::find($request->idPregunta);
          $opcionesPregunta = $preguntaAux->opciones;
          $contadorCorrectos = 0;
          //este ciclo es el contador de correcto para prorratar el peso
          for ($i=0; $i < count($opcionesPregunta); $i++) {
              if($opcionesPregunta[$i]->correcto==1){
                $contadorCorrectos ++;
              }
          }
          if($respuestas == null){
            $respuesta = new RespuestaSelMultiple();
            $respuesta->opcion_escogida_id = -1;
            $respuesta->preguntas_seleccion_multiple_id = $request->idPregunta;
            $respuesta->peso = 0;
            $respuesta->respuesta = "sin respuesta";
            $respuesta->correcto = 0;
            $respuesta->save();
          }else{
                for($i = 0; $i< count($respuestas); $i++){
                  //si no escogio nada el usuario

                      $respuesta = new RespuestaSelMultiple();
                      $respuesta->opcion_escogida_id = $respuestas[$i];
                      //busca la opcion de la tabla de SeleccionOpciones
                      $opcion = SeleccionMultipleOpciones::find($respuestas[$i]);
                      $respuesta->respuesta = $opcion->opcion;

                      $respuesta->preguntas_seleccion_multiple_id = $request->idPregunta;
                      //prorratear peso
                      if($opcion->correcto==0){
                        $respuesta->peso = 0;
                      }else{
                      $respuesta->peso = 100/$contadorCorrectos;
                     }
                      //asigna si el respues es correcta o incorrecta segun la tabla de seleccion_opciones
                      $respuesta->correcto = $opcion->correcto;
                      $respuesta->save();
                      $aux[$i] = $respuesta->id;

                }
              }
          if($respuestas == null){
            $ultimasRespuetas = RespuestaSelMultiple::ultimasRepuestasSeleccionadas($request->idPregunta,1);
          }
          else{
            $ultimasRespuetas = RespuestaSelMultiple::ultimasRepuestasSeleccionadas($request->idPregunta,count($respuestas));
          }

          $pregunta = PreguntaSeleccionMultiple::find($request->idPregunta);

          $categoria = $pregunta->obtenerCategoria($request->idPregunta);
          $subcategoria = $pregunta->obtenerSubcategoria($request->idPregunta);


          return view('preguntaseleccionmultiple.correccion.resultados', ['pregunta' => $pregunta,'ultimasRespuestas'=>$ultimasRespuetas,'categoria'=>$categoria,'subcategoria'=>$subcategoria]);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //$respuesta = RespuestaSelMultiple::find($id);

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {

  }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {

  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //eliminacion en cascada

  }
}
