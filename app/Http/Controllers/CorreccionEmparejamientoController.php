<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;

use GoTest\Http\Requests;
use GoTest\RespuestaEmparejamiento;
use GoTest\RespuestaOrden;
use GoTest\PreguntaOrden;


class CorreccionEmparejamientoController extends Controller
{
  public function index()
  {
    //return view('preguntaseleccionmultiple.correccion.resultados', ['pregunta' => $pregunta]);

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create($id)
  {
    $pregunta = PreguntaOrden::find($id);
    foreach ($pregunta->respuestas as $respuesta) {
      $primerasOpciones = $respuesta->primervalor;
      $segundasOpciones = $pregunta->segundovalor;
    }
    $respuestas = $pregunta->respuestas;
    $respuestasSegundo = $pregunta->respuestas;
    $numRespuestas= count($pregunta->respuestas);
    $max = $numRespuestas-1;
    $lista = range(0,$max);
    //$respuestasreordenadas = shuffle($respuestas);

    //$reordenar2 = shuffle(range(0,$numRespuestas-1));
    return view('preguntaorden.correccion.show',['pregunta'=>$pregunta, 'respuestas' => $respuestas,'segundo'=>$respuestasSegundo]);

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
        //nota las relaciones tienen que ser parejos
        //dd($request->all());
        $id_pregunta = $request->idPregunta;
        $pregunta = PreguntaOrden::find($id_pregunta);
        $categoria = PreguntaOrden::obtenerCategoria($id_pregunta);
        $subcategoria = PreguntaOrden::obtenerSubcategoria($id_pregunta);
        //dd($subcategoria);
        $lista_primer_valor = $request->id_respuesta_primer;
        $lista_segundo_valor = $request->id_respuesta_segundo;

        //dd($ultimasRespuestas);
        for($i = 0; $i<count($lista_primer_valor); $i++){
          //esto es si en caso que escogio una opcion ya sea correcta o incorrecta
          if($lista_segundo_valor[$i]>= 0){
            $respuesta_emparejamiento_primer = RespuestaOrden::find($lista_primer_valor[$i]);
            $respuesta_emparejamiento_segundo = RespuestaOrden::find($lista_segundo_valor[$i]);

            $respuesta = new RespuestaEmparejamiento();
            $respuesta->preguntaorden_id = $id_pregunta;
            $respuesta->primervalor_respuesta = $respuesta_emparejamiento_primer->primervalor;
            $respuesta->segundovalor_respuesta = $respuesta_emparejamiento_segundo->segundovalor;
            if ($respuesta_emparejamiento_primer->id == $respuesta_emparejamiento_segundo->id) {
              $respuesta->correcto = true;

            } else {
              $respuesta->correcto = false;

            }
            $respuesta->respondida = true;

          }
          else{
            $respuesta_emparejamiento_primer = RespuestaOrden::find($lista_primer_valor[$i]);
            $respuesta = new RespuestaEmparejamiento();
            $respuesta->preguntaorden_id = $id_pregunta;
            $respuesta->primervalor_respuesta = $respuesta_emparejamiento_primer->primervalor;
            $respuesta->segundovalor_respuesta = "sin respuesta";
            $respuesta->respondida = false;
            $respuesta->correcto = false;
          }
          $respuesta->peso = 100/count($lista_primer_valor);
          $respuesta->save();
        }
        //nota me esta fallando el onDeleteCascade
        $ultimasRespuestas = RespuestaEmparejamiento::ultimasRepuestas($id_pregunta, count($lista_primer_valor));
        return view('preguntaorden.correccion.resultados', ['pregunta'=>$pregunta, 'categoria'=>$categoria,'subcategoria'=>$subcategoria,'ultimasrespuestas'=>$ultimasRespuestas]);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //$respuesta = RespuestaSelMultiple::find($id);

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {

  }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {

  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //eliminacion en cascada

  }
}
