<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use GoTest\Categoria;
use GoTest\SubCategoria;
use GoTest\Http\Requests;
use GoTest\PreguntaOrdenamiento;
use GoTest\OrdenamientoOpciones;
use Session;
use DB;

class PreguntaOrdenamientoController extends Controller
{
    public function __construct(){
      $this->middleware('usuario',['only'=>['create','store','update','destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preguntas = PreguntaOrdenamiento::getAll();
        return view('preguntaordenamiento.index',compact('preguntas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $categorias = Categoria::lists('categoria','id');
      //$subcategorias = SubCategoria::lists('categoria','id');
      return view('preguntaordenamiento.create',compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if(isset($request->secuencia2) && isset($request->opcion2)){//NUEVOS CAMPOS AGREGADOS
        $fusion = array_merge($request->secuencia,$request->secuencia2);
        $repeats = array_count_values($fusion);
        //return $repeats;
        $flag = "ok";
        foreach ($repeats as $key => $value) {
          if($value > 1)
            $flag = "repetido";
        }
        if($flag == "repetido" ){
          Session::flash('store-error','No puede existir orden de secuencia repetidos !');
          return redirect()->route('ordenamiento.create');
        }else{
          //STORE CAMPOS VIEJOS
          PreguntaOrdenamiento::create($request->all());
          // ULTIMO REGISTRO Esto me regresa el campo como un arreglo y puedo accederlo como un string ->first()
          $tupla = DB::table('preguntas_ordenamiento')->select('id')
                                                   ->orderBy('created_at', 'desc')
                                                   ->take('1')
                                                   ->first();
          // OK
          $preguntas_ordenamiento_id = $tupla->id;
          //return $tupla;
          $array_assoc = array_combine($request->secuencia, $request->opcion);//generar array asociativo
          ksort($array_assoc);//ordenar
          foreach ($array_assoc as $sec => $op) {
            $ordenamiento_opciones = new OrdenamientoOpciones();
            $ordenamiento_opciones->secuencia = $sec;
            $ordenamiento_opciones->opcion = $op;
            $ordenamiento_opciones->preguntas_ordenamiento_id = $preguntas_ordenamiento_id;
            $ordenamiento_opciones->save();
          }
          //STORE CAMPOS NUEVOS
          $array_assoc2 = array_combine($request->secuencia2, $request->opcion2);//generar array asociativo
          ksort($array_assoc2);
          for($i = 0; $i < count($array_assoc2); $i++){
            $nuevoCampo = new OrdenamientoOpciones();
            $nuevoCampo->opcion = $request->opcion2[$i];
            $nuevoCampo->secuencia = $request->secuencia2[$i];
            $nuevoCampo->preguntas_ordenamiento_id = $preguntas_ordenamiento_id;
            $nuevoCampo->save();
            }
            Session::flash('update-success','Datos agregados correctamente!');
            return redirect()->route('ordenamiento.index');
          }
        }else{
          PreguntaOrdenamiento::create($request->all());
          // ULTIMO REGISTRO Esto me regresa el campo como un arreglo y puedo accederlo como un string ->first()
          $tupla = DB::table('preguntas_ordenamiento')->select('id')
                                                   ->orderBy('created_at', 'desc')
                                                   ->take('1')
                                                   ->first();
          // OK
          $preguntas_ordenamiento_id = $tupla->id;
          $array_assoc = array_combine($request->secuencia, $request->opcion);//generar array asociativo
          ksort($array_assoc);//ordenar
          $repeats = array_count_values($request->secuencia);
          //return $repeats;
          $flag = "ok";
          foreach ($repeats as $key => $value) {
            if($value > 1)
              $flag = "repetido";
          }
          if($flag == "repetido" ){
            Session::flash('store-error','No puede existir orden de secuencia repetidos !');
            return redirect()->route('ordenamiento.create');
          }else{

          foreach ($array_assoc as $key => $value) {
            $ordenamiento_opciones = new OrdenamientoOpciones();
            $ordenamiento_opciones->secuencia = $key;
            $ordenamiento_opciones->opcion = $value;
            $ordenamiento_opciones->preguntas_ordenamiento_id = $preguntas_ordenamiento_id;
            $ordenamiento_opciones->save();
          }
          Session::flash('update-success','Datos agregados correctamente!');
          return redirect()->route('ordenamiento.index');
        }

      }

      }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pregunta = PreguntaOrdenamiento::getPregunta_detalles($id);
        $respuesta_array = OrdenamientoOpciones::getOpciones_detalles($id);
        $valores = array();
        for($i = 0; $i < count($respuesta_array); $i++) $valores[$i] = $respuesta_array[$i]->opcion;
        
        $respuesta_correcta = implode(", ", $valores);
        return view('preguntaordenamiento.show',['pregunta'=>$pregunta,'respuesta_correcta'=>$respuesta_correcta]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $pregunta = PreguntaOrdenamiento::find($id);
      $subcategoria = DB::table('sub_categorias')->select('id','categoria')->where('categorias_id','=',$pregunta->categorias_id)->lists('categoria','id');
      $categoria = Categoria::lists('categoria','id');
      $opcion = OrdenamientoOpciones::where('preguntas_ordenamiento_id','=',$pregunta->id)->get();
      $count = count($opcion);
      return view('preguntaordenamiento.edit',['pregunta'=>$pregunta,'categoria'=>$categoria,
                  'subcategoria'=>$subcategoria,'opcion'=>$opcion,'count'=>$count]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $pregunta = PreguntaOrdenamiento::find($id);
      $pregunta->categorias_id = $request->categorias_id;
      $pregunta->sub_categorias_id = $request->sub_categorias_id;
      $pregunta->enunciado = $request->enunciado;
      $pregunta->save();

      $secuencia = $request->secuencia;
      $opcion = $request->opcion;

      if(isset($request->secuencia2) && isset($request->opcion2)){//NUEVOS CAMPOS AGREGADOS
        $fusion = array_merge($secuencia,$request->secuencia2);
        $repeats = array_count_values($fusion);
        //return $repeats;
        $flag = "ok";
        foreach ($repeats as $key => $value) {
          if($value > 1)
            $flag = "repetido";
        }
        if($flag == "repetido" ){
          Session::flash('store-error','No puede existir orden de secuencia repetidos !');
          return redirect()->route('ordenamiento.edit',['id'=>$id]);
        }else{
          //UPDATE A CAMPOS VIEJOS
          $array_assoc = array_combine($secuencia, $opcion);//generar array asociativo
          ksort($array_assoc);//ordenar
          $i = 0;
          $id_array = $request->id_opciones;
          foreach ($array_assoc as $sec => $op) {
            $ordenamiento_opciones = OrdenamientoOpciones::find($id_array[$i]);
            $ordenamiento_opciones->secuencia = $sec;
            $ordenamiento_opciones->opcion = $op;
            $ordenamiento_opciones->save();
            $i++;
          }
          //UPDATE A CAMPOS VIEJOS
          //UPDATE A CAMPOS NUEVOS
          $array_assoc2 = array_combine($request->secuencia2, $request->opcion2);//generar array asociativo
          ksort($array_assoc2);
          for($i = 0; $i < count($request->secuencia2); $i++){
            $nuevoCampo = new OrdenamientoOpciones();
            $nuevoCampo->opcion = $request->opcion2[$i];
            $nuevoCampo->secuencia = $request->secuencia2[$i];
            $nuevoCampo->preguntas_ordenamiento_id = $id;
            $nuevoCampo->save();
            }
          }
        }else {
          $repeats = array_count_values($secuencia);
          //return $repeats;
          $flag = "ok";
          foreach ($repeats as $key => $value) {
            if($value > 1)
              $flag = "repetido";
          }
          if($flag == "repetido" ){
            Session::flash('store-error','No puede existir orden de secuencia repetidos !');
            return redirect()->route('ordenamiento.edit',['id'=>$id]);
          }else{
            //UPDATE A CAMPOS VIEJOS
            $array_assoc = array_combine($secuencia, $opcion);//generar array asociativo
            ksort($array_assoc);//ordenar
            $i = 0;
            $id_array = $request->id_opciones;
            foreach ($array_assoc as $sec => $op) {
              $ordenamiento_opciones = OrdenamientoOpciones::find($id_array[$i]);
              $ordenamiento_opciones->secuencia = $sec;
              $ordenamiento_opciones->opcion = $op;
              $ordenamiento_opciones->save();
              $i++;
            }
        }
      }
      Session::flash('update-success','Datos actualizados correctamente!');
      return redirect()->route('ordenamiento.index');
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $pregunta = PreguntaOrdenamiento::find($id);
      $pregunta->delete();
      Session::flash('delete-success','Datos eliminados correctamente!');
      return redirect()->route('ordenamiento.index');
    }
}
