<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use GoTest\Http\Requests;
use GoTest\PreguntaComplementacion;
use GoTest\ComplexionRespuesta;
class ResolverController extends Controller {
    public function getComplecion( $pregunta){
        $pregunta = PreguntaComplementacion::find($pregunta);
        return view('preguntacomplementacion.resolver', array(
            'pregunta' => $pregunta
        ));
    }
    
    public function postComplecion(Request $request){
        $pregunta = $request->get('pregunta');
        $deletedRows = ComplexionRespuesta::where('pregunta_complementacion_id', $pregunta)->delete();
        foreach( $request->get('respuestas') as $key => $res ){
            $respuesta = new \GoTest\ComplexionRespuesta;
            $respuesta->respuesta = $res;
            $respuesta->pregunta_complementacion_id = $pregunta;
            $respuesta->save();
        }
	return redirect()->action('CoreccionController@getComplexion' , array($pregunta));
    }
       
}
