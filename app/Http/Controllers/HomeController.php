<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use GoTest\PreguntaAbierta;
use GoTest\PreguntaComplementacion;
use GoTest\PreguntaFalsoVerdadero;
use GoTest\PreguntaOrden;
use GoTest\PreguntaOrdenamiento;
use GoTest\PreguntaSeleccionMultiple;
use GoTest\PreguntaSeleccionSimple;
use GoTest\Examen;
use GoTest\Usuario;
use GoTest\Http\Requests;

class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.index');
    }
    public function home(){
        $abiertas = PreguntaAbierta::getRows_count();
        $complementacion = PreguntaComplementacion::getRows_count();
        $falso_verdadero = PreguntaFalsoVerdadero::getRows_count();
        $emparejamiento = PreguntaOrden::getRows_count();
        $ordenamiento = PreguntaOrdenamiento::getRows_count();
        $sel_multiple = PreguntaSeleccionMultiple::getRows_count();
        $sel_simple = PreguntaSeleccionSimple::getRows_count();
        $examenes = Examen::getRows_count();
        $usuarios = Usuario::getRows_count();
        $total = $abiertas + $complementacion + $falso_verdadero + $emparejamiento + $sel_multiple
                 + $sel_simple ;
        return view('home.home',['preguntas'=>$total,'examenes'=>$examenes,'usuarios'=>$usuarios]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
