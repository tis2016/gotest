<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use GoTest\FalsoVerdaderoOpciones;
use GoTest\PreguntaFalsoVerdadero;
use GoTest\Http\Requests;
use GoTest\Http\Requests\FalsoVerdaderoCreateRequest;
use GoTest\Http\Requests\FalsoVerdaderoUpdateRequest;
use GoTest\Categoria;
use GoTest\SubCategoria;
use Session;
use DB;

class PreguntaFalsoVerdaderoController extends Controller
{
    public function __construct(){
      $this->middleware('usuario',['only'=>['create','store','update','destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $preguntas = PreguntaFalsoVerdadero::getAll();
      return view('falsoverdadero.index',compact('preguntas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $categorias = Categoria::lists('categoria','id');
      //$subcategorias = SubCategoria::lists('categoria','id');
      return view('falsoverdadero.create',compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $opcionesSelect = $request->opcion;
        $correctosSelect = $request->correcto;
        PreguntaFalsoVerdadero::create($request->all());
        $tupla = DB::table('preguntas_falso_verdadero')->select('id')
                                                 ->orderBy('created_at', 'desc')
                                                 ->take('1')
                                                 ->first();
        // OK
        $preguntas_falso_verdadero_id = $tupla->id;

        for($i = 0; $i < count($opcionesSelect); $i++){
          $opciones = new FalsoVerdaderoOpciones();
          $opciones->opcion = $opcionesSelect[$i];
          if($opcionesSelect[$i] == "falso"){
            $opciones->correcto = $correctosSelect[$i];
            $opciones->peso = 0;
          }else{
           $opciones->correcto = $correctosSelect[$i];
           $opciones->peso = 100;
          }
          $opciones->preguntas_falso_verdadero_id = $preguntas_falso_verdadero_id;
          //return $opciones->preguntas_falso_verdadero_id;
          $opciones->save();
        }
        Session::flash('store-success','Datos agregados correctamente!');
        return redirect()->route('falso-verdadero.index');
  
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pregunta = PreguntaFalsoVerdadero::getPregunta_detalles($id);
        $opciones_pregunta = PreguntaFalsoVerdadero::getOpciones_detalles($id);
        //dd($opciones_pregunta);
        return view('falsoverdadero.show',['pregunta'=>$pregunta,'opciones'=>$opciones_pregunta]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $pregunta = PreguntaFalsoVerdadero::find($id);
      $subcategoria = DB::table('sub_categorias')->select('id','categoria')->where('categorias_id','=',$pregunta->categorias_id)->lists('categoria','id');
      $categoria = Categoria::lists('categoria','id');
      $obj = new FalsoVerdaderoOpciones();
      $opcion = FalsoVerdaderoOpciones::where('preguntas_falso_verdadero_id','=',$pregunta->id)->get();
      $count = count($opcion);
      $correcto = $obj->getCorrectos($id);
      return view('falsoverdadero.edit',['pregunta'=>$pregunta,'categoria'=>$categoria,
      'subcategoria'=>$subcategoria,'opcion'=>$opcion]);//quitar count fv
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $fusion = array_merge($request->opcion,$request->correcto);
      //["falso","falso","1","0"]
      $array = $request->id_opciones;
      $flag = "ok";
      $var_opcion1 = $fusion[0];
      $var_opcion2 = $fusion[1];
      $value_correcto1 = $fusion[2];
      $value_correcto2 = $fusion[3];
      if($var_opcion1 == "falso" && $var_opcion1 == $var_opcion2) $flag = "falsos-repetidos";
      if($var_opcion1 == "verdadero" && $var_opcion1 == $var_opcion2) $flag = "verdaderos-repetidos";
      if($value_correcto1 == "1" && $value_correcto1 == $value_correcto2) $flag = "correctos-repetidos";
      if($value_correcto1 == "0" && $value_correcto1 == $value_correcto2) $flag = "incorrectos-repetidos";


      switch($flag){
        case "falsos-repetidos":
          Session::flash('store-error','No pueden existir valores falsos repetidos !');
          return redirect()->route('falso-verdadero.edit',['id'=>$id]);
          break;
        case "verdaderos-repetidos":
          Session::flash('store-error','No pueden existir valores verdaderos repetidos !');
          return redirect()->route('falso-verdadero.edit',['id'=>$id]);
          break;
        case "correctos-repetidos":
          Session::flash('store-error','No pueden existir opciones correctas repetidas !');
          return redirect()->route('falso-verdadero.edit',['id'=>$id]);
          break;
        case "incorrectos-repetidos":
          Session::flash('store-error','No pueden existir opciones incorrectas repetidas !');
          return redirect()->route('falso-verdadero.edit',['id'=>$id]);
          break;
        default:
          $pregunta = PreguntaFalsoVerdadero::find($id);
          $pregunta->enunciado = $request->enunciado;
          $pregunta->categorias_id = $request->categorias_id;
          $pregunta->sub_categorias_id = $request->sub_categorias_id;
          $pregunta->save();
          for($i = 0; $i < count($array); $i++){
            $opcionObj = FalsoVerdaderoOpciones::find($array[$i]);
            $opcionObj->opcion = $request->opcion[$i];
            $opcionObj->correcto = $request->correcto[$i];
            $opcionObj->peso = 100;
            $opcionObj->save();
          }
        }
        Session::flash('store-success','Datos actualizados correctamente!');
        return redirect()->route('falso-verdadero.index');
      }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $pregunta = PreguntaFalsoVerdadero::find($id);
      $pregunta->delete();
      Session::flash('delete-success','Datos eliminados correctamente!');
      return redirect()->route('falso-verdadero.index');
    }
}
