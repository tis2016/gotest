<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use GoTest\SeleccionSimpleOpciones;
use GoTest\PreguntaSeleccionSimple;
use GoTest\Http\Requests;
use GoTest\Categoria;
use GoTest\SubCategoria;
use Session;
use DB;

class PreguntaSeleccionSimpleController extends Controller
{
    public function __construct(){
      $this->middleware('usuario',['only'=>['create','store','update','destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $preguntas = PreguntaSeleccionSimple::getAll();
      return view('preguntaseleccion.index',compact('preguntas'));
      //dd($preguntas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $categorias = Categoria::lists('categoria','id');
      //$subcategorias = SubCategoria::lists('categoria','id');
      return view('preguntaseleccion.create',compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
         {
             $opcionesSelect = $request->opcion;
             $correctosSelect = $request->correcto;
             PreguntaSeleccionSimple::create($request->all());
             // ULTIMO REGISTRO Esto me regresa el campo como un arreglo y puedo accederlo como un string ->first()
             $tupla = DB::table('preguntas_seleccion_simple')->select('id')
                                                      ->orderBy('created_at', 'desc')
                                                      ->take('1')
                                                      ->first();
             // OK
             $preguntas_seleccion_simple_id = $tupla->id;
             for($i = 0; $i < count($opcionesSelect); $i++){
               $opciones = new SeleccionSimpleOpciones();
               $opciones->opcion = $opcionesSelect[$i];
               if($correctosSelect[$i] == '0'){
                 $opciones->correcto = 0;
                 $opciones->peso = 0;
               }else{
                $opciones->correcto = 1;
                $opciones->peso = 100;
               }
               $opciones->preguntas_seleccion_simple_id = $preguntas_seleccion_simple_id;
               $opciones->save();
             }
             Session::flash('store-success','Datos agregados correctamente!');
             return redirect()->route('seleccion-simple.index');
     }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pregunta = PreguntaSeleccionSimple::getPregunta_detalles($id);
        $opciones_pregunta = PreguntaSeleccionSimple::getOpciones_detalles($id);
        return view('preguntaseleccion.show',['pregunta'=>$pregunta,'opciones'=>$opciones_pregunta]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $pregunta = PreguntaSeleccionSimple::find($id);
      $subcategoria = DB::table('sub_categorias')->select('id','categoria')->where('categorias_id','=',$pregunta->categorias_id)->lists('categoria','id');
      $categoria = Categoria::lists('categoria','id');
      $obj = new SeleccionSimpleOpciones();
      $opcion = SeleccionSimpleOpciones::where('preguntas_seleccion_simple_id','=',$pregunta->id)->get();
      $count = count($opcion);
      $correcto = $obj->getCorrectos($id);
      return view('preguntaseleccion.edit',['pregunta'=>$pregunta,'categoria'=>$categoria,
      'subcategoria'=>$subcategoria,'opcion'=>$opcion,'count'=>$count]);//quitar count fv
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
         if(isset($request->correcto2) && isset($request->opcion2)){
           $correcto_old = $request->correcto;
           $correcto_new = $request->correcto2;
           $opcion_old = $request->opcion;
           $opcion_new = $request->opcion2;

           $fusion = array_merge($correcto_old,$correcto_new);
           $repeats = array_count_values($fusion);
           $flag = "ok";
           $count_inc = 0;
           $count_cor = 0;
           for ($i=0; $i < count($fusion) ; $i++) {
             if($fusion[$i] == '0') $count_inc++;
             if($fusion[$i] == '1') $count_cor++;
           }
           if($count_cor > 1 ){
             Session::flash('store-error','No puede existir valores verdadero repetidos !');
             return redirect()->route('seleccion-simple.edit',['id'=>$id]);
           }
           if($count_inc > 1 && $count_cor < 1){
             Session::flash('store-error','Debe existir al menos una opcion correcta !');
             return redirect()->route('seleccion-simple.edit',['id'=>$id]);
           }else{
             $pregunta = PreguntaSeleccionSimple::find($id);
             $pregunta->enunciado = $request->enunciado;
             $pregunta->categorias_id = $request->categorias_id;
             $pregunta->sub_categorias_id = $request->sub_categorias_id;
             $pregunta->save();
             $array = $request->id_opciones;
             for($i = 0; $i < count($array); $i++){
               $opcionObj = SeleccionSimpleOpciones::find($array[$i]);
               $opcionObj->opcion = $request->opcion[$i];
               $opcionObj->correcto = $request->correcto[$i];
               $opcionObj->peso = 100;
               $opcionObj->save();
             }
             for($i = 0; $i < count($request->opcion2); $i++){
               $newField = new SeleccionSimpleOpciones();
               $newField->opcion = $request->opcion2[$i];
               $newField->correcto = $request->correcto2[$i];
               $newField->peso = 100;
               $newField->preguntas_seleccion_simple_id = $id;
               $newField->save();
             }
             Session::flash('store-success','Datos actualizados correctamente!');
             return redirect()->route('seleccion-simple.index');
         }
       }else{
         //sin cambios en opciones
         $correcto_old = $request->correcto;
         $repeats = array_merge($correcto_old);
         $count_inc = 0;
         $count_cor = 0;
         for ($i=0; $i < count($repeats) ; $i++) {
           if($repeats[$i] == '0') $count_inc++;
           if($repeats[$i] == '1') $count_cor++;
         }
         if($count_cor > 1 ){
           Session::flash('store-error','No puede existir valores verdadero repetidos !');
           return redirect()->route('seleccion-simple.edit',['id'=>$id]);
         }
         if($count_inc > 1 && $count_cor < 1){
           Session::flash('store-error','Debe existir al menos una opcion correcta !');
           return redirect()->route('seleccion-simple.edit',['id'=>$id]);
         }else{
           $pregunta = PreguntaSeleccionSimple::find($id);
           $pregunta->enunciado = $request->enunciado;
           $pregunta->categorias_id = $request->categorias_id;
           $pregunta->sub_categorias_id = $request->sub_categorias_id;
           $pregunta->save();
           $array = $request->id_opciones;
           for($i = 0; $i < count($array); $i++){
             $opcionObj = SeleccionSimpleOpciones::find($array[$i]);
             $opcionObj->opcion = $request->opcion[$i];
             $opcionObj->correcto = $request->correcto[$i];
             $opcionObj->peso = 100;
             $opcionObj->save();
           }
         }
         Session::flash('store-success','Datos actualizados correctamente!');
         return redirect()->route('seleccion-simple.index');
}
       }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //eliminacion en cascada
        $pregunta = PreguntaSeleccionSimple::find($id);
        $pregunta->delete();
        Session::flash('delete-success','Datos eliminados correctamente!');
        return redirect()->route('seleccion-simple.index');
    }
}
