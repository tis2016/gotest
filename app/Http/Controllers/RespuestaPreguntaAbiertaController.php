<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use GoTest\PreguntaAbierta;
use GoTest\Categoria;
use GoTest\RespuestaPreguntaAbierta;
use GoTest\Http\Requests;

class RespuestaPreguntaAbiertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pregunta = PreguntaAbierta::getPregunta_detalles($request->pregunta_id);
        
        $respuesta_usuario = $request->respuesta;
        $respuesta = new RespuestaPreguntaAbierta();
        $respuesta->respuesta = $request->respuesta;
        $respuesta->peso = -1;
        $respuesta->preguntas_abiertas_id = $request->pregunta_id;
        $respuesta->save();

        return view('preguntaabierta.correccion.detalle',['pregunta'=>$pregunta,'respuesta'=>$respuesta_usuario]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pregunta = PreguntaAbierta::find($id);
        $categorias = Categoria::lists('categoria','id');
        return view('preguntaabierta.correccion.show',compact('categorias'),['pregunta'=>$pregunta]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
