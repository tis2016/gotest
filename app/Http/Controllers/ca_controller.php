<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;

use GoTest\Http\Requests;

use GoTest\CAEmparejamiento;
use GoTest\RespuestaOrden;


class ca_controller extends Controller
{
    public function index()
    {
    	$ca_emp = CAEmparejamiento::all();
    	$op_emp = RespuestaOrden::all();
    	return view('ca_emparejamiento.ver', ['ca_emp' => $ca_emp,'op_emp' => $op_emp]);
    }
}
