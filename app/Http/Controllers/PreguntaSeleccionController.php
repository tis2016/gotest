<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use GoTest\SeleccionOpciones;
use GoTest\PreguntaSeleccion;
use GoTest\Http\Requests;
use GoTest\Categoria;
use GoTest\SubCategoria;
use Session;
use DB;
use Validator;

class PreguntaSeleccionController extends Controller
{
    public function __construct(){
      $this->middleware('usuario',['only'=>['create','store','update','destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $preguntas = PreguntaSeleccion::getAll();
      return view('preguntaseleccion.index',compact('preguntas'));
      //dd($preguntas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('preguntaseleccion.create');
        //return redirect()-route('pregunta.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        PreguntaSeleccion::create($request->all());
        // ULTIMO REGISTRO Esto me regresa el campo como un arreglo y puedo accederlo como un string ->first()
        $tupla = DB::table('preguntas_seleccion')->select('id')
                                                 ->orderBy('created_at', 'desc')
                                                 ->take('1')
                                                 ->first();
        // OK
        $preguntas_seleccion_id = $tupla->id;
        $opcionesSelect = $request->opcion;
        $correctosSelect = $request->correcto;

        for($i = 0; $i < count($opcionesSelect); $i++){
          $opciones = new SeleccionOpciones();
          $opciones->opcion = $opcionesSelect[$i];
          if($correctosSelect[$i] == '0'){
            $opciones->correcto = 0;
            $opciones->peso = 0;
          }else{
           $opciones->correcto = 1;
           $opciones->peso = 100;
          }
          $opciones->preguntas_seleccion_id = $preguntas_seleccion_id;
          $opciones->save();
        }
        
        Session::flash('store-success','Datos agregados correctamente!');
        return redirect()->route('seleccion.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $pregunta = PreguntaSeleccion::find($id);
      $subcategoria = DB::table('sub_categorias')->select('id','categoria')->where('categorias_id','=',$pregunta->categorias_id)->lists('categoria','id');
      $categoria = Categoria::lists('categoria','id');
      $obj = new SeleccionOpciones();
      $opcion = SeleccionOpciones::where('preguntas_seleccion_id','=',$pregunta->id)->get();
      $correcto = $obj->getCorrectos($id);
      return view('preguntaseleccion.edit',['pregunta'=>$pregunta,'categoria'=>$categoria,
      'subcategoria'=>$subcategoria,'opcion'=>$opcion]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pregunta = PreguntaSeleccion::find($id);
        $pregunta->enunciado = $request->enunciado;
        $pregunta->categorias_id = $request->categorias_id;
        $pregunta->sub_categorias_id = $request->sub_categorias_id;
        $pregunta->save();
        $array = $request->id_opciones;
        for($i = 0; $i < count($array); $i++){
          $opcionObj = SeleccionOpciones::find($array[$i]);
          $opcionObj->opcion = $request->opcion[$i];
          $opcionObj->correcto = $request->correcto[$i];
          $opcionObj->save();
        }
        if(isset($request->correcto2) && isset($request->opcion2)){//NUEVOS CAMPOS AGREGADOS
          for($i = 0; $i < count($request->correcto2); $i++){
            $nuevoCampo = new SeleccionOpciones();
            $nuevoCampo->opcion = $request->opcion2[$i];
            $nuevoCampo->correcto = $request->correcto2[$i];
            $nuevoCampo->preguntas_seleccion_id = $id;
            $nuevoCampo->save();
          }
        }
        Session::flash('update-success','Datos actualizados correctamente!');
        return redirect()->route('seleccion.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //eliminacion en cascada
        $pregunta = PreguntaSeleccion::find($id);
        $pregunta->delete();
        Session::flash('delete-success','Datos eliminados correctamente!');
        return redirect()->route('seleccion.index');
    }
}
