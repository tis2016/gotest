<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;

use GoTest\Http\Requests;
use GoTest\Pregunta;

class PreguntasController extends Controller {
    public function getIndex(){
        $preguntas = Pregunta::paginate(10);
        return view('preguntas.index', array(
            'preguntas' => $preguntas
        ));
    } 
}
