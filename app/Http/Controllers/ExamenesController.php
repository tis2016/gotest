<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;

use GoTest\Http\Requests;
use GoTest\Examen;
class ExamenesController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
    }
    public 	function getIndex(){
    	
    	$examenes = Examen::all();
    	return view('examenes.listar', array(
    		'examenes' => $examenes
    	));
    }

    public function getAgregar(){
    	return view('examenes.agregar');
    }

    public function postAgregar(Request $request) {
    	
    	$this->validate($request, Examen::$reglas, Examen::$mensajes);
    	$examen = new Examen;
    	$examen->create($request->all());

    	return redirect()->action('ExamenesController@getIndex');
    }

}
