<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use GoTest\PreguntaAbierta;
use GoTest\Categoria;
use GoTest\SubCategoria;
use GoTest\Http\Requests;
use GoTest\Http\Requests\AbiertaCreateRequest;
use Session;
use DB;

class PreguntaAbiertaController extends Controller
{
    public function __construct(){
      $this->middleware('usuario',['only'=>['create','store','update','destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preguntas = PreguntaAbierta::getAll();
        return view('preguntaabierta.index',compact('preguntas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $categorias = Categoria::lists('categoria','id');
      //$subcategorias = SubCategoria::lists('categoria','id');
      return view('preguntaabierta.create',compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AbiertaCreateRequest $request)
    {
        PreguntaAbierta::create($request->all());
        Session::flash('store-success','Datos agregados correctamente!');
        return redirect()->route('abierta.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pregunta1 = PreguntaAbierta::getPregunta_detalles($id);
        $pregunta2 = PreguntaAbierta::find($id);
        $categorias = Categoria::lists('categoria','id');
        return view('preguntaabierta.show',compact('categorias'),['pregunta1'=>$pregunta1,'pregunta2'=>$pregunta2]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $pregunta = PreguntaAbierta::find($id);
      $subcategoria = DB::table('sub_categorias')->select('id','categoria')->where('categorias_id','=',$pregunta->categorias_id)->lists('categoria','id');
      $categoria = Categoria::lists('categoria','id');
      return view('preguntaabierta.edit',['pregunta'=>$pregunta,'categoria'=>$categoria,'subcategoria'=>$subcategoria]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AbiertaCreateRequest $request, $id)
    {
        $pregunta = PreguntaAbierta::find($id);
        $pregunta->enunciado = $request->enunciado;
        $pregunta->respuesta = $request->respuesta;
        $pregunta->infopath = $request->infopath;

        $pregunta->categorias_id = $request->categorias_id;
        $pregunta->sub_categorias_id = $request->sub_categorias_id;
        $pregunta->save();
        Session::flash('update-success','Datos actualizados correctamente!');
        return redirect()->route('abierta.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pregunta = PreguntaAbierta::find($id);
        $pregunta->delete();
        Session::flash('delete-success','Datos eliminados correctamente!');
        return redirect()->route('abierta.index');
    }
}
