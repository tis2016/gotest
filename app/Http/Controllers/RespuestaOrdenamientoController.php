<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use GoTest\PreguntaOrdenamiento;
use GoTest\OrdenamientoOpciones;
use GoTest\RespuestaOrdenamiento;
use GoTest\Http\Requests;
use DB;

class RespuestaOrdenamientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $opciones = $request->opcion;//opciones elegidas en string
        $array_opciones = explode(", ", $opciones);//opciones elegidas a array
        $preg_detalles = PreguntaOrdenamiento::getPregunta_detalles($request->pregunta_id);//pregunta
        $opciones_detalles = OrdenamientoOpciones::getOpciones_detalles($request->pregunta_id);//correctos
        $correctos = array();
        for($i = 0; $i < count($opciones_detalles); $i++) $correctos[$i] = $opciones_detalles[$i]->opcion;

        $nuevo_registro = new RespuestaOrdenamiento();
        $nuevo_registro->respuesta = $opciones;
        $nuevo_registro->preguntas_ordenamiento_id = $request->pregunta_id;

        if($array_opciones === $correctos){
            $nuevo_registro->peso = 100;
            $nuevo_registro->correcto = true;
        }else{
            $nuevo_registro->peso = 0;
            $nuevo_registro->correcto = false;
        }
        $nuevo_registro->save();
        $opcion_elegida = $opciones;
        $opcion_correcta = implode(", ",$correctos);
        // ULTIMO REGISTRO Esto me regresa el campo como un arreglo y puedo accederlo como un string ->first()
        $detalle_respuesta = DB::table('respuestas_ordenamiento')
                     ->select('id','respuesta','correcto','peso','created_at','updated_at')
                     ->orderBy('created_at', 'desc')
                     ->take('1')
                     ->first();
        // OK
        return view("preguntaordenamiento.correccion.detalle",["pregunta"=>$preg_detalles,
                                                               "opcion_elegida"=>$opcion_elegida,
                                                               "opcion_correcta"=>$opcion_correcta,
                                                               "detalle_respuesta"=>$detalle_respuesta]);        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pregunta = PreguntaOrdenamiento::find($id);
        $opciones = OrdenamientoOpciones::getOpciones_detalles($id);
        $original = array();
        
        for($i = 0; $i < count($opciones); $i++) $original[$i] = $opciones[$i]->opcion;
        //shuffle($output);
        //uksort($output, function() { return rand() > rand(); });
        //$arr = array_values($output);
        return view('preguntaordenamiento.correccion.show',['pregunta'=>$pregunta,
                                                            'original'=>$original]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
