<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use GoTest\Http\Requests;
use GoTest\Categoria;
use GoTest\SubCategoria;

use GoTest\PreguntaAbierta;
use GoTest\PreguntaOrdenamiento;
use GoTest\PreguntaFalsoVerdadero;
use GoTest\PreguntaSeleccionSimple;

use GoTest\CAEmparejamiento;
use GoTest\PreguntaSeleccionMultiple;
use GoTest\PreguntaComplementacion;
use GoTest\PreguntaExamen;
use Response;

class PreguntasExamenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function paginate($items,$perPage){
     $pageStart = \Request::get('page', 1);
     // muestro valores a partir de este numero
     $offSet = ($pageStart * $perPage) - $perPage;
     // muestro solo los valores q necesito usando array_slice
     $itemsForCurrentPage = array_slice($items, $offSet, $perPage, true);
     return new LengthAwarePaginator($itemsForCurrentPage, count($items), $perPage,Paginator::resolveCurrentPage(), array('path' => Paginator::resolveCurrentPath()));
    }

    public function index(){
      $pregunta_examen = PreguntaExamen::all();
      $preg_ab_obj = PreguntaAbierta::getAll_front();
      $preg_fv_obj = PreguntaFalsoVerdadero::getAll_front();

      $preg_ord_obj = PreguntaOrdenamiento::getAll_front();
      $preg_ss_obj = PreguntaSeleccionSimple::getAll_front();

      $preg_emparej_obj = CAEmparejamiento::getAll_front();
      $preg_sm_obj = PreguntaSeleccionMultiple::getAll_front1();
      $preg_comp_obj = PreguntaComplementacion::getAll_front();

      $preguntas_ab = (array) $preg_ab_obj;
      $preguntas_fv = (array) $preg_fv_obj;
      $preguntas_ord = (array) $preg_ord_obj;
      $preguntas_ss = (array) $preg_ss_obj;

      $preguntas_em = (array) $preg_emparej_obj;
      $preguntas_sm = (array) $preg_sm_obj;
      $preguntas_comp = (array) $preg_comp_obj;

      $merge1 = array_merge(array_values($preguntas_ab), array_values($preguntas_fv));
      $merge2 = array_merge(array_values($preguntas_ord), array_values($preguntas_ss));
      $merge3 = array_merge(array_values($preguntas_em), array_values($preguntas_sm));

      $merge4 = array_merge(array_values($merge3), array_values($preguntas_comp));
      $merge5 = array_merge(array_values($merge4), array_values($merge2));
      $merge6 = array_merge(array_values($merge5), array_values($merge1));
      //$merge1 = array_merge(array_values($preguntas_ab), array_values($preguntas_em));

      shuffle($merge6);//desordenar (opcional)

     // dd($merge6);
      $preguntas = Response::json($merge6);
      $preguntas = $this->paginate($merge6, 100);//merge 3
      return view('preguntasexamen.agregarpreguntas',['pregunta_examen' => $pregunta_examen,'preguntas' => $preguntas]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function post(Request $request)
    {
      $tipo_lista = $request->input('tipo');
      $preg_id_lista = $request->input('pregunta_id');
      $orden_lista = $request->input('orden');
      $valor_lista = $request->input('valor');
      $exa_id = $request->input('examen');
      $ind = 0;
      foreach ($tipo_lista as $tipo ) {
        $examen = new PreguntaExamen;
        $examen->examen_id = $exa_id;
        $examen->tipo_pregunta = $tipo_lista[$ind];
        $examen->pregunta_id = $preg_id_lista[$ind];
        $examen->orden = $orden_lista[$ind];
        $examen->valor = $valor_lista[$ind];
        $examen->save();
         $ind ++;
      }
      return redirect('/agregar-pregunta?examen='.$exa_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $preguntasAbiertas = PreguntaAbierta::lists();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
