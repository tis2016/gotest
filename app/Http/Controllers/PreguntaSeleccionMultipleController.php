<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use GoTest\PreguntaSeleccionMultiple;
use GoTest\SeleccionMultipleOpciones;
use GoTest\Categoria;
use GoTest\Http\Requests;
use Session;
use DB;
class PreguntaSeleccionMultipleController extends Controller
{
    public function __construct(){
      $this->middleware('usuario',['only'=>['create','store','update','destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $preguntas = PreguntaSeleccionMultiple::getAll();
      return view('preguntaseleccionmultiple.index',compact('preguntas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $categorias = Categoria::lists('categoria','id');
      //$subcategorias = SubCategoria::lists('categoria','id');
      return view('preguntaseleccionmultiple.create',compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        PreguntaSeleccionMultiple::create($request->all());
         //ULTIMO REGISTRO Esto me regresa el campo como un arreglo y puedo accederlo como un string ->first()
        $last_record = DB::table('preguntas_seleccion_multiple')->select('id')
                                               ->orderBy('created_at', 'desc')
                                                ->take('1')
                                                ->first();
        // OK
        $pesos = 0;
        if(isset($request->contador)) 
          $pesos = 100/$request->contador;
        else $pesos = 2;
        $opciones = $request->opcion;
        $correctos = $request->correcto;

        for($i=0; $i < count($opciones); $i++) { 
            $opcion_obj = new SeleccionMultipleOpciones();
            $opcion_obj->opcion = $opciones[$i];
            if($correctos[$i] == '1'){
                $opcion_obj->peso = $pesos;
                $opcion_obj->correcto = 1;
            }
            else 
                $opcion_obj->peso = 0;
            $opcion_obj->preguntas_seleccion_multiple_id = $last_record->id;
            $opcion_obj->save();
        }
        Session::flash('store-success','Datos agregados correctamente!');
        return redirect()->route('seleccion-multiple.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $pregunta = PreguntaSeleccionMultiple::find($id);
      $subcategoria = DB::table('sub_categorias')->select('id','categoria')->where('categorias_id','=',$pregunta->categorias_id)->lists('categoria','id');
      $categoria = Categoria::lists('categoria','id');
      $obj = new SeleccionMultipleOpciones();
      $opcion = SeleccionMultipleOpciones::where('preguntas_seleccion_multiple_id','=',$pregunta->id)->get();
      $count = count($opcion);
      $correcto = $obj->getCorrectos($id);
      return view('preguntaseleccionmultiple.edit',['pregunta'=>$pregunta,'categoria'=>$categoria,
      'subcategoria'=>$subcategoria,'opcion'=>$opcion,'count2'=>$count]);//quitar count fv
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id_opciones = $request->id_opciones;
        $correcto_old = $request->correcto;
        $opcion_old = $request->opcion;

        if(isset($request->correcto2) && isset($request->opcion2)){
           $correcto_new = $request->correcto2;
           $opcion_new = $request->opcion2;
           $pregunta = PreguntaSeleccionMultiple::find($id);
           $pregunta->enunciado = $request->enunciado;
           $pregunta->categorias_id = $request->categorias_id;
           $pregunta->subcategorias_id = $request->sub_categorias_id;

           for ($i=0; $i < count($id_opciones) ; $i++) { 
               $opcion_obj = SeleccionMultipleOpciones::find($id_opciones[$i]);
               $opcion_obj->opcion = $opcion_old[$i];
               $opcion_obj->correcto = $correcto_old[$i];
               $opcion_obj->preguntas_seleccion_multiple_id = $id;
               $opcion_obj->save();
           }

           for ($i=0; $i < count($correcto_new); $i++) { 
               $nuevas_opciones = new SeleccionMultipleOpciones();
               //echo $opcion_new[$i].":".$correcto_new[$i];
               $nuevas_opciones->opcion = $opcion_new[$i];
               $nuevas_opciones->correcto = $correcto_new[$i];
               $nuevas_opciones->preguntas_seleccion_multiple_id = $id;
               $nuevas_opciones->save();
           }
           Session::flash('update-success','Datos actualizados correctamente!');
           return redirect()->route('seleccion-multiple.index');
       }else{

           $pregunta = PreguntaSeleccionMultiple::find($id);
           $pregunta->enunciado = $request->enunciado;
           $pregunta->categorias_id = $request->categorias_id;
           $pregunta->subcategorias_id = $request->sub_categorias_id;

           for ($i=0; $i < count($id_opciones) ; $i++) { 
               $opcion_obj = SeleccionMultipleOpciones::find($id_opciones[$i]);
               $opcion_obj->opcion = $opcion_old[$i];
               $opcion_obj->correcto = $correcto_old[$i];
               $opcion_obj->preguntas_seleccion_multiple_id = $id;
               $opcion_obj->save();
           }
           Session::flash('update-success','Datos actualizados correctamente!');
           return redirect()->route('seleccion-multiple.index');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pregunta = PreguntaSeleccionMultiple::find($id);
        $pregunta->delete();
        Session::flash('delete-success','Datos eliminados correctamente!');
        return redirect()->route('seleccion-multiple.index');
    }
}
