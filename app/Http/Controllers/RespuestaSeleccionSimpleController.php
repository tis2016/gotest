<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;
use GoTest\RespuestaSeleccionSimple;
use GoTest\SeleccionSimpleOpciones;
use GoTest\PreguntaSeleccionSimple;
use GoTest\Categoria;
use GoTest\Http\Requests;
use DB;

class RespuestaSeleccionSimpleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pregunta = PreguntaSeleccionSimple::getPregunta_detalles($request->pregunta_id);
        $opciones = PreguntaSeleccionSimple::getOpciones_detalles($request->pregunta_id);
        $correcto_row = SeleccionSimpleOpciones::getCorrectos_row($request->pregunta_id);
        
        $correcto = 1;
        $incorrecto = 0;
        $puntaje_correcto = 100;
        $puntaje_incorrecto = 0;
        $opcion_elegida = $request->opcion;
        
        $respuesta = new RespuestaSeleccionSimple();
        $respuesta->opcion_elegida = $opcion_elegida;

        if($opcion_elegida == $correcto_row[0]->opcion ){ //si es correcto agregamos campos 
            $respuesta->correcto = $correcto;
            $respuesta->peso = 100;    
        } else{
            $respuesta->correcto = $incorrecto;
            $respuesta->peso = 0;
        }
        $respuesta->preguntas_seleccion_simple_id = $request->pregunta_id;
        $respuesta->save();
        // ULTIMO REGISTRO Esto me regresa el campo como un arreglo y puedo accederlo como un string ->first()
        $tupla = DB::table('respuestas_sel_simple')
                     ->select('id','opcion_elegida','correcto','peso','created_at','updated_at')
                     ->orderBy('created_at', 'desc')
                     ->take('1')
                     ->first();
        // OK
        return view('preguntaseleccion.correccion.detalle',['respuesta'=>$tupla, 'opciones'=>$opciones, 'correcto_row'=>$correcto_row,'pregunta'=>$pregunta]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pregunta = PreguntaSeleccionSimple::getPregunta_detalles($id);
        $opciones_pregunta = PreguntaSeleccionSimple::getOpciones_detalles($id);
        return view('preguntaseleccion.correccion.show',['pregunta'=>$pregunta,'opciones'=>$opciones_pregunta]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
