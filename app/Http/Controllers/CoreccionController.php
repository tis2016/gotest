<?php

namespace GoTest\Http\Controllers;

use Illuminate\Http\Request;

use GoTest\Http\Requests;
use GoTest\Pregunta;
use GoTest\Categoria;
use GoTest\SubCategoria;
use GoTest\PreguntaSeleccionSimple;
use GoTest\SeleccionSimpleOpciones;
use GoTest\RespuestaSeleccionSimple;

class CoreccionController extends Controller
{   
    public function getComplexion($pregunta) {
        $pregunta = \GoTest\PreguntaComplementacion::find($pregunta);
    	//algoritmo de comparacion
    	$opciones1 = \GoTest\RespuestaComplementacion::where('pregunta_complementacion_id',$pregunta->id)->orderBy('id','ASC')->get();

    	$opciones2 = \GoTest\ComplexionRespuesta::where('pregunta_complementacion_id',$pregunta->id)->orderBy('id','ASC')->get();
    	
        $res = array();
    	$res['correcto'] = true;
        $respuestas = array();
        $correctas = array();
        $cantidad = 0;
    	for($i=0;$i<count($opciones1);$i++){
    		$op1 = $opciones1[$i];
    		$op2 = $opciones2[$i];
                if($op1->respuesta == $op2->respuesta){
                    $cantidad++;
                    $respuestas[] = "<span class='text-success'>". $op2->respuesta ."</span>";
    		}else{
                    $respuestas[] = "<span class='text-danger'>". $op2->respuesta ."</span>";
                    $res['correcto'] = false;
    		}
                $correctas[] = $op1->respuesta;
    		
    	}
        $respuestaUsuario = vsprintf($pregunta->enunciado, $respuestas);
        $respuestaCorrecta = vsprintf($pregunta->enunciado, $correctas);
    	return view('correccion.complexion',array(
    		'pregunta' => $pregunta->enunciado,
    		'respuesta_usuario'=>$respuestaUsuario,
    		'respuesta_correcta' => $respuestaCorrecta,
    		'cantidad' => $cantidad,
                'correcto' => $res['correcto']
    		));	
    }
    
}
