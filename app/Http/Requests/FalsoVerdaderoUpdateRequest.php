<?php

namespace GoTest\Http\Requests;

use GoTest\Http\Requests\Request;

class FalsoVerdaderoUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
          return [
          'enunciado'=>'required|max:255|min:5',
          'categorias_id'=>'required',
          'sub_categorias_id'=>'required',
        ];
    }
    }

