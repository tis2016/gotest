<?php

namespace GoTest;
use DB;
use Illuminate\Database\Eloquent\Model;

class OrdenamientoOpciones extends Model
{
  protected $table = "ordenamiento_opciones";
  protected $fillable = ['secuencia','opcion','preguntas_ordenamiento_id'];

  public static function getOpciones_detalles($id){
	return DB::select('SELECT o.opcion
	                   FROM ordenamiento_opciones o, preguntas_ordenamiento p
	                   WHERE o.preguntas_ordenamiento_id = p.id AND p.id = '.$id.'
	                   ORDER BY o.secuencia ASC');
	}
}
