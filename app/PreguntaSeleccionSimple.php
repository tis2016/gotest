<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;
use DB;
//
class PreguntaSeleccionSimple extends Model
{
  protected $table = "preguntas_seleccion_simple";
  protected $fillable = ['enunciado','categorias_id','sub_categorias_id','usuarios_id'];

  public static function getAll(){
    return DB::table('preguntas_seleccion_simple')
          ->join('categorias','preguntas_seleccion_simple.categorias_id','=','categorias.id')
          ->join('sub_categorias','preguntas_seleccion_simple.sub_categorias_id','=','sub_categorias.id')
          //->join('sub_categorias','sub_categorias.categorias_id','=','categorias.id')
          ->select('preguntas_seleccion_simple.id','preguntas_seleccion_simple.enunciado','categorias.categoria',
                   'sub_categorias.categoria as subcategoria','preguntas_seleccion_simple.created_at')
          ->paginate(5);
  }

  public static function getAll_front(){
      return DB::select("SELECT p.id, t.tipo, t.alias, p.enunciado, c.categoria, s.categoria as subcategoria, p.created_at 
               FROM sub_categorias s, preguntas_seleccion_simple p, categorias c, tipos_pregunta t
                 WHERE s.id = p.sub_categorias_id AND s.categorias_id = c.id AND t.id = p.tipos_pregunta_id");        
  }

  public static function getAll_front1(){
    return DB::table('preguntas_seleccion_simple')
          ->join('categorias','preguntas_seleccion_simple.categorias_id','=','categorias.id')
          ->join('sub_categorias','preguntas_seleccion_simple.sub_categorias_id','=','sub_categorias.id')
          //->join('sub_categorias','sub_categorias.categorias_id','=','categorias.id')
          ->select('preguntas_seleccion_simple.id','preguntas_seleccion_simple.tipo_pregunta','preguntas_seleccion_simple.enunciado','categorias.categoria',
                   'sub_categorias.categoria as subcategoria','preguntas_seleccion_simple.created_at')
          ->get();
  }

  public static function getPregunta_detalles($id){
    return DB::select('SELECT p.id,p.enunciado, c.categoria, s.categoria as subcategoria, p.created_at, p.updated_at
                      FROM sub_categorias s, preguntas_seleccion_simple p, categorias c
                      WHERE p.categorias_id = c.id AND p.sub_categorias_id = s.id AND s.categorias_id = c.id AND p.id = '.$id);
  }

  public static function getOpciones_detalles($id){
    return DB::select('SELECT s.opcion, s.correcto, s.peso
                       FROM seleccion_simple_opciones s, preguntas_seleccion_simple p
                       WHERE s.preguntas_seleccion_simple_id = p.id AND p.id = '.$id);
  }
     public static function getRows_count(){
        return DB::table("preguntas_seleccion_simple")->count();         
    }
}
