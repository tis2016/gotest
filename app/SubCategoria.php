<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;

class SubCategoria extends Model
{
  protected $table = "sub_categorias";
  protected $fillable = ['categoria','obs','categorias_id'];

  public static function subCategorias($id) {
      return SubCategoria::where('categorias_id', '=', $id)->get();
  }

  public function categoriap(){
      return $this->belongsTo('GoTest\Categoria', 'categorias_id');
  }
}
