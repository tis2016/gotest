<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;

class RespuestasFalsoVerdadero extends Model
{
    protected $table = "respuestas_falso_verdadero";
	  protected $fillable = [
	  	'opcion_elegida',
	  	'correcto',
	  	'peso',
	  	'preguntas_falso_verdadero_id'
	  ];
}
