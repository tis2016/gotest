<?php

namespace GoTest;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Storage;
use File;
use DB;

class PreguntaAbierta extends Model
{
  protected $table = "preguntas_abiertas";
  protected $fillable = ['enunciado','respuesta','infopath','categorias_id','sub_categorias_id','tipos_pregunta_id','usuarios_id'];

  public function setInfopathAttribute($infopath){//hace referencia al campo path
    if(!empty($infopath)){
    $name = Carbon::now()->second.$infopath->getClientOriginalName();
    $this->attributes['infopath'] = $name;
    Storage::disk('local')->put($name, File::get($infopath));
  }
  }
  public static function getAll(){
    return DB::table('preguntas_abiertas')
          ->join('categorias','preguntas_abiertas.categorias_id','=','categorias.id')
          ->join('sub_categorias','preguntas_abiertas.sub_categorias_id','=','sub_categorias.id')
          //->join('sub_categorias','sub_categorias.categorias_id','=','categorias.id')
          ->select('preguntas_abiertas.id','preguntas_abiertas.enunciado','preguntas_abiertas.respuesta','categorias.categoria',
                   'sub_categorias.categoria as subcategoria','preguntas_abiertas.infopath','preguntas_abiertas.created_at')
          ->paginate(5);
  }

  public static function getAll_front(){
    return DB::table('preguntas_abiertas')
          ->join('categorias','preguntas_abiertas.categorias_id','=','categorias.id')
          ->join('sub_categorias','preguntas_abiertas.sub_categorias_id','=','sub_categorias.id')
          ->join('tipos_pregunta','preguntas_abiertas.tipos_pregunta_id','=','tipos_pregunta.id')
          //->join('sub_categorias','sub_categorias.categorias_id','=','categorias.id')
          ->select('preguntas_abiertas.id','tipos_pregunta.tipo','tipos_pregunta.alias','preguntas_abiertas.enunciado','preguntas_abiertas.respuesta','categorias.categoria',
                   'sub_categorias.categoria as subcategoria','preguntas_abiertas.created_at')
          ->get();
  }

  public static function getPregunta_detalles($id){
    return DB::select('SELECT p.id,p.enunciado, c.categoria, s.categoria as subcategoria,p.respuesta,p.infopath, p.created_at, p.updated_at
                      FROM sub_categorias s, preguntas_abiertas p, categorias c
                      WHERE p.categorias_id = c.id AND p.sub_categorias_id = s.id AND s.categorias_id = c.id AND p.id = '.$id);
  }
  public static function getRows_count(){
        return DB::table("preguntas_abiertas")->count();         
  }
  public static function getPreguntas_usuario($id){
    return DB::select('SELECT p.id,p.enunciado, c.categoria, s.categoria as subcategoria,p.respuesta,p.infopath, p.created_at, p.updated_at
                      FROM sub_categorias s, preguntas_abiertas p, categorias c, usuarios u
                      WHERE p.categorias_id = c.id AND p.sub_categorias_id = s.id AND s.categorias_id = c.id AND p.usuarios_id = '.$id);
  }
}
