<?php

namespace GoTest;
use DB;
use Illuminate\Database\Eloquent\Model;

class SeleccionMultipleOpciones extends Model
{
  protected $table = "sel_multiple_opciones";
  protected $fillable = ['opcion','correcto','peso','preguntas_seleccion_multiple_id'];

  public static function getOpciones($id){
    return DB::table('sel_multiple_opciones')
           ->select('id','opcion')
           ->where('preguntas_seleccion_multiple_id','=',$id)->get();
  }
  public static function getCorrectos($id){
    return DB::table('sel_multiple_opciones')
           ->select('correcto')
           ->where('preguntas_seleccion_multiple_id','=',$id)->get();
  }
	  
}
