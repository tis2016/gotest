@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-sm-12">
    <h1 class="page-header">Preguntas Registradas </h1>
  </div>
  <div class="div col-lg-12">
    <div class="panel panel-default">
              <div class="panel-heading">
                  Preguntas registradas en la base de datos
                  <div class="pull-right">
                                <div class="btn-group">
    <a class="glyphicon-plus btn btn-info btn-xs" href="{{ url('/crear-pregunta-complementacion')}}"> Crear Nueva</a>
                                </div>
                            </div>
                  
              </div>
    </div>
      

  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
  <div class="row">
    <div class="col-lg-12">
      @include('alerts.success')
      @include('alerts.required')
      
      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>Enunciado </th>
            <th>Palabras<br> Clave </th>
            <th>Categoria </th>
            <th>Sub <br>Categoria </th>
            <th>Acciones </th>
          </tr>
        </thead>
        <tbody>
          <?php $cat = "";?>
          @foreach($preguntas as $pregunta)
          <tr>
            <td>
              @foreach(explode(" ", $pregunta->enunciado) as $token)
              @if($token == "%s")
              <?php $cat .= "_____";?>
              @else
              <?php $cat .= $token." ";?>
              @endif
              @endforeach
              {{str_limit($cat ,$limit = 30,$end='...')}}
              <?php $cat ="";?>
            </td>
            <td>
              @foreach ($pregunta->respuestas as $respuesta)
              {{ $respuesta->respuesta }} <br>
              @endforeach
            </td>
            @if($pregunta->subcategoria)
            <td>{{ $pregunta->subcategoria->categoriap->categoria }}</td>
            <td>{{ $pregunta->subcategoria->categoria }}</td>
            @else
            <td> sin categoria </td>
            <td> sin sub categoria</td>
            @endif
            <td>
              <div class="form-group" align="center">
                <a href="{{ url('/ver-pregunta-complementacion/'. $pregunta->id)  }}" class="btn btn-success btn-circle" data-tooltip="Ver pregunta"><i class="fa fa-search "></i></a>
                <a href="{{ url('editar-pregunta-complementacion/'. $pregunta->id) }}" class="btn btn-primary btn-circle" data-tooltip="Editar pregunta"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{{ url('/resolver/complecion/' . $pregunta->id ) }}" class="btn btn-warning btn-circle" data-tooltip="Resolver y corregir"><i class="fa fa-mortar-board "></i></a>
                <a href="{{ url('eliminar-pregunta-complementacion/'. $pregunta->id) }}" class="btn btn-danger btn-circle" data-tooltip="Eliminar pregunta" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="glyphicon glyphicon-trash"></i></a></div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        
      </div>
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
  </div>
  <!-- /.row -->
  {!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
  <!-- Bootstrap Core JavaScript -->
  {!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
  <!-- Metis Menu Plugin JavaScript -->
  {!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
  <!-- Morris Charts JavaScript -->
  {!!Html::script('bower_components/raphael/raphael-min.js')!!}
  {!!Html::script('bower_components/morrisjs/morris.min.js')!!}
  {!!Html::script('js/morris-data.js')!!}
  <!-- Custom Theme JavaScript -->
  {!!Html::script('dist/js/sb-admin-2.js')!!}
  @endsection