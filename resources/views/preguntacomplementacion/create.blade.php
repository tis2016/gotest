@extends('layouts.principal')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Registrar tipo de pregunta compleci&oacute;n</h1>


    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
    <div class="row">
        <div class="col-lg-10">
            @include('alerts.success')
            @include('alerts.required')

            <form id="pregunta-crear" action="{{ url('crear-pregunta-complementacion')}}" method="POST" data-toggle="validator" role="form">
                {!! csrf_field() !!}
                {!!Form::hidden('usuarios_id', Auth::user()->id)!!}
                <div class="form-group">
                    <label for="categoria" class="control-label text-primary">Categorias</label>
                    <div class="">
                        <select class="form-control" name="categoria" id="categoria" class="form-control">
                            <option value=""> Seleccione una opcion </option>
                            @foreach( $categorias as $categoria )
                            <option value="{{ $categoria->id }}"> {{ $categoria->categoria }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="subcategoria" class="control-label text-primary">Sub Categorias</label>
                    <div class="">
                        <select class="form-control" name="subcategoria" id="subcategorias" class="form-control">
                            <option value=""> Seleccione una opcion </option>
                        </select>
                    </div>
                </div>     
                
                
                <div class="form-group">
                    <label for="enunciado" class="control-label text-primary">Enunciado</label>
                    <div class="form-group">
                        <textarea class="form-control" name="enunciado" id="enunciado" class="form-control" minlength="6" required ></textarea>
                        <p class="text-success">Para las palabras a complementar usar el  %s </p>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                        <div class="text-danger mensaje_clave">
        </div>  
                <label for="enunciado" class="control-label text-primary">Respuestas - Palabras clave </label>               
                <div id="respuestas">
                    <div class="form-group">
                        <div class="col-sm-11">
                            <input type="text" class="form-control"  name="respuestas[]"placeholder="Palabra Clave" minlength="1" required>
                            <div class="help-block with-errors"></div>

                        </div>
                        <div class="col-sm-1">
                            <a  class="btn btn-danger btn-outline eliminar-respuesta"> <span class="glyphicon glyphicon-remove"> </span> </a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-1 col-sm-offset-11">
                        <a  class="btn btn-default btn-default" id="agregar-respuesta"> <span class="glyphicon glyphicon-plus"> </span> </a>
                    </div>
                </div>
                <br>
                <div class="">
                    <div class="">
                        <button type="submit" class="btn btn-primary"> Guardar
                        </button>
                        <a href="{{url('pregunta-complementacion')}}" type="reset" class="btn btn-primary"> Cancelar
                        </a>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>
<script>

    $('#agregar-respuesta').click(function () {
        var template = '<div class="form-group">' +
                '<div class="col-sm-11">' +
                '<input type="text" class="form-control"  name="respuestas[]"placeholder="Palabra Clave" minlength="1" required>' +
                '<div class="help-block with-errors"></div>'+

                '</div>' +
                '<div class="col-sm-1">' +
                '<a  class="btn btn-danger btn-outline eliminar-respuesta"> <span class="glyphicon glyphicon-remove"> </span> </a>' +
                '</div>' +
                '</div>';
        $('#respuestas').append(template);
        
        $('form .eliminar-respuesta').on('click', function () {
            var respuestas = $(this).closest('#respuestas').find('.form-group');
            if(respuestas.length > 1) {
                $(this).closest('.form-group').remove();
            }
        });
    });

    $('form .eliminar-respuesta').on('click', function () {
        $('form .eliminar-respuesta').on('click', function () {
            var respuestas = $(this).closest('#respuestas').find('.form-group');
            if(respuestas.length > 1) {
                $(this).closest('.form-group').remove();
            }
        });
    });
    
    $('#categoria').change(function(){
        var id = $(this).val();
        if(id == false) {
            var res = '<option value=""> Seleccione una opcion </option>';
            $( "#subcategorias" ).html( res );
            return;
        }
        
        var id = $(this).val();
        var url = "{{url('/subcategorias/')}}";
        
        $.get( url +"/" +id , function( data ) {
            var res = '<option value=""> Seleccione una opcion </option>';
            $.each( data , function(key, value){
                res += '<option value="'+value.id+'">'+ value.categoria +'</option>';
            });
            
            $( "#subcategorias" ).html( res );
            
        });
    });
    

</script>
@endsection
@section('javascript')
<script>
$(function(){
    $('#pregunta-crear').submit(function(){
        $('.mensaje_clave').html("");

        var cadena = $('[name="enunciado"]').val();
        var aux;
        var cont = 0;
        for(var i =0 ; i < cadena.length - 1;i++){
            aux = cadena[i] + cadena[i+1];
            if(aux == "%s") {
                cont++;
            }
        }
        if($("#respuestas > div").length == cont) {
            return true;
        } else if ($("#respuestas > div").length < cont) {
            $('.mensaje_clave').html(" La cantidad de palabras clave es menor al del enunciado (%s)");
        } else {
            $('.mensaje_clave').html(" La cantidad de palabras clave es mayor al del enunciado (%s) ");

        }
        return false;
    });

});
</script>
@endsection