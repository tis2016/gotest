@extends('layouts.principal')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar tipo de pregunta complementaci&oacute;n</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
    <div class="row">
        <div class="col-lg-10">
            @include('alerts.success')
            @include('alerts.required')

            <form action="{{ url('editar-pregunta-complementacion', $pregunta->id)}}" method="POST" role="form" data-toggle="validator">
                {!! csrf_field() !!}
                <div >
                    <label for="categoria" class="control-label text-primary">Categorias</label>
                    <div class="">
                        <select class="form-control" name="categoria" id="categoria" class="form-control">
                            <option value=""> Seleccione una opcion </option>
                            @foreach( $categorias as $categoria )
                            @if($pregunta->subcategoria)
                            <option value="{{ $categoria->id }}" @if($pregunta->subcategoria->categoriap->id ==  $categoria->id) selected @endif> {{ $categoria->categoria }} </option>
                            @else
                            <option value="{{ $categoria->id }}"> {{ $categoria->categoria }} </option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div >
                    <label for="subcategoria" class="control-label text-primary">Sub Categorias</label>
                    <div class="">
                        <select class="form-control" name="subcategoria" id="subcategorias" class="form-control">
                            <option value=""> Seleccione una opcion </option>
                            @if($pregunta->subcategoria) 
                                @foreach ($pregunta->subcategoria->categoriap->subcategorias as $subcategoria)
                                <option value="{{ $subcategoria->id }}" @if($pregunta->subcategoria->id ==  $subcategoria->id) selected @endif> {{ $subcategoria->categoria }} </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="enunciado" class="control-label text-primary">Enunciado</label>
                    <div class="form-group">
                        <textarea class="form-control" name="enunciado" class="form-control" minlength="6" required>{{ $pregunta->enunciado }}</textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <label for="enunciado" class="control-label text-primary">Respuestas - Palabras clave </label>               
                <div id="respuestas">
                    @foreach($pregunta->respuestas as $respuesta)
                    <div class="form-group">
                        <div class="col-sm-11">
                            <input value="{{ $respuesta->respuesta }}" type="text" class="form-control"  name="respuestas[]" placeholder="Palabra Clave" minlength="1" required> 
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-sm-1">
                            <a  class="btn btn-danger btn-outline eliminar-respuesta"> <span class="glyphicon glyphicon-remove"> </span> </a>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="form-group">
                    <div class="col-sm-1 col-sm-offset-11">
                        <a  class="btn btn-default btn-default" id="agregar-respuesta"> <span class="glyphicon glyphicon-plus"> </span> </a>
                    </div>
                </div>
                <br>
                <div class="">
                    <div class="">
                        <button type="submit" class="btn btn-primary"> Actualizar
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->
{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
{!!Html::script('bower_components/raphael/raphael-min.js')!!}
{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
{!!Html::script('js/morris-data.js')!!}
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
<script>

    $('#agregar-respuesta').click(function () {
        var template = '<div class="form-group">' +
                '<div class="col-sm-11">' +
                '<input type="text" class="form-control"  name="respuestas[]"placeholder="Palabra Clave" minlength="1" required>' +
                '<div class="help-block with-errors"></div>'+
                '</div>' +
                '<div class="col-sm-1">' +
                '<a  class="btn btn-danger eliminar-respuesta"> <span class="glyphicon glyphicon-remove"> </span> </a>' +
                '</div>' +
                '</div>';
        $('#respuestas').append(template);
        
        $('form .eliminar-respuesta').on('click', function () {
            var respuestas = $(this).closest('#respuestas').find('.form-group');
            if(respuestas.length > 1) {
                $(this).closest('.form-group').remove();
            }
        });
    });

    $('form .eliminar-respuesta').on('click', function () {
        $('form .eliminar-respuesta').on('click', function () {
            var respuestas = $(this).closest('#respuestas').find('.form-group');
            if(respuestas.length > 1) {
                $(this).closest('.form-group').remove();
            }
        });
    });
    
        $('#categoria').change(function(){
        var id = $(this).val();
        if(id == false) {
            var res = '<option value=""> Seleccione una opcion </option>';
            $( "#subcategorias" ).html( res );
            return;
        }
        
        var id = $(this).val();
        var url = "{{url('/subcategorias/')}}";
        
        $.get( url +"/" +id , function( data ) {
            var res = '<option value=""> Seleccione una opcion </option>';
            $.each( data , function(key, value){
                res += '<option value="'+value.id+'">'+ value.categoria +'</option>';
            });
            
            $( "#subcategorias" ).html( res );
            
        });
    });

</script>
@endsection
