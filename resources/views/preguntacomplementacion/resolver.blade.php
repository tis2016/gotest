@extends('layouts.principal')
@section('content')

  <div class="row">
    <div class="col-sm-12">
        <h1 class="page-header">Pregunta de Compleci&oacute;n</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
<div class="row">
    <div class="col-lg-12">
                <div class="panel panel-primary">
          <div class="panel-heading">
            <b>Pregunta # 1</b>
          </div>
      @include('alerts.success')
      @include('alerts.required')
    
        <form action="{{url('/resolver/complecion')}}" method="post">
                <div class="panel-body">

        {!! csrf_field() !!}
        <input type="hidden" name="pregunta" value="{{ $pregunta->id }}">
        <div class="">
            <h3> 
                @foreach(explode(" ", $pregunta->enunciado) as $token) 
                    @if($token == "%s")
                    <input type="text" name="respuestas[]">
                    @else
                        {{ $token }}
                    @endif
                @endforeach
            </h3>
           
        </div>
                </div>
            
                      <div class="panel-footer" align="right">
            <a href="#" class="btn btn-primary btn-circle" data-tooltip="Atras"><i class="fa fa-chevron-left "></i></a>
            <a href="#" class="btn btn-primary btn-circle" data-tooltip="Adelante"><i class="fa fa-chevron-right"></i></a>
            <a href="{!! URL::to('pregunta-complementacion') !!}" class="btn btn-warning btn-circle" data-tooltip="Regresar a inicio"><i class="fa fa-home "></i></a>
            <button type="submit" class="btn btn-success btn-circle" data-tooltip="Enviar para corregir"><i class="fa fa-check"></i>
            </button>
            
            
          </div>
            
        </form>
                                
    </div>
    </div>
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->
{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
{!!Html::script('bower_components/raphael/raphael-min.js')!!}
{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
{!!Html::script('js/morris-data.js')!!}
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
@endsection
