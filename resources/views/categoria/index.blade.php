@extends('layouts.principal')
@section('content')
  <div class="row">
  <div class="col-lg-12">
      <h1 class="page-header">Categorias</h1>
  </div>
  <!-- /.col-lg-12 -->
  </div>
              <!-- /.row -->
  <div class="row">
      <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                  Categorias de examenes
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                  <div class="dataTable_wrapper">
                      <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                              <tr>
                                  <th>Categoria</th>
                                  <th>Observaciones</th>
                                  <th>Accion</th>
                              </tr>
                          </thead>
                          {{-- <tbody id="datos"> --}}
                            @foreach($categorias as $cat)
                            <tbody>
                            <tr>
                              <td>{{$cat->categoria}}</td>
                              <td>{{$cat->obs}}</td>
                              <td>{!!link_to_route('categoria.edit', $title = 'Editar', $parameters = $cat->id, $attributes = ['class'=>'btn btn-outline btn-primary'])!!}
                                  {!!link_to_route('categoria.destroy',$title = 'Eliminar',$parameteres = $cat->id,$attributes = ['class'=>'btn btn-outline btn-danger','onclick'=>'return confirm("¿Estas seguro de querer eliminar?")'])!!}
                             </td>
                            </tr>
                            </tbody>
                            @endforeach
                          {{-- </tbody> --}}
                      </table>
                      {!!$categorias->render()!!}
                  </div>
              </div>
            </div>
          </div>
        </div>
@endsection
