@extends('layouts.principal')
@section('content')
  {!!Form::model($categoria,['route'=>['categoria.update',$categoria->id],'method'=>'PUT'])!!} <!--enrutando los datos -->
  <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar categoria</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
      @include('categoria.form.categoria')
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->
{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
{!!Html::script('bower_components/raphael/raphael-min.js')!!}
{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
{!!Html::script('js/morris-data.js')!!}
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
@endsection
