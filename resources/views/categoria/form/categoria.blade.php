	<form role="form">
		<div class="form-group">
			{!!Form::label('categoria','Categoria: ')!!}
			{!!Form::text('categoria',null, ['id'=>'categoria','class'=>'form-control', 'placeholder' => 'Ingresa la categoria'])!!}<br>
			{!!Form::label('obs','Observacion: ')!!}
			{!!Form::text('obs',null, ['id'=>'obs','class'=>'form-control', 'placeholder' => 'Observacion'])!!}
			@if(Route::current()->getName() == 'categoria.edit')
				<br>{!!Form::submit('Actualizar',['class'=>'btn btn-outline btn-primary'])!!}
	    @else
				<br>{!!Form::submit('Registrar',['class'=>'btn btn-outline btn-primary'])!!}
	    @endif
  	</div>
	</form>
	{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
	<!-- Bootstrap Core JavaScript -->
	{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
	<!-- Metis Menu Plugin JavaScript -->
	{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
	<!-- Morris Charts JavaScript -->
	{!!Html::script('bower_components/raphael/raphael-min.js')!!}
	{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
	{!!Html::script('js/morris-data.js')!!}
	<!-- Custom Theme JavaScript -->
	{!!Html::script('dist/js/sb-admin-2.js')!!}
