@extends('layouts.principal')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1>Registrar tipo de pregunta Emparejamiento</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
    <div class="row">
        <div class="col-lg-10">
            @include('alerts.success')
            @include('alerts.required')

            <form action="{{ url('crear-pregunta-orden')}}" method="POST" role="form" data-togle="validator">
                {!! csrf_field() !!}
                {!!Form::hidden('usuarios_id', Auth::user()->id)!!}                
                <div >
                    <label for="categoria" class="control-label text-primary">Categorias</label>
                    <div class="">
                        <select class="form-control" name="categoria" id="categoria" class="form-control">
                            <option value=""> Seleccione una opcion </option>
                            @foreach( $categorias as $categoria )
                            <option value="{{ $categoria->id }}"> {{ $categoria->categoria }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div >
                    <label for="subcategoria" class="control-label text-primary">Sub Categorias</label>
                    <div class="">
                        <select class="form-control" name="subcategoria" id="subcategorias" class="form-control">
                            <option value=""> Seleccione una opcion </option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="enunciado" class="control-label text-primary">Enunciado</label>
                    <div class="form-group">
                        <textarea class="form-control" name="enunciado" class="form-control" minlength="6" required></textarea>
                    </div>
                </div>

                <label for="enunciado" class="control-label text-primary">Respuestas</label>               
                <div id="respuestas">
                    <div class="form-group">
                        <div class="col-sm-6">
                            <input type="text" class="form-control"  name="primervalor[]"placeholder="Primer Valor" minlength="1" required>
                        </div>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="segundovalor[]" placeholder="Segundo Valor" minlength="1" required>
                        </div>
                        <div class="col-sm-1">
                            <a  class="btn btn-danger eliminar-respuesta btn-outline"> <span class="glyphicon glyphicon-remove"> </span> </a>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6">
                            <input type="text" class="form-control"  name="primervalor[]"placeholder="Primer Valor" minlength="1" required>
                        </div>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="segundovalor[]" placeholder="Segundo Valor" minlength="1" required>
                        </div>
                        <div class="col-sm-1">
                            <a  class="btn btn-danger btn-outline eliminar-respuesta"> <span class="glyphicon glyphicon-remove"> </span> </a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-1 col-sm-offset-11">
                        <a  class="btn btn-default btn-default" id="agregar-respuesta"> <span class="glyphicon glyphicon-plus"> </span> </a>
                    </div>
                </div>
                <br>
                <div class="">
                    <div class="">
                        <button type="submit" class="btn btn-primary"> Guardar
                        </button>
                        <a href="{{url('pregunta-emparejamiento')}}" type="reset" class="btn btn-primary"> Cancelar
                        </a>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->
{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
{!!Html::script('bower_components/raphael/raphael-min.js')!!}
{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
{!!Html::script('js/morris-data.js')!!}
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
<script>

    $('#agregar-respuesta').click(function () {
        var template = '<div class="form-group">' +
                '<div class="col-sm-6">' +
                '<input type="text" class="form-control"  name="primervalor[]"placeholder="Primer Valor" minlength="1" required>' +
                '</div>' +
                '<div class="col-sm-5">' +
                '<input type="text" class="form-control" name="segundovalor[]" placeholder="Segundo Valor" minlegth="1" required>' +
                '</div>' +
                '<div class="col-sm-1">' +
                '<a  class="btn btn-danger btn-outline eliminar-respuesta"> <span class="glyphicon glyphicon-remove"> </span> </a>' +
                '</div>' +
                '</div>';
        $('#respuestas').append(template);
        
        $('form .eliminar-respuesta').on('click', function () {
            var respuestas = $(this).closest('#respuestas').find('.form-group');
            if(respuestas.length > 2) {
                $(this).closest('.form-group').remove();
            }
        });
    });

    $('form .eliminar-respuesta').on('click', function () {
        $('form .eliminar-respuesta').on('click', function () {
            var respuestas = $(this).closest('#respuestas').find('.form-group');
            if(respuestas.length > 2) {
                $(this).closest('.form-group').remove();
            }
        });
    });

    $('#categoria').change(function(){
        var id = $(this).val();
        if(id == false) {
            var res = '<option value=""> Seleccione una opcion </option>';
            $( "#subcategorias" ).html( res );
            return;
        }
        
        var id = $(this).val();
        var url = "{{url('/subcategorias/')}}";
        
        $.get( url +"/" +id , function( data ) {
            var res = '<option value=""> Seleccione una opcion </option>';
            $.each( data , function(key, value){
                res += '<option value="'+value.id+'">'+ value.categoria +'</option>';
            });
            
            $( "#subcategorias" ).html( res );
            
        });
    });
</script>
@endsection
