@extends('layouts.principal')
@section('content')

  <div class="row">
    <div class="col-sm-12">
        <h1 class="page-header">Preguntas de Emparejamiento </h1>
    </div>
      <div class="div col-lg-12">
          <a class="btn btn-primary" href="{{ url('/crear-pregunta-orden')}}"> <span class="glyphicon glyphicon-plus-sign"> </span> Agregar</a>
      </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
<div class="row">
    <div class="col-lg-12">
      @include('alerts.success')
      @include('alerts.required')

      <table class="table table-striped table-bordered">
          <thead>
              <tr>
                  <th>Enunciado </th>
                  <th>No. Respuestas </th>
                  <th>Categoria </th>
                  <th>Sub Categoria </th>
                  <th>Acciones </th>
              </tr>
          </thead>
          <tbody>
              @foreach($preguntas as $pregunta)
               <tr>
                   <td> {{ $pregunta->enunciado }}</td>
                   <td> {{ count ($pregunta->respuestas) }}</td>
                   @if($pregunta->subcategoria)
                    <td>{{ $pregunta->subcategoria->categoriap->categoria }}</td>
                    <td>{{ $pregunta->subcategoria->categoria }}</td>
                   @else
                    <td> sin categoria </td>
                    <td> sin sub categoria</td>
                   @endif
                   <td>

                       <a href="#" class="btn btn-success btn-circle" data-tooltip="Ver detalle"><i class="fa fa-search"></i></a>

                       <a href="{{ url('editar-pregunta-emparejamiento/'. $pregunta->id) }}" class="btn btn-primary btn-circle" data-tooltip="Editar pregunta"><i class="glyphicon glyphicon-edit"></i></a>

                       <a href="{{ url('resolver-emparejamiento/'. $pregunta->id.'/create') }}" class="btn btn-warning btn-circle" data-tooltip="Resolver y corregir"><i class="fa fa-mortar-board "></i></a>

                       <a href="{{ url('eliminar-pregunta-emparejamiento/'. $pregunta->id) }}" class="btn btn-danger btn-circle" data-tooltip="Eliminar pregunta" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="glyphicon glyphicon-trash"></i></a></div>
                   </td>
               </tr>
               @endforeach
          </tbody>
      </table>


    </div>
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->
{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
{!!Html::script('bower_components/raphael/raphael-min.js')!!}
{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
{!!Html::script('js/morris-data.js')!!}
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
@endsection
