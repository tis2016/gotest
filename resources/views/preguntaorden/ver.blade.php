@extends('layouts.principal')
@section('content')

  <div class="row">
    <div class="col-sm-12">
        <h1 class="page-header">Pregunta de Emparejamiento</h1>
    </div>
      <div class="col-sm-offset-8 col-sm-4">
        <a class="btn btn-primary btn-outline" href="{{ url('editar-pregunta-emparejamiento/'. $pregunta->id) }}"> <span class="glyphicon glyphicon-edit"> </span> Editar</a>
        <a class="btn btn-danger btn-outline" href="{{ url('eliminar-pregunta-emparejamiento/'. $pregunta->id) }}"> <span class="glyphicon glyphicon-trash"> </span> Eliminar</a>
        <a class="btn btn-default" href="{{ url('/pregunta-emparejamiento') }}"> <span class="glyphicon glyphicon-list"></span> Lista </a>
      </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
<div class="row">
    <div class="col-lg-12">
      @include('alerts.success')
      @include('alerts.required')
    @if($pregunta->subcategoria)
    <div class="">
        <label for="enunciado" class="control-label text-primary">Categoria</label>
        <div class="">
            <p> {{ ($pregunta->subcategoria->categoriap->categoria) }}</p>
        </div>
    </div>
    <div class="">
        <label for="enunciado" class="control-label text-primary">Sub Categoria</label>
        <div class="">
            <p> {{ $pregunta->subcategoria->categoria }}</p>
        </div>
    </div>
    @endif
    <div class="">
        <label for="enunciado" class="control-label text-primary">Enunciado</label>
        <div class="">
            <p> {{ $pregunta->enunciado }}</p>
        </div>
    </div>
    <label for="respuesta" class="control-label text-primary">Respuestas</label>
      <table class="table">
          <thead> 
              <tr>
                  <th>Primer Valor </th>
                  <th>Segundo Valor </th>
              </tr>
          </thead>
          <tbody>
              @foreach($respuestas as $respuesta)
               <tr>
                   <td> {{ $respuesta->primervalor }}</td>
                   <td> {{ $respuesta->segundovalor }}</td>
               </tr>
               @endforeach
          </tbody>
      </table>

                                
    </div>
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->
{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
{!!Html::script('bower_components/raphael/raphael-min.js')!!}
{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
{!!Html::script('js/morris-data.js')!!}
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
@endsection
