@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Detalles de su respuesta</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<div class="table-responsive table-bordered">
  <table class="table">
    <thead>
      <tr>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><h4><b>Enunciado: </b></h4></td>
        <td><h5>{{ $pregunta->enunciado }}</h5></td>
      </tr>
      <tr>
        <td><h4><b>Categoria: </b></h4></td>
        <td><h5></h5>{{ $categoria[0]->categoria }}</td>
      </tr>
      <tr>
        <td><h4><b>Sub-categoria: </b></h4></td>
        <td><h5>{{ $subcategoria[0]->subcategoria }}</h5></td>
      </tr>
        <td><h4><b>Relaciones : </b></h4></td>
        <td>
          @foreach($pregunta->respuestas as $relacion)
                  <div class="col-lg-5">
                    <h4><p class="text-success" data-tooltip="Respuesta correcta">{{ $relacion->primervalor }}</p></h4>
                  </div>
                  <div class="col-lg-2">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  </div>
                  <div class="col-lg-5">
                    <h4><p class="text-success" data-tooltip="Respuesta correcta">{{ $relacion->segundovalor }}</p></h4>
                  </div>
          @endforeach
          </td>
      </tr>
      <tr>
        <td><h4><b>Puntaje o valor: </b></h4></td>
        <td>
          <h5>El puntaje es seg&uacute;n a la cantidad de relaciones que acertaste.</h5>
        </td>
      </tr>
      <tr>
        <td><h4><b>Tu respuesta: </b></h4></td>
        <td>

            @for($i=0; $i < count($ultimasrespuestas); $i++)
              @if($ultimasrespuestas[$i]->correcto == 1)
              <div class="col-lg-5">
                <h4><p class="text-success" data-tooltip="Respuesta correcta">{{ $ultimasrespuestas[$i]->primervalor_respuesta }}</p></h4>
              </div>
              <div class="col-lg-2">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              </div>
              <div class="col-lg-5">
                <h4><p class="text-success" data-tooltip="Respuesta correcta">{{ $ultimasrespuestas[$i]->segundovalor_respuesta }}</p></h4>
              </div>
              @endif
              @if($ultimasrespuestas[$i]->correcto == 0 && $ultimasrespuestas[$i]->respondida == 1 )
              <div class="col-lg-5">
                <h4><p class="text-success" data-tooltip="Respuesta incorrecta">{{ $ultimasrespuestas[$i]->primervalor_respuesta }}</p></h4>
              </div>
              <div class="col-lg-2">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              </div>
              <div class="col-lg-5">
                <h4><p class="text-danger" data-tooltip="Respuesta incorrecta">{{ $ultimasrespuestas[$i]->segundovalor_respuesta }}</p></h4>
              </div>
              @endif
              @if($ultimasrespuestas[$i]->respondida == 0 )
              <div class="col-lg-5">
                <h4><p class="text-success" data-tooltip="Respuesta incorrecta">{{ $ultimasrespuestas[$i]->primervalor_respuesta }}</p></h4>
              </div>
              <div class="col-lg-2">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              </div>
              <div class="col-lg-5">
                <h4><p class="text-danger" data-tooltip="Sin Respuesta">{{ $ultimasrespuestas[$i]->segundovalor_respuesta }}</p></h4>
              </div>
              @endif
            @endfor
        </td>
      </tr>
      <tr>
        <td><h4><b>Su puntaje: </b></h4></td>
        <td>
          @for($i=0; $i < count($ultimasrespuestas); $i++)
            @if($ultimasrespuestas[$i]->correcto == 1)
              <h4><p class="text-info" data-tooltip="nota de la respuesta">{{ $ultimasrespuestas[$i]->peso }}</p></h4>
            @else
            <h4><p class="text-danger" data-tooltip="nota de la respuesta">{{ 0 }}</p></h4>
            @endif
          @endfor

        </td>
      </tr>

    </tbody>
  </table>
</div>
<h1 class="page-header"></h1>
<a href="{!! URL::to('pregunta-emparejamiento') !!}" class="btn btn-primary"> Regresar<i class="fa fa-back"></i></a><br><br><br><br>
</div>
</div>
</div>
@endsection
