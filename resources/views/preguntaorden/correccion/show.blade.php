@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Pregunta de Emparejamiento</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
  <div class="row">
    <div class="col-lg-10">
      {!!Form::open(['route'=>'resolver-emparejamiento.store', 'method'=>'POST'])!!}
      <input name="idPregunta" type="hidden" value="{{ $pregunta->id}}">

      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <b>Pregunta # 1</b>
          </div>
          <div class="panel-body">
            <h3>{!! Form::label('enunciado', $pregunta->enunciado) !!}</h3>
            <div class="row"><br /></div>
            {!! Form::label('respuesta', "Su respuesta:") !!}<br/>
            @foreach($respuestas as $respuesta)
            <div class="row"><br />
            <label for=""class="col-lg-8">{{ $respuesta->primervalor }}</label>
              <div class="col-lg-4">
                <input name="id_respuesta_primer[]" type="hidden" value="{{ $respuesta->id }}">
                <select id="respuestasEscoger" name="id_respuesta_segundo[]"class="form-control" placeholder="Seleccione una relación..">

                  @foreach($segundo as $res)
                    <option value="{{ $res->id }}" >{{ $res->segundovalor }}</option>
                  @endforeach
                    <option selected="selectd" value="-1" >sin respuesta ..........</option>
                </select>
              </div>
              </div>
            @endforeach
          </div>
          <div class="panel-footer" align="right">
            <a href="#" class="btn btn-primary btn-circle" data-tooltip="Atras"><i class="fa fa-chevron-left "></i></a>
            <a href="#" class="btn btn-primary btn-circle" data-tooltip="Adelante"><i class="fa fa-chevron-right"></i></a>
            <a href="{!! URL::to('pregunta-emparejamiento') !!}" class="btn btn-warning btn-circle" data-tooltip="Regresar a inicio"><i class="fa fa-home "></i></a>
            <button type="submit" class="btn btn-success btn-circle" data-tooltip="Enviar para corregir"><i class="fa fa-check"></i>
            </button>


          </div>
        </div>
      </div>
      {!!Form::close()!!}
      <!-- /.col-lg-8 -->
      <!-- /.col-lg-4 -->
    </div>
        </div>
    </div>
    @include('layouts.scripts')
    @include('layouts.scripts-sel-simple-create')
    @endsection
