@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Pregunta de seleccion simple</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
  <div class="row">
    <div class="col-lg-10">
      {!!Form::open(['route'=>'correccion-sel-simple.store', 'method'=>'POST'])!!}
      {!!Form::hidden('pregunta_id', $pregunta[0]->id)!!}
      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <b>Pregunta # 1</b>
          </div>
          <div class="panel-body">
            <h3>{!! Form::label('enunciado', $pregunta[0]->enunciado) !!}</h3>
            <div class="row"><br /></div>
            {!! Form::label('respuesta', "Su respuesta:") !!}<br/>
            <table align="left"><br />
              <?php
              for ($i=0; $i < count($opciones); $i++) {
              echo "<tr>
                <td>
                  <h5><label for='opcion'>".$opciones[$i]->opcion."</label></h5>
                </td>
                <td>&nbsp;&nbsp;<input type='radio' name='opcion' id='opcion' value='".$opciones[$i]->opcion."' /></td>
              </tr>";
              }
              ?>
            </table>
          </div>
          <div class="panel-footer" align="right">
            <a href="#" class="btn btn-primary btn-circle" data-tooltip="Atras"><i class="fa fa-chevron-left "></i></a>
            <a href="#" class="btn btn-primary btn-circle" data-tooltip="Adelante"><i class="fa fa-chevron-right"></i></a>
            <a href="{!! URL::to('falso-verdadero') !!}" class="btn btn-warning btn-circle" data-tooltip="Regresar a inicio"><i class="fa fa-home "></i></a>
            <button type="submit" class="btn btn-success btn-circle" data-tooltip="Enviar para corregir"><i class="fa fa-check"></i>
            </button>
            
            
          </div>
        </div>
      </div>
      {!!Form::close()!!}
      <!-- /.col-lg-8 -->
      <!-- /.col-lg-4 -->
    </div>
  </div>
</div>
@include('layouts.scripts')
@include('layouts.scripts-sel-simple-create')
@endsection