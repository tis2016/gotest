@extends('layouts.principal')
@section('content')
  <div class="row">
  <div class="col-lg-12">
      <h1 class="page-header">Preguntas registradas</h1>
  </div>
  <!-- /.col-lg-12 -->
  </div>
  <div class="row">
      <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                  Preguntas registradas en la base de datos
                  <div class="pull-right">
                                <div class="btn-group">
                                    <a class="glyphicon-plus btn btn-outline btn-info btn-xs" href="{!!URL::to('seleccion-simple/create')!!}"> Crear nueva</a>
                                </div>
                            </div>
                  </button>
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                  <div class="dataTable_wrapper">
                      <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                              <tr>
                                  <th>Enunciado</th>
                                  <th>Categoria</th>
                                  <th>Sub-categoria</th>
                                  <th>Fecha creacion</th>
                                  <th>Accion</th>
                              </tr>
                          </thead>
                          {{-- <tbody id="datos"> --}}
                            @foreach($preguntas as $pregunta)
                            <tbody>
                            <tr>
                              <td>{{str_limit($pregunta->enunciado,$limit = 30,$end='...')}}</td>
                              <td>{{$pregunta->categoria}}</td>
                              <td>{{$pregunta->subcategoria}}</td>
                              <td>{{$pregunta->created_at}}</td>
                              <td>{!!link_to_route('respuestaSeleccion.create', $title = 'Resolver', $parameters = $pregunta->id, $attributes = ['class'=>'btn btn-outline btn-primary'])!!}

                             </td>
                            </tr>
                            </tbody>
                            @endforeach
                          {{-- </tbody> --}}
                      </table>
                      {!!$preguntas->render()!!}
                      @include('alerts.success')
                      @include('alerts.required')
                  </div>
              </div>
            </div>
          </div>
        </div>
@endsection
