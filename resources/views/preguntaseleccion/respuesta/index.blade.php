@extends('layouts.principal')
@section('content')
  <div class="row">
  <div class="col-lg-12">
      <h1 class="page-header">Correccion de Respuestas</h1>
  </div>
  <!-- /.col-lg-12 -->
  </div>
  <div class="row">
      <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                  Respuestas registradas en la base de datos
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                  <div class="dataTable_wrapper">
                      <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                              <tr>
                                  <th>Pregunta</th>
                                  <th>Respuesta</th>
                                  <th>Correccion</th>

                              </tr>
                          </thead>
                          {{-- <tbody id="datos"> --}}
                            @foreach($preguntas->respuestasSelMultiple as $pregunta)
                            <tbody>
                              @foreach($respuestas as $respuesta)
                                  <tr>

                                      <td>{{$pregunta->enunciado}}</td>

                                        <td>{{$respuesta->respuesta}}</td>
                                        <td>{{$respuesta->correcto}}</td>


                                  </tr>
                              @endforeach
                            </tbody>
                            @endforeach
                          {{-- </tbody> --}}
                      </table>

                      @include('alerts.success')
                      @include('alerts.required')
                  </div>
              </div>
            </div>
          </div>
        </div>
@endsection
