@extends('layouts.principal')
@section('content')
  <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Registrar pregunta de seleccion simple</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
<div class="row">
    <div class="col-lg-6">
@include('alerts.success')
@include('alerts.required')
{!!Html::style('css/style2.css')!!}
{!!Form::open(['route'=>'seleccion-simple.store', 'method'=>'POST','onsubmit'=>'return ValidationEventSS()','files' => true])!!}
{!!Form::hidden('usuarios_id', Auth::user()->id)!!}
@include('preguntaseleccion.form.pregunta')
{!!Form::close()!!}
</div>
</div>
<!-- /.col-lg-8 -->
<!-- /.col-lg-4 -->
</div>
@include('layouts.scripts')
@include('layouts.scripts-sel-simple-create')
@endsection
