
@extends('layouts.principal')
@section('content')
{{-- */$examen_con_pregunta=false;/* --}}
@if(isset($_GET['id']))
  @foreach($pregunta_examen as $p)
      @if($p->examen_id == $_GET['id'])        
        {{-- */$examen_con_pregunta=true;/* --}}        
      @endif
  @endforeach
@endif

{!!Html::style('css/tooltips.css')!!}
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Añadir preguntas a examen</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
@if($examen_con_pregunta)
  <p>El examen ya contiene preguntas.</p>
@else
@if(isset($_GET['examen']))
  <p>Las preguntas fueron añadidas al examen</p>
@else

@if(isset($_GET['preg_abierta'])||
isset($_GET['preg_fv'])||
isset($_GET['preg_ordenamiento'])||
isset($_GET['preg_sel_simple'])||
isset($_GET['preg_sel_multiple'])||
isset($_GET['preg_complecion'])||
isset($_GET['preg_emparejamiento']))
<form action="" method="POST" class="form-inline" role="form">
  <input type="text" name="_token" class="hidden form-control" value="{{Csrf_token()}}" >
  <div class="table-responsive">
    <table class="table table-striped table-bordered table-hover dataTable no-footer">
      <thead>
        <tr role="row">
          <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
            aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 172px;">Tipo pregunta
          </th>
          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
            aria-label="Browser: activate to sort column ascending" style="width: 204px;">Enunciado
          </th>
          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
            aria-label="Platform(s): activate to sort column ascending" style="width: 105px;">Prioridad de orden
          </th>
          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
            aria-label="Engine version: activate to sort column ascending" style="width: 145px;">Puntaje
          </th>
        </tr>
      </thead>
      <tbody>
        @foreach($preguntas as $pregunta)
        {{-- */$band=false;/* --}}
          @if($pregunta->alias == "preg_abierta")
            @if(isset($_GET['preg_abierta']))
              @if(in_array($pregunta->id,$_GET['preg_abierta']))
                {{-- */$band=true;/* --}}
              @endif
            @endif
          @endif
          @if($pregunta->alias == "preg_fv")
            @if(isset($_GET['preg_fv']))
              @if(in_array($pregunta->id,$_GET['preg_fv']))
                {{-- */$band=true;/* --}}
              @endif
            @endif
          @endif
          @if($pregunta->alias == "preg_ordenamiento")
            @if(isset($_GET['preg_ordenamiento']))
              @if(in_array($pregunta->id,$_GET['preg_ordenamiento']))
                {{-- */$band=true;/* --}}
              @endif
            @endif
          @endif
          @if($pregunta->alias == "preg_sel_simple")
            @if(isset($_GET['preg_sel_simple']))
              @if(in_array($pregunta->id,$_GET['preg_sel_simple']))
                {{-- */$band=true;/* --}}
              @endif
            @endif
          @endif
          @if($pregunta->alias == "preg_sel_multiple")
            @if(isset($_GET['preg_sel_multiple']))
              @if(in_array($pregunta->id,$_GET['preg_sel_multiple']))
                {{-- */$band=true;/* --}}
              @endif
            @endif
          @endif
          @if($pregunta->alias == "preg_complecion")
            @if(isset($_GET['preg_complecion']))
              @if(in_array($pregunta->id,$_GET['preg_complecion']))
                {{-- */$band=true;/* --}}
              @endif
            @endif
          @endif
          @if($pregunta->alias == "preg_emparejamiento")
            @if(isset($_GET['preg_emparejamiento']))
              @if(in_array($pregunta->id,$_GET['preg_emparejamiento']))
                {{-- */$band=true;/* --}}
              @endif
            @endif
          @endif
          @if($band)
          <tr>
            <td>{{$pregunta->tipo}}</td>
            <input type='text' name='tipo[]' class='hidden' value='{{$pregunta->tipo}}'>
            <td>{{str_limit($pregunta->enunciado,$limit = 50,$end='...')}}</td>
            <input type='text' name='pregunta_id[]' class='hidden' value='{{$pregunta->id}}'>
            <td><input type='number' name='orden[]' id='input' class='form-control' value='1' min='1' max='5'></td>
            <td><input type='number' name='valor[]' id='input' class='form-control' value='0' min='0' max='1000'></td>
          </tr>
          @endif
          @endforeach
        </tbody>
    </table>
  </div>
  <input type='text' name='examen' class='hidden' value='{{$_GET["id"]}}'>
  <button type="submit" class="btn btn-primary">Guardar</button>
</form>
  @else
  <form action="" method="GET" class="form-inline" role="form">
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover dataTable no-footer">
        <thead>
          <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
              aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 172px;">Tipo pregunta
            </th>
            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
              aria-label="Browser: activate to sort column ascending" style="width: 204px;">Enunciado
            </th>
            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
              aria-label="Platform(s): activate to sort column ascending" style="width: 105px;">Categoria
            </th>
            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
              aria-label="Engine version: activate to sort column ascending" style="width: 145px;">Subcategoria
            </th>
            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
              aria-label="CSS grade: activate to sort column ascending" style="width: 110px;">Fecha creación
            </th>
            <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
              aria-label="CSS grade: activate to sort column ascending" style="width: 60px;">Acción
            </th>
          </tr>
        </thead>
        <tbody>
          
          @foreach($preguntas as $pregunta)
          <tr>
            <td>{{$pregunta->tipo}}</td>
            <td>{{str_limit($pregunta->enunciado,$limit = 50,$end='...')}}</td>
            <td>{{$pregunta->categoria}}</td>
            <td>{{$pregunta->subcategoria}}</td>
            <td>{{$pregunta->created_at}}</td>
            <td><div class='checkbox'>
              <label>
                <input type='checkbox' name='{{$pregunta->alias}}[]' value='{{$pregunta->id}}'>
                Añadir
              </label>
            </div></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <input type='text' name='id' class='hidden' value='{{$_GET["id"]}}'>
    <button type="submit" class="btn btn-primary">Seleccionar preguntas</button>
  </form>
@endif
@endif
@endif
@endsection
