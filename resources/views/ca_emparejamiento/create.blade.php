@extends('layouts.principal')
@section('content')

  <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Registrar tipo de pregunta Falso / Verdadero</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
<div class="row">
    <div class="col-lg-6">
      @include('alerts.success')
      @include('alerts.required')
      
      <form action="{{ url('crear-falso-verdadero')}}" method="POST" class="form-horizontal">
                                {!! csrf_field() !!}
                                {!!Form::hidden('usuarios_id', Auth::user()->id)!!}
                                <div class="">
                                    <label for="enunciado" class="control-label text-primary">Enunciado</label>
                                    <div class="">
                                        <textarea class="form-control" name="enunciado" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="">
                                    <label for="enunciado" class="control-label text-primary">Respuesta Correcta</label>               
                                    <div class="checkbox">
                                        <input type="radio" name="respuesta" value="1">Verdadero
                                    </div>
                                    <div class="checkbox">
                                        <input type="radio" name="respuesta" value="0"> Falso
                                    </div>
                                    <div class="checkbox">
                                        <input type="radio" name="respuesta" value="" checked="checked"> Responder mas tarde
                                    </div>
                                </div>
                                
                                <br>
                                <div class="">
                                    <div class="">
                                        <button type="submit" class="btn btn-primary"> Guardar
                                        </button>
                                        <button type="reset" class="btn btn-primary"> Cancelar
                                        </button>
                                    </div>
                                </div>
                            </form>
      
    </div>
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->
{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
{!!Html::script('bower_components/raphael/raphael-min.js')!!}
{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
{!!Html::script('js/morris-data.js')!!}
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
@endsection
