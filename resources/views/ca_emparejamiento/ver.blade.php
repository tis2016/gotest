@extends('layouts.principal')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <h3 class="page-header">Correccion automática de pregunta de tipo emparejamiento</h3>
    </div>
</div>
<div class="panel-body">
    <?php $correctos=0; $incorrectos=0; ?>
    @foreach ($op_emp as $op)
        @foreach ($ca_emp as $ca)
            @if($op->primervalor === $ca->primervalor)
                @if($op->segundovalor == $ca->segundovalor)
                    <?php $correctos++; ?>                     
                    <div class="row">
                        <div class="emparejados-correcto text-center">
                            <div class="col-sm-6">                
                                <h4>Respuesta usuario:</h4>
                                <div class="row">
                                    <div class="col-xs-5">
                                        {{$ca->primervalor}}
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-xs-5">
                                        {{$ca->segundovalor}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">  
                                <h4>Respuesta correcta:</h4>
                                <div class="row">
                                    <div class="col-xs-5">
                                       {{$op->primervalor}}
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-xs-5">
                                       {{$op->segundovalor}}
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                @else
                    <?php $incorrectos++; ?>  
                    <div class="row">
                        <div class="emparejados-incorrecto text-center">
                            <div class="col-sm-6">                
                                <h4>Respuesta usuario:</h4>
                                <div class="row">
                                    <div class="col-xs-5">
                                        {{$ca->primervalor}}
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-xs-5">
                                        {{$ca->segundovalor}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">  
                                <h4>Respuesta correcta:</h4>
                                <div class="row">
                                    <div class="col-xs-5">
                                       {{$op->primervalor}}
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-xs-5">
                                       {{$op->segundovalor}}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>             
                @endif
            @endif
        @endforeach
    @endforeach

    <h4>Numero total de correctos = {{$correctos}}</h4>
    <h4>Numero total de incorrectos = {{$incorrectos}}</h4>
    <?php $total= $correctos+$incorrectos; $porcentajeAcierto = $correctos*100/$total;?>  
    <h4>Porcentaje de acierto= {{$porcentajeAcierto}}%</h4>

</div>
<!-- /.row -->
{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
{!!Html::script('bower_components/raphael/raphael-min.js')!!}
{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
{!!Html::script('js/morris-data.js')!!}
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
@endsection