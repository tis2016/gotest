@extends('layouts.principal')
@section('content')
  {!!Form::model($pregunta,['route'=>['abierta.update',$pregunta->id],'method'=>'PUT','files' => true])!!} <!--enrutando los datos -->
  <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar pregunta</h1>
    </div>
</div>
<div class="panel-body">
<div class="row">
    <div class="col-lg-6">
      {!!Html::style('css/style2.css')!!}
      {!!Form::label('categoria','Categoria: ')!!}
      {!!Form::select('categorias_id', $categoria,null,['id'=>'categoriaAbiertas','class'=>'form-control',
                      'placeholder'=>'Seleccione una opcion..','required'])!!}<br>
      {!!Form::label('subcategoria','Sub-categoria: ')!!}
      {!!Form::select('sub_categorias_id', $subcategoria,null,['id'=>'subcategoriaAbiertas','class'=>'form-control',
											'placeholder'=>'Seleccione una opcion..','required'])!!}<br>
      {!!Form::label('enunciado','Enunciado: ')!!}
      @include('alerts.success')
      @include('alerts.required')
      {!!Form::textarea('enunciado',null,['class'=>'form-control', 'rows' => 3,'required']) !!}<br>
      {!!Form::label('respuesta','Respuesta(s) correcta(s): ')!!}
      {!!Form::textarea('respuesta',null,['class'=>'form-control', 'rows' => 3]) !!}<br>
      {!!Form::label('img','Imagen:')!!}
      {!!Form::file('infopath',['enctype'=>'multipart/form-data'])!!}<br>
      <br>{!!Form::submit('Registrar',['class'=>'btn btn-outline btn-primary'])!!}<br><br><br>
      {!!Form::close()!!}
    </div>
    </div>
</div>
@include('layouts.scripts')
@endsection
