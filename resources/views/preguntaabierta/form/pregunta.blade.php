<!-- PREGUNTAS ABIERTAS -->
{!!Form::label('categoria','Categoria: ')!!}
{!!Form::select('categorias_id', $categorias,null,['id'=>'categoriaAbiertas','class'=>'form-control','placeholder'=>'Seleccione una opcion..','required'])!!}<br>
{!!Form::label('subcategoria','Sub-categoria: ')!!}
{!!Form::select('sub_categorias_id',['placeholder'=>'Seleccione una opcion..'],null,['id'=>'subcategoriaAbiertas','class'=>'form-control'])!!}<br>

{!!Form::label('enunciado','Enunciado: ')!!}
@include('alerts.success')
@include('alerts.required')
{!!Form::textarea('enunciado',null,['class'=>'form-control', 'rows' => 3,'required','min'=>3]) !!}<br>
{!!Form::label('respuesta','Respuesta(s) correcta(s): ')!!}
{!!Form::textarea('respuesta',null,['class'=>'form-control', 'rows' => 3]) !!}<br>
{!!Form::label('img','Imagen:')!!}
{!!Form::file('infopath',['enctype'=>'multipart/form-data'])!!}<br>
<br>{!!Form::submit('Registrar',['class'=>'btn btn-outline btn-primary'])!!}
{!!Form::close()!!}
