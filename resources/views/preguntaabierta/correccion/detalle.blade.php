@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Detalles de su respuesta</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
  <div class="row">
    <div class="col-lg-10">
      <?php
      $enunciado = $pregunta[0]->enunciado;
      $categoria = $pregunta[0]->categoria;
      $subcategoria = $pregunta[0]->subcategoria;
      $respuesta_original = $pregunta[0]->respuesta;
      $infopath = $pregunta[0]->infopath;
      $created_at = $pregunta[0]->created_at;
      $updated_at = $pregunta[0]->updated_at;
      ?>
      <div class="table-responsive table-bordered">
        <table class="table">
          <thead>
            <tr>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><h4><b>Enunciado: </b></h4></td>
              <td><h5>{{ $enunciado }}</h5></td>
            </tr>
            <tr>
              <td><h4><b>Categor&iacute;a: </b></h4></td>
              <td><h5>{{ $categoria }}</h5></td>
            </tr>
            <tr>
              <td><h4><b>Sub-categor&iacute;a: </b></h4></td>
              <td><h5>{{ $subcategoria }}</h5></td>
            </tr>
            <td><h4><b>Imagen: </b></h4></td>
            <td>@if($infopath == null)
              <img src="infopath/no-image.jpg" border="4" style="width:240px" />
              @else
              <img src="infopath/{{$infopath}}" border="4" style="width:240px" />
              @endif
            </td>
          </tr>
          <tr>
            <td><h4><b>Respuesta: </b></h4></td>
            @if('' == $respuesta_original)
            <td>
              <h4><p class="text-danger" >Sin respuesta</p></h4>
            </td>
            @else
            <td>
              <h4><p class="text-success" >{{ $respuesta_original }}</p></h4>
            </td>
            @endif
          </tr>
          <tr>
            <td><h4><b>Puntaje o valor: </b></h4></td>
            <td>
              <h5><p class="text-danger" >Puntuaci&oacute;n no definida por el sistema.</p></h5>
            </td>
          </tr>
          <tr>
            <td><h4><b>Su respuesta: </b></h4></td>
            <td>
              @if('' == $respuesta)
              <h4><p class="text-danger" >Sin respuesta</p></h4>
              @else
              <h4><p class="text-success" >{{ $respuesta }}</p></h4>
              @endif
            </td>
          </tr>
          <tr>
            <td><h4><b>Puntaje: </b></h4></td>
            <td>
              <h4><p class="text-info" >-1</p></h4>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <h1 class="page-header"></h1>
    <a href="{!! URL::to('abierta') !!}" class="btn btn-primary"> Regresar<i class="fa fa-back"></i></a><br><br><br><br>
  </div>
      </div>
    </div>
    </div>
  @include('layouts.scripts')
  @endsection