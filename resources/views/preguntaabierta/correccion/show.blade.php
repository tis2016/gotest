@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Pregunta abierta</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
  <div class="row">
    <div class="col-lg-10">
      {!!Form::open(['route'=>'correccion-abiertas.store', 'method'=>'POST','files' => true])!!}
      {!!Form::hidden('pregunta_id', $pregunta->id)!!}
      <div class="col-lg-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <b>Pregunta # 1</b>
          </div>
          <div class="panel-body">
            <h3>{!! Form::label('enunciado', $pregunta->enunciado) !!}</h3>
            <div class="row" align="center"><br />
              @if($pregunta->infopath == null)
              <img src="http://globalsystem.cs.umss.edu.bo/infopath/no-image.jpg" border="4" style="width:240px" />
              @else
              <img src="http://globalsystem.cs.umss.edu.bo/infopath/{{$pregunta->infopath}}" border="4" style="width:240px" />
              @endif
            </div><br />
            {!! Form::label('respuesta', "Su respuesta:") !!}
            {!!Form::textarea('respuesta',null,['class'=>'form-control', 'rows' => 3]) !!}
          </div>
          <div class="panel-footer" align="right">
            <a href="#" class="btn btn-primary btn-circle" data-tooltip="Atras"><i class="fa fa-chevron-left "></i></a>
            <a href="#" class="btn btn-primary btn-circle" data-tooltip="Adelante"><i class="fa fa-chevron-right"></i></a>
            <a href="{!! URL::to('abierta') !!}" class="btn btn-warning btn-circle" data-tooltip="Regresar a inicio"><i class="fa fa-home "></i></a>
            <button type="submit" class="btn btn-success btn-circle" data-tooltip="Enviar para corregir"><i class="fa fa-check"></i>
            </button>
          </div>
        </div>
      </div>
      {!!Form::close()!!}
      <!-- /.col-lg-8 -->
      <!-- /.col-lg-4 -->
    </div>
  </div>
</div>
@include('layouts.scripts')
@endsection