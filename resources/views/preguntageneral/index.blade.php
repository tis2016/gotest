@extends('layouts.principal')
@section('content')
  {!!Html::style('css/tooltips.css')!!}
  <div class="row">
  <div class="col-lg-12">
      <h1 class="page-header">Preguntas registradas</h1>
  </div>
  <!-- /.col-lg-12 -->
  </div>
  <div class="row">
      <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading">
                  Preguntas registradas en la base de datos
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                  <div class="dataTable_wrapper">
                      <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info">
                        <thead>
                            <tr role="row">
                              <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 172px;">Tipo pregunta
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Browser: activate to sort column ascending" style="width: 204px;">Enunciado
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Platform(s): activate to sort column ascending" style="width: 105px;">Categoria
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Engine version: activate to sort column ascending" style="width: 145px;">Subcategoria
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="CSS grade: activate to sort column ascending" style="width: 110px;">Fecha creación
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="CSS grade: activate to sort column ascending" style="width: 60px;">Acción
                              </th>
                            </tr>
                        </thead>
                        @foreach($preguntas as $pregunta)
                        <tbody>
                        <tr class="gradeA odd" role="row">
                          <td>{{$pregunta->tipo}}</td>
                          <td>{{str_limit($pregunta->enunciado,$limit = 50,$end='...')}}</td>
                          <td>{{$pregunta->categoria}}</td>
                          <td>{{$pregunta->subcategoria}}</td>
                          <td>{{$pregunta->created_at}}</td>
                          @if($pregunta->alias == "preg_abierta")
                            {{-- <td>{!!link_to_route('abierta.edit', $title = 'Editar', $parameters = $pregunta->id, $attributes = ['class'=>'btn btn-outline btn-primary'])!!} --}}
                           <td>
                                <a href="{!! URL::route('abierta.edit', array($pregunta->id)) !!}" class="btn btn-primary btn-circle" data-tooltip="Editar pregunta"><i class="fa fa-list"></i></a>
                                <a href="{!! URL::route('abierta.destroy', array($pregunta->id)) !!}" class="btn btn-danger btn-circle" data-tooltip="Eliminar pregunta" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="fa fa-times"></i></a>
                           </td>
                          @endif
                          @if($pregunta->alias == "preg_fv")
                            <td>
                                 <a href="{!! URL::route('falso-verdadero.edit', array($pregunta->id)) !!}" class="btn btn-primary btn-circle" data-tooltip="Editar pregunta"><i class="fa fa-list"></i></a>
                                 <a href="{!! URL::route('falso-verdadero.destroy', array($pregunta->id)) !!}" class="btn btn-danger btn-circle" data-tooltip="Eliminar pregunta" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="fa fa-times"></i></a>
                            </td>
                          @endif
                          @if($pregunta->alias == "preg_ordenamiento")
                            <td>
                                 <a href="{!! URL::route('ordenamiento.edit', array($pregunta->id)) !!}" class="btn btn-primary btn-circle" data-tooltip="Editar pregunta"><i class="fa fa-list"></i></a>
                                 <a href="{!! URL::route('ordenamiento.destroy', array($pregunta->id)) !!}" class="btn btn-danger btn-circle" data-tooltip="Eliminar pregunta" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="fa fa-times"></i></a>
                            </td>
                          @endif
                          @if($pregunta->alias == "preg_sel_simple")
                            <td>
                                 <a href="{!! URL::route('seleccion-simple.edit', array($pregunta->id)) !!}" class="btn btn-primary btn-circle" data-tooltip="Editar pregunta"><i class="fa fa-list"></i></a>
                                 <a href="{!! URL::route('seleccion-simple.destroy', array($pregunta->id)) !!}" class="btn btn-danger btn-circle" data-tooltip="Eliminar pregunta" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="fa fa-times"></i></a>
                            </td>
                          @endif
                          @if($pregunta->alias == "preg_sel_multiple")
                            <td>
                                 <a href="{!! URL::route('seleccion-multiple.edit', array($pregunta->id)) !!}" class="btn btn-primary btn-circle" data-tooltip="Editar pregunta"><i class="fa fa-list"></i></a>
                                 <a href="{!! URL::route('seleccion-simple.destroy', array($pregunta->id)) !!}" class="btn btn-danger btn-circle" data-tooltip="Eliminar pregunta" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="fa fa-times"></i></a>
                            </td>
                          @endif
                          @if($pregunta->alias == "preg_complecion")
                            <td>
                                 <a href="{{ url('editar-pregunta-complementacion/'. $pregunta->id) }}" class="btn btn-primary btn-circle" data-tooltip="Editar pregunta"><i class="fa fa-list"></i></a>
                                 <a href="{{ url('eliminar-pregunta-complementacion/'. $pregunta->id) }}" class="btn btn-danger btn-circle" data-tooltip="Eliminar pregunta" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="fa fa-times"></i></a>
                            </td>
                          @endif
                          @if($pregunta->alias == "preg_emparejamiento")
                            <td>
                                 <a href="#" class="btn btn-primary btn-circle" data-tooltip="Editar pregunta"><i class="fa fa-list"></i></a>
                                 <a href="#" class="btn btn-danger btn-circle" data-tooltip="Eliminar pregunta" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="fa fa-times"></i></a>
                            </td>
                          @endif
                        </tr>
                        </tbody>
                        @endforeach
                      </table>
                      {!!$preguntas->render()!!}
                      {{-- {!!$preguntas->render()!!} --}}
                      @include('alerts.success')
                      @include('alerts.required')
                  </div>
              </div>
            </div>
          </div>
        </div>
@endsection
