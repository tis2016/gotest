@extends('layouts.principal')
@section('content')
  <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Registro de nuevo usuario</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
<div class="row">
    <div class="col-lg-6">
{!!Html::style('css/style2.css')!!}
{!!Form::open(['route'=>'usuarios.store', 'method'=>'POST'])!!}
@include('usuarios.form.registro')
{!!Form::close()!!}
</div>
</div>
<!-- /.col-lg-8 -->
<!-- /.col-lg-4 -->
</div>
@endsection