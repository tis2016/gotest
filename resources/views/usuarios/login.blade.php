<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Iniciar sesi&oacute;n..</title>
        <!-- Bootstrap Core CSS -->
        {!! Html::style('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
        <!-- MetisMenu CSS -->
        {!! Html::style('bower_components/metisMenu/dist/metisMenu.min.css') !!}
        <!-- Custom CSS -->
        {!! Html::style('dist/css/sb-admin-2.css') !!}
        <!-- Custom Fonts -->
        {!! Html::style('bower_components/font-awesome/css/font-awesome.min.css') !!}
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Login</h3>
                        </div>
                        <div class="panel-body">
                            {!! Form::open(['route'=>'login.store','method'=>'POST']) !!}
                                <fieldset>
                                    <div class="form-group">
                                    {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'E-mail', 'required','autofocus']) !!}
                                    </div>
                                    <div class="form-group">
                                    {!! Form::password('password', ['class' => 'form-control','placeholder'=>'Password']) !!}  
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input name="remember" type="checkbox" value="Remember Me">Recordarme             
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <p class="text-info">
                                            <em>
                                                No tiene una cuenta? <a href="{!! URL::to('usuarios/create') !!}"><i class="fa fa-sign-in fa-fw"></i> Registrarse</a>
                                            </em>
                                        </p>
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    {!! Form::submit('Iniciar sesi&oacute;n',['class'=>'btn btn-lg btn-success btn-block']) !!}
                                </fieldset>
                            {!! Form::close() !!}
                        </div>            
                    </div>
                    @include('alerts.error')
                    @include('alerts.required')
                </div>

            </div>
        </div>
        <!-- jQuery -->
        {!! Html::script('bower_components/jquery/dist/jquery.min.js') !!}
        <!-- Bootstrap Core JavaScript -->
        {!! Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
        <!-- Metis Menu Plugin JavaScript -->
        {!! Html::script('bower_components/metisMenu/dist/metisMenu.min.js') !!}
        <!-- Custom Theme JavaScript -->
        {!! Html::script('dist/js/sb-admin-2.js') !!}
    </body>
</html>