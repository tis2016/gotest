{!! Form::label('nombre','Nombre: ') !!}
{!! Form::text('nombre',null,['class'=>'form-control', 'required','minlength'=>5]) !!}<br/>
{!! Form::label('ap_paterno','Apellido paterno: ') !!}
{!! Form::text('ap_paterno',null,['class'=>'form-control', 'required','minlength'=>5]) !!}<br/>
{!! Form::label('ap_materno','Apellido materno: ') !!}
{!! Form::text('ap_materno',null,['class'=>'form-control', 'required','minlength'=>5]) !!}<br/>

{!! Form::label('genero','Genero: ') !!}
<div class="form-group">
{!! Form::label('genero','Masculino ') !!}
{!! Form::radio('genero', 'Masculino') !!}
{!! Form::label('genero','Femenino') !!}
{!! Form::radio('genero', 'Femenino') !!}
</div><br/>
{!! Form::label('email','E-mail: ') !!}
{!! Form::text('email',null,['class'=>'form-control', 'required','minlength'=>5]) !!}<br/>
{!! Form::label('password','Password: ') !!}<br/>

<div class="form-group">
{!! Form::password('password', ['class' => 'form-control','placeholder'=>'Password']) !!}<br/>
</div>
{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}<br/><br/>