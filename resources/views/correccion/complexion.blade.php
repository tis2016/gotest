@extends('layouts.principal')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Correccion Automatica Pregunta Compleci&oacute;n</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
    <div class="row">
        <div class="col-lg-10">
            @include('alerts.success')  
            <h4> </h4>
            <div class="row">
            <div class="col-sm-6">
                <p> Respuesta del usuario:</p>
                <div>
                    <label for="titulo" class="control-label text-primary"> </label>
                    <div class="radio">
                        <p> {!! $respuesta_usuario !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <p>Respuesta Correcta:</p>
                <div class="form-group ">
                    <label for="titulo" class="control-label text-primary"> </label>
                    <div class="radio ">
                        {{  $respuesta_correcta }}
                    </div>
                </div>
            </div>
            </div>
<div class="alert alert-info" role="alert">
     {{ $cantidad }} Respuesta Correcta  
</div>
            <div>
                <a class="btn btn-primary" href="{{ url('pregunta-complementacion')}}">Regresar</a>
            </div>           

        </div>
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->
{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
{!!Html::script('bower_components/raphael/raphael-min.js')!!}
{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
{!!Html::script('js/morris-data.js')!!}
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}

@endsection
