@extends('layouts.principal')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Registrar pregunta de ordenamiento</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
	<div class="row">
		<div class="col-lg-10">
			{!!Form::open(['route'=>'correccion-ordenamiento.store', 'method'=>'POST'])!!}
			{!!Form::hidden('pregunta_id', $pregunta->id)!!}
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<b>Pregunta # 1</b>
					</div>
					<div class="panel-body">
						<h3>{!! Form::label('enunciado', $pregunta->enunciado) !!}</h3>
						<div class="row" align="center"><br />
						</div><br />
						{!! Form::label('respuesta', "Su respuesta:") !!}<br /><br />
						<table align="left">
							<?php
								$opcion1 = $original;
								$opcion2 = $original;
								$opcion3 = $original;
								$opcion4 = $original;
								shuffle($opcion2);
								shuffle($opcion3);
								shuffle($opcion4);
								$elements = array();
								array_push($elements, $opcion1,$opcion2,$opcion3,$opcion4);
								shuffle($elements);
								//dd($elements);
								$incisos = array("A) ","B) ", "C) ","D) ", "E) ", "F) ", "G) ", "H) ",
												"I) ", "J) ", "K) ","L) ", "M) ");
								for ($i=0; $i < count($elements); $i++) {
									echo "<tr>
								<td>
									<h5><label for='opcion'>".$incisos[$i].implode(", ", $elements[$i])."</label></h5>
								</td>
								<td>&nbsp;&nbsp;<input type='radio' name='opcion' id='opcion' value='".implode(", ", $elements[$i])."'/></td>
							</tr>";
							}
							?>
						</table>
					</div>
					<div class="panel-footer" align="right">
						<a href="#" class="btn btn-primary btn-circle" data-tooltip="Atras"><i class="fa fa-chevron-left "></i></a>
						<a href="#" class="btn btn-primary btn-circle" data-tooltip="Adelante"><i class="fa fa-chevron-right"></i></a>
						<a href="{!! URL::to('ordenamiento') !!}" class="btn btn-warning btn-circle" data-tooltip="Regresar a inicio"><i class="fa fa-home "></i></a>
						<button type="submit" class="btn btn-success btn-circle" data-tooltip="Enviar para corregir"><i class="fa fa-check"></i>
						</button>
					</div>
				</div>
			</div>
			{!!Form::close()!!}
		</div>
	</div>
</div>
@endsection