{!!Form::label('categoria','Categoria: ')!!}
{!!Form::select('categorias_id', $categorias,null,['id'=>'categoriaOrden','class'=>'form-control',
								'placeholder'=>'Seleccione una opcion..','required'])!!}<br>
{!!Form::label('subcategoria','Sub-categoria: ')!!}
{!!Form::select('sub_categorias_id',['placeholder'=>'Seleccione una opcion..'],null,['id'=>'subcategoriaOrden',
								'class'=>'form-control'])!!}<br>
{!!Form::label('enunciado','Enunciado: ')!!}
{!!Form::textarea('enunciado',null,['class'=>'form-control', 'placeholder'=>'Ingrese el enunciado..','rows' => 3,'required']) !!}<br>
@include('alerts.error')
{!!Form::label('opcion','Opciones de respuesta: ')!!}<br>
<div class="form-group option-container-orden">
		<div class="input-group ">
			<span class="input-group-addon">
				{!!Form::number('secuencia[]', '1',['id'=>'secuencia_1','class'=>'small-width','min'=>'1'])!!}
			</span>
				{!!Form::text('opcion[]',null,['id'=>'opcion_1','class'=>'form-control', 'placeholder'=>'Ingrese una opcion con su orden correspondiente..','required'])!!}
				<input id="count" type="hidden" name="contador" value="1">
			<span class="input-group-btn">
					<button class="btn btn-outline btn-danger btn-remove-orden" type="button">X</button>
			</span>
		</div>
</div>
<div class="form-group option-container-orden">
		<div class="input-group ">
			<span class="input-group-addon">
				{!!Form::number('secuencia[]', '1',['id'=>'secuencia_2','class'=>'small-width','min'=>'1'])!!}
			</span>
				{!!Form::text('opcion[]',null,['id'=>'opcion_2','class'=>'form-control', 'placeholder'=>'Ingrese una opcion con su orden correspondiente..','required'])!!}
				<input id="count" type="hidden" name="contador" value="2">	
			<span class="input-group-btn">
					<button class="btn btn-outline btn-danger btn-remove-orden" type="button">X</button>
			</span>
		</div>
</div>
			<button type="button" class="btn btn-outline btn-success btn-lg btn-block btn-add-more-options-orden">Agregar opción</button><br>
			<h1 class="page-header"></h1>

{!!Form::submit('Registrar',['class'=>'btn btn-outline btn-primary'])!!}<br><br><br>
