@extends('layouts.principal')
@section('content')
    <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">Registrar pregunta de ordenamiento</h1>
      </div>
      <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <!-- /.row -->
  <div class="panel-body">
  <div class="row">
      <div class="col-lg-6">
{!!Form::open(['route'=>'ordenamiento.store', 'method'=>'POST','onsubmit'=>'return ValidationEventOR()','files' => true])!!}
{!!Form::hidden('usuarios_id', Auth::user()->id)!!}
@include('preguntaordenamiento.form.pregunta')
{!!Form::close()!!}
</div>
</div>
<!-- /.col-lg-8 -->
<!-- /.col-lg-4 -->
</div>
@include('layouts.scripts-ordenamiento-create')
@endsection
