@extends('layouts.principal')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Registrar examen</h1>
    </div>
</div>
<div class="panel-body">
    <div class="row">
        <div class="col-lg-10">
            @include('alerts.success')
            <form action="{{ url('examenes/agregar')}}" method="POST" role="form" data-togle="validator" id="examen-crear">
                {!! csrf_field() !!}                
                {!!Form::hidden('usuarios_id', Auth::user()->id)!!}
                <div class="form-group @if($errors->has('titulo')) has-error @endif">
                    <label for="titulo" class="control-label text-primary">Titulo</label>
                    <div class="form-group">
                    	<input type="text" id="titulo" name="titulo" class="form-control" value="{{old('titulo')}}">
                    	@if($errors->has('titulo')) <span class="text-danger"> {{$errors->first('titulo')}}</span> @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('materia')) has-error @endif">
                    <label for="materia" class="control-label text-primary">Materia</label>
                    <div class="form-group">
                        <input type="text" id="materia" name="materia" class="form-control" value="{{old('materia')}}">
                    </div>
                    @if($errors->has('materia')) <span class="text-danger"> {{$errors->first('materia')}}</span> @endif
                </div>

                <div class="row">
                <div class="col-sm-6">

                <div id="inicio_inscripcion" class="form-group @if($errors->has('inicio_inscripcion')) has-error @endif">
                    <label for="inicio_inscripcion" class="control-label text-primary">Periodo inicio de Inscripcion</label>
                    <div class="form-group date datetimepicker1">
                        <input type="text" name="inicio_inscripcion" class="form-control datetimepicker1" value="{{old('inicio_inscripcion')}}">
                    </div>
                    <span class="text-danger"> @if($errors->has('inicio_inscripcion')) {{$errors->first('inicio_inscripcion')}} @endif </span>
                </div>
                
                </div>
                <div class="col-sm-6">

                <div id="fin_inscripcion" class="form-group @if($errors->has('fin_inscripcion')) has-error @endif">
                    <label for="fin_inscripcion" class="control-label text-primary">Periodo fin de inscripcion</label>
                    <div class="form-group  date datetimepicker1">
                        <input type="text"  name="fin_inscripcion" value="{{old('fin_inscripcion')}}" class="form-control datetimepicker1">
                    </div>
                    <span class="text-danger"> @if($errors->has('fin_inscripcion')) {{$errors->first('fin_inscripcion')}} @endif </span>
                </div>

                </div>
                </div>

                <div class="row">
                <div class="col-sm-6">

                <div id="inicio_examen" class="form-group @if($errors->has('inicio_examen')) has-error @endif">
                    <label for="inicio_examen" class="control-label text-primary">Periodo inicio de examen</label>
                    <div class="form-group date datetimepicker1">
                        <input type="text"  name="inicio_examen"  value="{{ old('inicio_examen') }}" class="form-control datetimepicker1">
                    </div>
                    <span class="text-danger"> @if($errors->has('inicio_examen'))  {{$errors->first('inicio_examen')}} @endif </span>
                </div>
                </div>
                <div class="col-sm-6">
                <div id="fin_examen" class="form-group @if($errors->has('fin_examen')) has-error @endif">
                    <label for="fin_examen" class="control-label text-primary">Periodo fin de examen</label>
                    <div class="form-group date datetimepicker1">
                        <input type="text"  name="fin_examen"  value="{{old('fin_examen')}}" class="form-control datetimepicker1">
                    </div>
                    <span class="text-danger"> @if($errors->has('fin_examen')){{$errors->first('fin_examen')}} @endif </span>
                </div>
                </div>
                </div>

                <div class="row">
                <div class="col-sm-4">

                <div class="form-group @if($errors->has('porcentaje_total')) has-error @endif">
                    <label for="porcentaje_total" class="control-label text-primary">Porcentaje Total</label>
                    <div class="form-group">
                        <input type="text" id="porcentaje_total" name="porcentaje_total" value="{{old('porcentaje_total')}}" class="form-control">
                    </div>
                    @if($errors->has('porcentaje_total')) <span class="text-danger"> {{$errors->first('porcentaje_total')}}</span> @endif
                </div>              
                </div>
                <div class="col-sm-4">  
                <div class="form-group @if($errors->has('valor_total')) has-error @endif">
                    <label for="valor_total" class="control-label text-primary">Valor total</label>
                    <div class="form-group">
                        <input type="text" id="valor_total" name="valor_total" value="{{old('valor_total')}}" class="form-control">
                    </div>
                    @if($errors->has('valor_total')) <span class="text-danger"> {{$errors->first('valor_total')}}</span> @endif
                </div>
                </div>
                <div class="col-sm-4">
                <div class="form-group @if($errors->has('numero_intentos')) has-error @endif">
                    <label for="numero_intentos" class="control-label text-primary">Numero intentos</label>
                    <div class="form-group">
                        <input type="text" id="numero_intentos" name="numero_intentos" value="{{old('numero_intentos')}}" class="form-control">
                    </div>
                    @if($errors->has('numero_intentos')) <span class="text-danger"> {{$errors->first('numero_intentos')}}</span> @endif
                </div>
                </div>
                </div>
                <br>
                <div class="">
                    <div class="">
                        <button type="submit" class="btn btn-primary"> Guardar
                        </button>
                        <a href="{{url('examenes')}}" type="reset" class="btn btn-primary"> Cancelar
                        </a>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->



@endsection

