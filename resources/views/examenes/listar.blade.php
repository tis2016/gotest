@extends('layouts.principal')
@section('content')
  <div class="row">
    <div class="col-sm-12">
        <h1 class="page-header">Ex&aacute;menes </h1>
    </div>
      <div class="div col-lg-12">
          <a class="btn btn-primary" href="{{ url('/examenes/agregar')}}"> <span class="glyphicon glyphicon-plus-sign"> </span> Agregar</a>
      </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
<div class="row">
    <div class="col-lg-12">
      @include('alerts.success')
      @include('alerts.required')
      
      <table class="table table-striped table-bordered">
          <thead> 
              <tr>
                  <th>T&iacute;tulo</th>
                  <th>Materia</th>
                  <th> Porcentaje total</th>
                  <th> Valor total </th>
                  <th>N&uacute;mero de intentos</th>
                  <th>Acci&oacute;n</th>
              </tr>
          </thead>
          <tbody>
              @foreach($examenes as $examen)
               <tr>
                   <td> {{ $examen->titulo }}</td>
                   <td> {{ $examen->materia }}</td>
                   <td> {{ $examen->porcentaje_total }} </td>
                   <td> {{ $examen->valor_total }} </td>
                   <td> {{ $examen->numero_intentos }} </td>
                   <td><a href="#" class="btn btn-primary btn-circle" data-tooltip="Editar examen"><i class="glyphicon glyphicon-edit"></i></a>
                   <a href="{!! URL::to('/agregar-pregunta') !!}?id={{$examen->id}}" class="btn btn-success btn-circle" data-tooltip="Agregar preguntas"><i class="fa fa-plus"></i></a>
                                <a href="#" class="btn btn-danger btn-circle" data-tooltip="Eliminar examen" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="glyphicon glyphicon-trash"></i></a>
                   </td>
               </tr>
               @endforeach
          </tbody>
      </table>

                                
    </div>
    </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>

@endsection
