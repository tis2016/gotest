<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Go Test | Sistema de evaluaciones en l&iacute;nea</title>
        <!-- Bootstrap Core CSS -->
        {!!Html::style('css/tooltips.css')!!}
        {!!Html::style('css/bootstrap.min.css')!!}
        {!!Html::style('css/bootstrap-datetimepicker.css')!!}
        <!-- MetisMenu CSS -->
        {!!Html::style('bower_components/metisMenu/dist/metisMenu.min.css')!!}
        <!-- Timeline CSS -->
        {!!Html::style('dist/css/timeline.css')!!}
        <!-- Custom CSS -->
        {!!Html::style('dist/css/sb-admin-2.css')!!}
        <!-- Morris Charts CSS -->
        {!!Html::style('bower_components/morrisjs/morris.css')!!}
        <!-- Custom Fonts -->
        {!!Html::style('bower_components/font-awesome/css/font-awesome.min.css')!!}
        {!!Html::style('css/style-div.css')!!}
        {!!Html::style('css/styles.css')!!}
        {!! Html::style('sass/app.css') !!}
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand center-block" href="{!! URL::to('/') !!}">Go Test 1.0</a> 
                </div>
                 <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{!!URL::to('home')!!}">Panel de administraci&oacute;n<i class="fa fa-tasks fa-fw"></i></a>
                    </li>
                    <!-- /.dropdown -->
                    <li>
                        <a href="{{ url('examenes/agregar') }}">Crear ex&aacute;men
                            <i class="fa fa-file-text-o fa-fw"></i>
                        </a>
                    </li>
                    <!-- /.dropdown -->
                    @if (Auth::check())
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{Auth::user()->nombre}}
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="{!! URL::to('logout') !!}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
                @else
                <li>
                    <a href="{!! URL::to('login') !!}">Iniciar sesi&oacute;n
                        <i class="fa fa-user fa-fw"></i> 
                    </a>
                </li>
                @endif
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-header -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- /#page-wrapper -->
        <div class="section2">
            <div class="row">
                <div class="col-md-6 left-block " >
                    <p><b class="main-text"> PERMITA QUE NUESTRA HERRAMIENTA SE ENCARGUE DE USTED </b></p>
                </div>
                <div class="col-md-6 left-block " >
                    <img class="img-responsive" src="images/img1.png" alt="">
                </div>
            </div>
        </div>
        <div class="about-me">
            <p><b>ACERCA DE GO TEST</b></p>
            <div class="internal-1">
                <p>~~~~~::~~~~~</p>
                <p>Es una herramienta para crear examenes en l&iacute;nea, con Go Test usted puede ahorrarse el trabajo de crear sus examenes para sus estudiantes, con una interfaz amigable y super intuitiva. </p>
            </div>
            <div class="row">
                <div class="col-md-3" >
                    <div class="icon-focus center-block"></div>
                    <b>F&aacute;cil uso</b><p>Su interfaz tiene todas las herramientas necesarias para crear sus ex&aacute;menes facilmente.</p>
                </div>
                <div class="col-md-3" >
                    <div class="icon-pencil center-block"></div>
                    <b>Personalizable</b><p>Puede crear sus preguntas en las diferentes categorias o elegir desde la base de datos.</p>
                </div>
                <div class="col-md-3" >
                    <div class="icon-conf center-block"></div>
                    <b>Configurable</b><p>Puede configurar el horario, el tiempo de ex&aacute;men, puntuaci&oacute;n, todo desde un panel super amigable. </p>
                </div>
                <div class="col-md-3" >
                    <div class="icon-computer center-block"></div>
                    <b>Ex&aacute;men en l&iacute;nea</b><p>Sus estudiantes pueden rendir el examen en l&iacute;nea con toda confianza, los reportes y notas son visualizados en su panel de control.</p>
                </div>
            </div>
        </div>
        <div class="section4">
            <div class="row">
                <div class="col-md-6 left-block clear" >
                    <p><b class="main-text"> Siete tipos de preguntas </b></p>
                    <p>~~~~~::~~~~~</p>
                    <p>Puede crear siete tipos de preguntas para sus ex&aacute;menes:</p>
                    <ul>
                        <li><b>Preguntas de selecci&oacute;n simple</b></li>
                        <li><b>Preguntas de falso y verdadero</b></li>
                        <li><b>Preguntas de seleccion m&uacute;ltiple</b></li>
                        <li><b>Preguntas de ordenamiento</b></li>
                        <li><b>Preguntas de emparejamiento</b></li>
                        <li><b>Preguntas de compleci&oacute;n</b></li>
                        <li><b>Preguntas abiertas</b></li>
                    </ul>
                    <p>Puede tambi&eacute;n elegir preguntas de nuestra base de datos, en las diferentes categorias que usted requiera, muchos de nuestros usuarios crean las preguntas todo el tiempo !</p>
                </div>
            </div>
        </div>
        <div class="section5">
            <div class="row">
                <div class="col-md-6 left-block " >
                    <img class="img-responsive" src="images/exam.png" alt="">
                </div>
                <div class="col-md-6 left-block " >
                    <p><b class="text-box"> Resoluci&oacute;n y correcci&oacute;n autom&aacute;tica </b></p><br/><br/>
                    <p>Cada pregunta tiene su vista de detalles, vista previa, resoluci&oacute;n y correcci&oacute;n autom&aacute;tica, usted podr&aacute; visualizar cada pregunta antes de anadirla a su examen.</p>
                    <ul>
                        <li>Mantenga control todo el tiempo acerca de su ex&aacute;men</li>
                        <li>Obtenga resultados en pantalla de sus calificaciones</li>
                        <li>Todo muy f&aacute;cil !</li>
                    </ul>
                </div>
                
            </div>
        </div>
        <div class="footer-1">
            <div class="row">
                <div class="col-md-12 ">
                    <p><b class="footer-text">© Derechos reservados 2016</b></p>
                </div>
            </div>
        </div>
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    {!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
    {!!Html::script('js/moment.js')!!}
    {!!Html::script('js/transition.js')!!}
    {!!Html::script('js/collapse.js')!!}
    <!-- Bootstrap Core JavaScript -->
    {!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
    {!!Html::script('js/bootstrap-datetimepicker.min.js')!!}
    <!-- Metis Menu Plugin JavaScript -->
    {!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
    {!!Html::script('bower_components/raphael/raphael-min.js')!!}
    {!!Html::script('bower_components/morrisjs/morris.min.js')!!}
    {!!Html::script('dist/js/sb-admin-2.js')!!}
</body>
</html>