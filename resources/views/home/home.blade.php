@extends('layouts.principal')
@section('content')
  <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Bienvenido al panel de administraci&oacute;n</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-question fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $preguntas }}</div>
                                    <div>Preguntas registradas!</div>
                                </div>
                            </div>
                        </div>
                        <a href="{!!URL::to('preguntas/listar')!!}">
                            <div class="panel-footer">
                                <span class="pull-left">Listar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file-text-o fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $examenes }}</div>
                                    <div>Examenes registrados!</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ url('examenes/') }}">
                            <div class="panel-footer">
                                <span class="pull-left">Listar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $usuarios }}</div>
                                    <div>Usuarios registrados!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Sin detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-star fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">100</div>
                                    <div>Estrellas recibidas!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Sin detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
<div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock-o fa-wrench"></i> Detalles del uso
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <ul class="timeline">
                                <li>
                                    <div class="timeline-badge success"><i class="fa fa-user"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Panel de administraci&oacute;n</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>A la izquierda se encuentra el panel de administraci&oacute;n para su uso, existe cuatro niveles que le guiar&aacute;n durante su creaci&oacute;n de examenes.</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-badge warning"><i class="fa fa-home"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Inicio</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Esta opci&oacute;n le redirige a la vista principal del panel de administraci&oacute;n, &uacute;sela para regresar al inicio si asi lo desea.</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="timeline-badge danger"><i class="fa fa-question"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Todas las preguntas</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Esta opci&oacute;n le permite visualizar todas las preguntas que existen en la base de datos, las que usted cre&oacute; y las de los dem&aacute;s usuarios, en ella puede editar o eliminar preguntas.</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-badge info"><i class="fa fa-file-text-o"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Ex&aacute;menes</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Esta secci&oacute;n tiene dos niveles, crear y listar ex&aacute;menes de la base de datos, con estas opciones usted podr&aacute; crear nuevos ex&aacute;menes o buscar, agregar preguntas al ex&aacute;men, editar y/o eliminar ex&aacute;menes.</b></p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="timeline-badge "><i class="fa fa-save"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Tipos de preguntas</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>En esta secci&oacute;n se le presenta a usted todas los tipos de preguntas que el sistema puede crear para sus ex&aacute;menes en las diferentes categor&iacute;as, son siete tipos y cada una le permite crear, editar y/o eliminar preguntas.</p>
                                            <hr>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-badge success"><i class="fa fa-graduation-cap"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Disfrute de la aplicaci&oacute;n</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Ya tiene todo lo necesario para empezar, si usted tiene consultas, no dude en contactarnos.</p>
                                            <p><h3><b>A comenzar !</b></h3></p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- /.panel-body -->
                    </div>
@endsection