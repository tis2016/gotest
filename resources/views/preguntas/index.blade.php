@extends('layouts.principal')

@section('content')
  <div class="row">
  <table class="table table-striped table-bordered">
    <thead>
       <th>ID</th>
       <th>Enunciado</td> 
       <th>Tipo de pregunta</td> 
       <th>Acciones</td> 
    </thead>
    <tbody>
@foreach($preguntas as $pregunta)
   <tr>
     <td>{{ $pregunta->id }}</td>
     <td>{{ $pregunta->enunciado }} </td>
     <td>{{ $pregunta->tipo->alias }} </td>
     <td><a class="btn btn-default">Ver</a></td>
   </tr>
@endforeach
    </tbody>
   </table>
   {!! $preguntas->render() !!}
</div> 


@endsection


