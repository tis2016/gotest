
  @if(Session::has('store-success'))
    <div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Exito!</strong> {!! session('store-success') !!}
    </div>
  @elseif(Session::has('update-success'))
    <div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Exito!</strong> {!! session('update-success') !!}
    </div>
  @elseif(Session::has('delete-success'))
    <div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Exito!</strong> {!! session('delete-success') !!}
    </div>
  @endif
