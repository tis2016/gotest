<br><section class="content-header">
  @if(Session::has('message-error'))
    <div class="alert alert-danger" role="alert">
    <strong>Error!</strong> {!! session('message_error') !!}
    </div>
  @elseif(Session::has('update_message'))
    <div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>Exito!</strong> {!! session('update_message') !!}
    </div>
  @endif
</section>
