@if(count($errors) > 0)
  <div class="alert alert-danger" role="alert">
  <strong>Error al iniciar sesi&oacute;n!</strong>
  <ul>
    @foreach($errors->all() as $error)
        <li>{!!$error!!}</li>
    @endforeach
  </ul>
  </div>
@endif
