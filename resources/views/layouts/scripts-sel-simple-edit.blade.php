<script type="text/javascript">
var lim = {{json_encode($count)}};
var template ='<div class="form-group option-container-edit"><div class="input-group"><span class="input-group-addon">{!!Form::select('correcto2[]', ['0' => 'Incorrecto', '1' => 'Correcto'])!!}</span>'+
'{!!Form::text('opcion2[]',null,['class'=>'form-control', 'placeholder'=>'Ingresa una opcion..','min'=>'2','required'])!!}<span class="input-group-btn">'+
'<button class="btn btn-outline btn-danger btn-remove" type="button">X</button></span></div></div>';

$('.btn-add-more-options-edit').on('click',function(e){
			e.preventDefault();
			$(this).before(template);
			lim++;
});

$(document).on('click','.btn-remove',function(e){
			if(lim == 2){
				alert("Debe existir al menos dos opciones en el formulario !");
			}else{
				e.preventDefault();
				$(this).parents('.option-container-edit').remove();
				lim--;
			}
});
</script>

<script type="text/javascript">
var lim_orden2 = {{json_encode($count)}};
$(document).ready(function(){
  $('.btn-remove').click(function(e){
    if(lim_orden2 == 2){

    }else{
          e.preventDefault();//evitar que la pagina sea recargada despues del ok del alert
          var id = $(this).parent('span').attr('opcion-id');
          var form = $('#form-delete');
          var url = form.attr('action').replace(':OPTION_ID',id);
          var data = form.serialize();
          $.post(url, data, function(result){
            //alert(result);
            lim_orden2--;
          });
          alert(lim_orden2);
        }
  });
});

</script>
