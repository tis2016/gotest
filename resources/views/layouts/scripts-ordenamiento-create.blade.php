{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
{!!Html::script('js/dropdown.js')!!}


<script type="text/javascript">
var lim_orden = 2;
var actual = 3;
function replace_keys(key){
var str ='<div class="form-group option-container-orden">'+'<div class="input-group "><span class="input-group-addon">'+
'{!!Form::number('secuencia2[]', '1',['id'=>'replace_it','class'=>'small-width','min'=>'1','required'])!!}</span>'+
'{!!Form::text('opcion2[]',null,['class'=>'form-control', 'placeholder'=>'Ingrese una opcion con su orden correspondiente..','required'])!!}<input id="count" type="hidden" name="contador" value="replace_count">'+'<span class="input-group-btn">'+
'<button class="btn btn-outline btn-danger btn-remove-orden" type="button">X</button>'+'</span></div></div>';
var str2 = str.replace("replace_it","secuencia_"+key);
var template_orden = str2.replace("replace_count",key);
return template_orden;
}

$('.btn-add-more-options-orden').on('click',function(e){
  e.preventDefault();
  $(this).before(replace_keys(actual));
  lim_orden++;
  actual++;
});

$(document).on('click','.btn-remove-orden',function(e){
  if(lim_orden == 2){
    alert("Debe existir al menos dos opciones en el formulario!");
  }else{
    e.preventDefault();
    $(this).parents('.option-container-orden').remove();
    lim_orden--;
    actual--;
  }
});

function ValidationEventOR(){
  var count = actual - 1;
  var count_secuencia = 0;
  var array = Array();
  for (var i = 1; i <= count; i++) {
    var secuencia_target = document.getElementById("secuencia_"+i).value;
    array.push(secuencia_target);
  }
  console.log(array);
  var flag = checkValues(array,count);
  if(flag == "false"){
    alert("El orden debe ser correlativo !");
    return false;
  }else{
    return true;
  }
}
function checkValues(array,count){
    var flag = "false";
    for (var i = 1; i <= count; i++) {
      if(array.indexOf(i+"") == -1) flag = "false";
      else flag = "true";
    }
    return flag;
}
</script>

<script type="text/javascript">
var lim_orden2 = 2;
$(document).ready(function(){
  $('.btn-remove-orden').click(function(e){
    if(lim_orden2 == 2){

    }else{
          e.preventDefault();//evitar que la pagina sea recargada despues del ok del alert
          var id = $(this).parent('span').attr('option-id');
          var form = $('#form-delete-orden');
          var url = form.attr('action').replace(':OPTION_ID',id);
          var data = form.serialize();
          $.post(url, data, function(result){
            //alert(result);
            lim_orden2--;
          });
        }
  });
});
</script>
