{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
{!!Html::script('js/dropdown.js')!!}

<script type="text/javascript">
var lim_orden = {{json_encode($count)}};
var template_orden ='<div class="form-group option-container-orden">'+'<div class="input-group "><span class="input-group-addon">'+
'{!!Form::number('secuencia2[]', '1',['class'=>'small-width','min'=>'1','required'])!!}</span>'+
'{!!Form::text('opcion2[]',null,['class'=>'form-control', 'placeholder'=>'Ingrese una opcion con su orden correspondiente..','min'=>'2','required'])!!}'+'<span class="input-group-btn">'+
'<button class="btn btn-outline btn-danger btn-remove-orden" type="button">X</button>'+'</span></div></div>';

$('.btn-add-more-options-orden').on('click',function(e){
  e.preventDefault();
  $(this).before(template_orden);
  lim_orden++;
});

$(document).on('click','.btn-remove-orden',function(e){
  if(lim_orden == 2){
    alert("Debe existir al menos dos opciones en el formulario!");
  }else{
    e.preventDefault();
    $(this).parents('.option-container-orden').remove();
    lim_orden--;
  }
});
</script>

<script type="text/javascript">
var lim_orden2 = {{json_encode($count)}};
$(document).ready(function(){
  $('.btn-remove-orden').click(function(e){
    if(lim_orden2 == 2){

    }else{
          e.preventDefault();//evitar que la pagina sea recargada despues del ok del alert
          var id = $(this).parent('span').attr('option-id');
          var form = $('#form-delete-orden');
          var url = form.attr('action').replace(':OPTION_ID',id);
          var data = form.serialize();
          $.post(url, data, function(result){
            //alert(result);
            lim_orden2--;
          });
        }
  });
});
</script>
