<script type="text/javascript">
var lim = 2;
var actual = 3;
function replace_keys(key){
	var str ='<div class="form-group option-container"><div class="input-group option-container"><span class="input-group-addon">{!!Form::select('correcto[]', ['0' => 'Incorrecto', '1' => 'Correcto'],null,['id'=>'replace_it','required'])!!}</span>'+
	'{!!Form::text('opcion[]',null,['class'=>'form-control', 'placeholder'=>'Ingresa una opcion..','min'=>'2','required'])!!}<input id="count" type="hidden" name="contador" value="replace_count"><span class="input-group-btn">'+
	'<button class="btn btn-outline btn-danger btn-remove" type="button">X</button></span></div></div>';
	var str2 = str.replace("replace_it", "correcto_"+key);
	var template = str2.replace("replace_count",key);
	return template;
}
//console.log(template);

$('.btn-add-more-options').on('click',function(e){
			e.preventDefault();
			$(this).before(replace_keys(actual));
			actual++;
			lim++;
});

$(document).on('click','.btn-remove',function(e){
			if(lim == 2){
				alert("Debe existir al menos dos opciones en el formulario !");
			}else{
				e.preventDefault();
				$(this).parents('.option-container').remove();
				actual--;
				lim--;
			}
});

function ValidationEventSM() {
// Storing Field Values In Variables
var count = actual - 1;
var count_correctos = 0;
var count_incorrectos = 0;
for (var i = 1; i <= count; i++) {
	var correcto_target = document.getElementById("correcto_"+i).value;
	if (correcto_target == "0") count_incorrectos++;
	if (correcto_target == "1") count_correctos++;
}
if(count_correctos == 0){
	 alert("Debe existir por lo menos una opcion correcta !");
	 return false;
 }
//alert("Todo bien !");
return true;
}

</script>
