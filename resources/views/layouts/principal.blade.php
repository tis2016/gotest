<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Go Test | Sistema de evaluaciones en l&iacute;nea</title>
        <!-- Bootstrap Core CSS -->
        {!!Html::style('css/tooltips.css')!!}
        {!!Html::style('bower_components/bootstrap/dist/css/bootstrap.min.css')!!}
        {!!Html::style('css/bootstrap-datetimepicker.css')!!}
        <!-- MetisMenu CSS -->
        {!!Html::style('bower_components/metisMenu/dist/metisMenu.min.css')!!}
        <!-- Timeline CSS -->
        {!!Html::style('dist/css/timeline.css')!!}
        <!-- Custom CSS -->
        {!!Html::style('dist/css/sb-admin-2.css')!!}
        <!-- Morris Charts CSS -->
        {!!Html::style('bower_components/morrisjs/morris.css')!!}
        <!-- Custom Fonts -->
        {!!Html::style('bower_components/font-awesome/css/font-awesome.min.css')!!}
        {!!Html::style('css/style-div.css')!!}
        {!!Html::style('css/styles.css')!!}
        {!! Html::style('sass/footer.css') !!}
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
            <!-- /#wrapper -->
        <!-- jQuery -->
        {!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
        {!!Html::script('js/moment.js')!!}
        {!!Html::script('js/transition.js')!!}
        {!!Html::script('js/collapse.js')!!}
        <!-- Bootstrap Core JavaScript -->
        {!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
        {!!Html::script('js/bootstrap-datetimepicker.min.js')!!}
        <!-- Metis Menu Plugin JavaScript -->
        {!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
        {!!Html::script('bower_components/raphael/raphael-min.js')!!}
        {!!Html::script('bower_components/morrisjs/morris.min.js')!!}
        {!!Html::script('dist/js/sb-admin-2.js')!!}
        {!!Html::script('js/date-picker.js')!!}
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{!! URL::to('/') !!}">Go Test 1.0</a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{!!URL::to('home')!!}">Panel de administraci&oacute;n<i class="fa fa-tasks fa-fw"></i></a>
                    </li>
                    <!-- /.dropdown -->
                    <li>
                        <a href="{{ url('examenes/agregar') }}">Crear ex&aacute;men
                            <i class="fa fa-file-text-o fa-fw"></i>
                        </a>
                    </li>
                    <!-- /.dropdown -->
                    @if (Auth::check())
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{Auth::user()->nombre}}
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="{!! URL::to('logout') !!}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
                @else
                <li>
                    <a href="{!! URL::to('login') !!}">Iniciar sesi&oacute;n
                        <i class="fa fa-user fa-fw"></i> 
                    </a>
                </li>
                @endif
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-header -->
            
            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{!!URL::to('home')!!}"><i class="fa fa-home fa-fw"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Ex&aacute;menes <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url('examenes/agregar') }}">Crear</a>
                                </li>
                                <li>
                                    <a href="{{ url('examenes/') }}">Examenes</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-list-ul"></i> Tipos de preguntas <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    
                                </li>
                                <li>
                                    <a href="{{ url('seleccion-simple') }}">Seleccion Simple</a>
                                </li>
                                <li>
                                    <a href="{{ url('seleccion-multiple') }}">Seleccion Multiple</a>
                                </li>
                                <li>
                                    <a href="{{ url('falso-verdadero') }}">Falso y verdadero</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('abierta/')!!}"> Abiertas</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('ordenamiento/')!!}"> Ordenamiento</a>
                                    <a href="{{ url('pregunta-emparejamiento') }}"> Emparejamiento </a>
                                </li>
                                <li>
                                    <a href="{{ url('pregunta-complementacion') }}"> Compleci&oacute;n </a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{!!URL::to('preguntas/listar')!!}"><i class="fa fa-stack-overflow"></i> Todas las preguntas</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
            @yield('content')
        </div>
        <div class="footer-1">
            <div class="col-md-12 ">
                <p><b class="footer-text">© Derechos reservados 2016</b></p>
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>

</body>
</html>