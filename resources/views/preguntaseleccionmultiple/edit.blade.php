@extends('layouts.principal')
@section('content')
  {!!Form::model($pregunta,['route'=>['seleccion-multiple.update',$pregunta->id],'method'=>'PUT','onsubmit'=>'return ValidationEventSM()','files' => true])!!} <!--enrutando los datos -->
  <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar pregunta</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
<div class="row">
    <div class="col-lg-8">
      {!!Html::style('css/style2.css')!!}

      <!-- PREGUNTAS ABIERTAS -->
      {!!Form::hidden('tipo', 'seleccion-multiple')!!}
      {!!Form::label('categoria','Categoria: ')!!}
      {!!Form::select('categorias_id', $categoria,null,['id'=>'categoriaSelMultiple','class'=>'form-control',
                      'placeholder'=>'Seleccione una opcion..','required'])!!}<br>
      {!!Form::label('subcategoria','Sub-categoria: ')!!}
      {!!Form::select('sub_categorias_id', $subcategoria,null,['id'=>'subcategoriaSelMultiple','class'=>'form-control',
                      'placeholder'=>'Seleccione una opcion..','required'])!!}<br>
      {!!Form::label('enunciado','Enunciado: ')!!}
      {!!Form::textarea('enunciado',null,['class'=>'form-control', 'rows' => 2]) !!}<br>
      @include('alerts.error')
      {!!Form::label('opcion','Opciones de respuesta: ')!!}<br>
      @foreach($opcion as $key => $value)
        {{-- {!!Form::model($value,['route'=>['opciones.update',$value->id],'method'=>'PUT','id'=>'Form'.$a])!!} --}}
        <div class="form-group option-container-edit">
          {!!Form::hidden('id_opciones[]', $value->id)!!}
          <input id="count_old" type="hidden" name="contador" value="{{$count2}}">
            <div class="input-group ">
              @if($value->correcto == 1)
                <span class="input-group-addon">
                  {!!Form::select('correcto[]', ['1' => 'Correcto','0' => 'Incorrecto'],'default', ['id' => $value->id ])!!}
                </span>
              @else
                <span class="input-group-addon">
                  {!!Form::select('correcto[]', ['0' => 'Incorrecto','1' => 'Correcto'],'default', ['id' => $value->id ])!!}
                </span>
              @endif
                {!!Form::text('opcion[]',$value->opcion,['class'=>'form-control','min'=>'2','required']) !!}
              <span opcion-id="{{$value->id}}" class="input-group-btn">
                  <button class="btn btn-outline btn-danger btn-remove" type="button">X</button>
              </span>
            </div>
        </div>
      @endforeach
            <button type="button" class="btn btn-outline btn-success btn-lg btn-block btn-add-more-options-edit">Agregar opción</button>
            <h1 class="page-header"></h1>
            {{-- {!!link_to('#', $title='Actualizar', $attributes = ['id'=>'subbut', 'class'=>'btn btn-outline btn-primary'], $secure = null)!!} --}}
            {!!Form::submit('Actualizar',['class'=>'btn btn-outline btn-primary','id'=>'subbut'])!!}<br><br><br><br>
        </div>
    <!-- /.col-lg-8 -->
    <!-- /.col-lg-4 -->
</div>
</div>
{!!Form::close()!!}
{!!Form::open(['route'=>['opciones-multiple.destroy',':OPTION_ID'],'method'=>'DELETE','id'=>'form-delete'])!!}
{!!Form::close()!!}
<!-- /.row -->
@include('layouts.scripts')
@include('layouts.scripts-sel-multiple-edit')

@endsection
