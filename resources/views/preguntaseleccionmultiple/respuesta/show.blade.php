@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
      <h1 class="page-header">Correccion de la Pregunta</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
<div class="row">
  <div class="col-lg-12">
    @include('alerts.success')
    @include('alerts.required')

                              {!! csrf_field() !!}



                                  <div class="panel panel-default">
                                      <div class="panel-heading">
                                          {{ $pregunta->enunciado }}
                                      </div>
                                      <!-- /.panel-heading -->
                                      <div class="panel-body">
                                          <div class="dataTable_wrapper">
                                              <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                      <tr>

                                                          <th>Respuesta</th>
                                                          <th>Correcto</th>

                                                      </tr>
                                                  </thead>
                                                  {{-- <tbody id="datos"> --}}
                                                    @foreach($pregunta->respuestasSelMultiple as $respuesta)
                                                    <tbody>

                                                          <tr>

                                                            @if($respuesta->correcto == 1)
                                                              <div class="">
                                                                <td><span class="label label-primary">{{ $respuesta->respuesta }}</span></td>
                                                              </div>
                                                            @else
                                                              <div class="">
                                                                <td><span class="label label-danger">{{ $respuesta->respuesta }}</span></td>
                                                              </div>
                                                            @endif
                                                            @if($respuesta->correcto == 1)
                                                              <div class="">
                                                                <td><span class="label label-primary">Correcto</span></td>
                                                              </div>
                                                            @else
                                                              <div class="">
                                                                <td><span class="label label-danger">Incorrecto</span></td>
                                                              </div>
                                                            @endif

                                                          </tr>

                                                    </tbody>
                                                    @endforeach
                                                  {{-- </tbody> --}}
                                              </table>

                                              @include('alerts.success')
                                              @include('alerts.required')
                                          </div>
                                      </div>
                                    </div>

                              </div>

                              <br>
                              {!!link_to_route('seleccion.index', $title = 'Volver a Pregutnas', null, $attributes = ['class'=>'btn btn-outline btn-primary'])!!}

  </div>
  </div>
  <!-- /.col-lg-8 -->
  <!-- /.col-lg-4 -->
</div>

<!-- /.row -->
{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
{!!Html::script('bower_components/raphael/raphael-min.js')!!}
{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
{!!Html::script('js/morris-data.js')!!}
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
@endsection
