@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Detalles de su respuesta</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<div class="table-responsive table-bordered">
  <table class="table">
    <thead>
      <tr>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><h4><b>Enunciado: </b></h4></td>
        <td><h5>{{ $pregunta->enunciado }}</h5></td>
      </tr>
      <tr>
        <td><h4><b>Categoria: </b></h4></td>
        <td><h5></h5>{{ $categoria[0]->categoria }}</td>
      </tr>
      <tr>
        <td><h4><b>Sub-categoria: </b></h4></td>
        <td><h5>{{ $subcategoria[0]->subcategoria }}</h5></td>
      </tr>
        <td><h4><b>Opciones : </b></h4></td>
        <td>
          @foreach($pregunta->opciones as $opciones)
              @if($opciones->correcto ==1)
                    <h4><p class="text-success" data-tooltip="Respuesta correcta">{{ $opciones->opcion }}</p></h4>
              @else
                    <h4><p class="text-danger" data-tooltip="Respuesta incorrecta">{{ $opciones->opcion }}</p></h4>
              @endif
         @endforeach
          </td>
      </tr>
      <tr>
        <td><h4><b>Puntaje o valor: </b></h4></td>
        <td>
          <h5>El puntaje es seg&uacute;n a la cantidad de opciones que acertaste.</h5>
        </td>
      </tr>
      <tr>
        <td><h4><b>Tu respuesta: </b></h4></td>
        <td>

            @for($i=0; $i < count($ultimasRespuestas); $i++)
              @if($ultimasRespuestas[$i]->correcto == 1)
              <h4><p class="text-success" data-tooltip="Respuesta correcta">{{ $ultimasRespuestas[$i]->respuesta }}</p></h4>
              @endif
              @if($ultimasRespuestas[$i]->correcto == 0 )
              <h4><p class="text-danger" data-tooltip="Respuesta incorrecta">{{ $ultimasRespuestas[$i]->respuesta }}</p></h4>
              @endif
              @if($ultimasRespuestas[$i]->respuesta == "sin respuesta" )
              <h4><p class="text-danger" data-tooltip="Respuesta incorrecta">{{ $ultimasRespuestas[$i]->respuesta }}</p></h4>
              @endif
            @endfor
        </td>
      </tr>
      <tr>
        <td><h4><b>Su puntaje: </b></h4></td>
        <td>
          @for($i=0; $i < count($ultimasRespuestas); $i++)
            @if($ultimasRespuestas[$i]->correcto == 1)
              <h4><p class="text-info" data-tooltip="nota de la respuesta">{{ $ultimasRespuestas[$i]->peso }}</p></h4>
            @else
            <h4><p class="text-danger" data-tooltip="nota de la respuesta">{{ 0 }}</p></h4>
            @endif
          @endfor

        </td>
      </tr>

    </tbody>
  </table>
</div>
    <h1 class="page-header"></h1>
    <a href="{!! URL::to('seleccion-multiple') !!}" class="btn btn-primary"> Regresar<i class="fa fa-back"></i></a><br><br><br><br>


@include('layouts.scripts')
@include('layouts.scripts-sel-simple-create')
@endsection
