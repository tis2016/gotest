@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Vista Previa de la Pregunta</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
  <div class="row">
    <div class="col-lg-6">
      @include('alerts.success')
      @include('alerts.required')
      <form action="{{ route('respuestaSeleccion.store') }}" method="POST" class="form-horizontal">
        {!! csrf_field() !!}
        <div class="">
          <label for="enunciado" class="control-label text-primary">{{ $pregunta->enunciado }}</label>
        </div>
        <div class="">
          <label for="enunciado" class="control-label ">Seleccione las Respuestas Correctas</label>
          @foreach($pregunta->opciones as $opcion)
          <div class="checkbox">
            <input type="checkbox" name="respuesta[]" value="{{ $opcion->id }}"> {{ $opcion->opcion }}
          </div>
          @endforeach
          {!!Form::hidden('idPregunta', $pregunta->id)!!}
        </div>
        <br>
        <div class="">
          <div class="">
            <button type="submit" class="btn btn-primary"> Añadir Respuesta
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- /.col-lg-8 -->
  <!-- /.col-lg-4 -->
</div>
<!-- /.row -->
{!!Html::script('bower_components/jquery/dist/jquery.min.js')!!}
<!-- Bootstrap Core JavaScript -->
{!!Html::script('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
<!-- Metis Menu Plugin JavaScript -->
{!!Html::script('bower_components/metisMenu/dist/metisMenu.min.js')!!}
<!-- Morris Charts JavaScript -->
{!!Html::script('bower_components/raphael/raphael-min.js')!!}
{!!Html::script('bower_components/morrisjs/morris.min.js')!!}
{!!Html::script('js/morris-data.js')!!}
<!-- Custom Theme JavaScript -->
{!!Html::script('dist/js/sb-admin-2.js')!!}
@endsection