@extends('layouts.principal')
@section('content')
    <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">Registrar pregunta</h1>
      </div>
      <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <!-- /.row -->
  <div class="panel-body">
  <div class="row">
      <div class="col-lg-6">
{!!Form::open(['route'=>'seleccion.store', 'method'=>'POST','files' => true])!!}
{!!Form::hidden('tipo', 'seleccion-simple')!!}
@include('preguntaseleccion.form.pregunta')
{!!Form::close()!!}
@include('layouts.scripts')
@include('layouts.scripts-sel-simple-create')
</div>
</div>
<!-- /.col-lg-8 -->
<!-- /.col-lg-4 -->
</div>
@endsection
