{!!Form::label('categoria','Categoria: ')!!}
{!!Form::select('categorias_id', $categorias,null,['id'=>'categoriaSelMultiple','class'=>'form-control',
								'placeholder'=>'Seleccione una opcion..','required'])!!}<br>
{!!Form::label('subcategoria','Sub-categoria: ')!!}
{!!Form::select('sub_categorias_id',['placeholder'=>'Seleccione una opcion..'],null,['id'=>'subcategoriaSelMultiple',
								'class'=>'form-control'])!!}<br>
{!!Form::label('enunciado','Enunciado: ')!!}
{!!Form::textarea('enunciado',null,['class'=>'form-control', 'rows' => 2,'required']) !!}<br>
@include('alerts.error')
{!!Form::label('opcion','Opciones de respuesta: ')!!}
<div class="form-group option-container">
		<div class="input-group ">
			<span class="input-group-addon">
				{!!Form::select('correcto[]', ['0' => 'Incorrecto', '1' => 'Correcto'],null,['id'=>'correcto_1','required'])!!}
			</span>
				{!!Form::text('opcion[]',null,['class'=>'form-control', 'placeholder'=>'Ingresa una opcion..','min'=>'3','required'])!!}
			<span class="input-group-btn">
					<button class="btn btn-outline btn-danger btn-remove" type="button">X</button>
			</span>
		</div>
</div>
<div class="form-group option-container">
		<div class="input-group ">
			<span class="input-group-addon">
				{!!Form::select('correcto[]', ['0' => 'Incorrecto', '1' => 'Correcto'],null,['id'=>'correcto_2','required'])!!}
			</span>
				{!!Form::text('opcion[]',null,['class'=>'form-control', 'placeholder'=>'Ingresa una opcion..','min'=>'3','required'])!!}
			<span class="input-group-btn">
					<button class="btn btn-outline btn-danger btn-remove" type="button">X</button>
			</span>
		</div>
</div>
			<button type="button" class="btn btn-outline btn-success btn-lg btn-block btn-add-more-options">Agregar opción</button>
			<h1 class="page-header"></h1>
{!!Form::submit('Registrar',['class'=>'btn btn-outline btn-primary'])!!}<br><br><br>
