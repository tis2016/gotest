@extends('layouts.principal')
@section('content')
  <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Registrar pregunta de falso y verdadero</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->

<div class="panel-body">
<div class="row">
    <div class="col-lg-6">

{!!Form::open(['route'=>'falso-verdadero.store', 'method'=>'POST','onsubmit'=>'return ValidationEventFV()','files' => true])!!}
{!!Form::hidden('usuarios_id', Auth::user()->id)!!}
@include('falsoverdadero.form.pregunta')
{!!Form::close()!!}
</div>
</div>
<!-- /.col-lg-8 -->
<!-- /.col-lg-4 -->
</div>
@include('layouts.scripts')
@include('layouts.scripts-sel-simple-create')
<script type="text/javascript">
	$('#select-1').parent().parent().css('width','auto');
</script>
@endsection
