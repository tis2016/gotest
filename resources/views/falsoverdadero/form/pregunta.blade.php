{!!Form::label('categoria','Categoria: ')!!}
{!!Form::select('categorias_id', $categorias,null,['id'=>'categoriaFalsoVerdadero','class'=>'form-control',
								'placeholder'=>'Seleccione una opcion..','required'])!!}<br>
{!!Form::label('subcategoria','Sub-categoria: ')!!}
{!!Form::select('sub_categorias_id',['placeholder'=>'Seleccione una opcion..'],null,['id'=>'subcategoriaFalsoVerdadero',
								'class'=>'form-control'])!!}<br>
{!!Form::label('enunciado','Enunciado: ')!!}
{!!Form::textarea('enunciado',null,['class'=>'form-control', 'rows' => 2,'required','minlength'=>5],['id'=>'enunciado_id']) !!}<br>
@include('alerts.success')
@include('alerts.required')
{!!Form::label('opcion','Opciones de respuesta: ')!!}<br>
<ol><li><div class="form-group option-container">
		<div class="input-group ">
			<div data-role="page" id="home">
    	<div data-role="content">
        <div data-role="controlgroup" data-type="horizontal" data-inline="true">
				{!!Form::select('correcto[]', ['0' => 'Incorrecto', '1' => 'Correcto'],null,['id'=>'correcto_1','data-inline'=>'true','class'=>'form-control','required'])!!}
				{!!Form::select('opcion[]', ['falso' => 'Falso', 'verdadero' => 'Verdadero'],null,['id'=>'opcion_1','data-inline'=>'true','class'=>'form-control','required'])!!}
				</div>
				</div>
				</div>
		</div>
</div></li>
<li><div class="form-group option-container">
		<div class="input-group ">
			<div data-role="page" id="home">
    	<div data-role="content">
        <div data-role="controlgroup" data-type="horizontal" data-inline="true">
				{!!Form::select('correcto[]', ['1' => 'Correcto','0' => 'Incorrecto'],null,['id'=>'correcto_2','data-inline'=>'true','class'=>'form-control'])!!}
				{!!Form::select('opcion[]', ['verdadero' => 'Verdadero','falso' => 'Falso'],null,['id'=>'opcion_2','data-inline'=>'true','class'=>'form-control'])!!}
				</div>
				</div>
				</div>
		</div>
</div></li></ol>
<h1 class="page-header"></h1>
{!!Form::submit('Registrar',['class'=>'btn btn-outline btn-primary'])!!}<br><br><br>
