@extends('layouts.principal')
@section('content')
  <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar pregunta</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
<div class="row">
    <div class="col-lg-8">
        {!!Form::model($pregunta,['route'=>['falso-verdadero.update',$pregunta->id],'method'=>'PUT','onsubmit'=>'return ValidationEvent()','files' => true])!!}
        {!!Form::hidden('tipo', 'falso-verdadero')!!}
        {!!Form::label('categoria','Categoria: ')!!}
        {!!Form::select('categorias_id', $categoria,null,['id'=>'categoriaSelSimple','class'=>'form-control',
        								'placeholder'=>'Seleccione una opcion..','required'])!!}<br>
        {!!Form::label('subcategoria','Sub-categoria: ')!!}
        {!!Form::select('sub_categorias_id', $subcategoria,null,['id'=>'subcategoriaSelSimple','class'=>'form-control',
  											'placeholder'=>'Seleccione una opcion..','required'])!!}<br>
        {!!Form::label('enunciado','Enunciado: ')!!}
        {!!Form::textarea('enunciado',null,['class'=>'form-control', 'rows' => 2,'required']) !!}<br>
        @include('alerts.success')
        @include('alerts.required')
        {!!Form::label('opcion','Opciones de respuesta: ')!!}<br>
        <ol>
        @foreach($opcion as $key => $value)
          {{-- {!!Form::model($value,['route'=>['opciones.update',$value->id],'method'=>'PUT','id'=>'Form'.$a])!!} --}}
          <li><div class="form-group option-container">
            {!!Form::hidden('id_opciones[]', $value->id)!!}
              <div class="input-group ">
                <div data-role="page" id="home">
                <div data-role="content">
                  <div data-role="controlgroup" data-type="horizontal" data-inline="true">
                @if($value->correcto == 1)
                    {!!Form::select('correcto[]', ['1' => 'Correcto','0' => 'Incorrecto'],'default', ['id' => $value->id ,'class'=>'form-control'])!!}
                @else
                    {!!Form::select('correcto[]', ['0' => 'Incorrecto','1' => 'Correcto'],'default', ['id' => $value->id ,'class'=>'form-control'])!!}
                @endif
                @if($value->opcion == "falso")
                  {!!Form::select('opcion[]', ['falso' => 'Falso', 'verdadero' => 'Verdadero'],null,['data-inline'=>'true','class'=>'form-control'])!!}
                @else
                  {!!Form::select('opcion[]', ['verdadero' => 'Verdadero', 'falso' => 'Falso'],null,['data-inline'=>'true','class'=>'form-control'])!!}
                @endif
              </div>
              </div>
          </div>
          </div>
        </div></li>
        @endforeach
      </ol>
        <h1 class="page-header"></h1>
        {!!Form::submit('Actualizar',['class'=>'btn btn-outline btn-primary'])!!}<br><br><br>
        {!!Form::close()!!}
    </div>
</div>
</div>
@include('layouts.scripts')
@endsection
