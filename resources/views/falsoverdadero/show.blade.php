@extends('layouts.principal')
@section('content')
{!!Html::style('css/tooltips.css')!!}
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Detalles de la pregunta</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
	<div class="row">
		<div class="col-lg-10">
			<?php
				$enunciado = '';
				$categoria = '';
				$subcategoria = '';
				$created_at = '';
				$updated_at = '';
				$correcto = '';
				$incorrecto = '';
				$peso = '';
			
			foreach($pregunta as $p){
				$enunciado = $p->enunciado;
				$categoria = $p->categoria;
				$subcategoria = $p->subcategoria;
				$created_at = $p->created_at ;
				$updated_at = $p->updated_at ;
			}
			?>
			<div class="table-responsive table-bordered">
				<table class="table">
					<thead>
						<tr>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><h4><b>Enunciado: </b></h4></td>
							<td><h5>{{ $enunciado }}</h5></td>
						</tr>
						<tr>
							<td><h4><b>Categor&iacute;a: </b></h4></td>
							<td><h5>{{ $categoria }}</h5></td>
						</tr>
						<tr>
							<td><h4><b>Sub-categor&iacute;a: </b></h4></td>
							<td><h5>{{ $subcategoria }}</h5></td>
						</tr>
						<tr>
							<td><h4><b>Fecha de creaci&oacute;n: </b></h4></td>
							<td><h5>{{ $created_at }}</h5></td>
						</tr>
						<tr>
							<td><h4><b>Fecha de modificaci&oacute;n: </b></h4></td>
							<td><h5>{{ $updated_at }}</h5></td>
						</tr>
						<tr>
							<td><h4><b>Opciones de respuesta: </b></h4></td>
							<td>
								@if($opciones[0]->correcto == 1)
								<h4><p class="text-success" data-tooltip="Respuesta correcta">{{ $opciones[0]->opcion }}</p></h4>
								@else
								<h4><p class="text-danger" data-tooltip="Respuesta incorrecta">{{ $opciones[0]->opcion }}</p></h4>
								@endif
								@if($opciones[1]->correcto == 1)
								<h4><p class="text-success" data-tooltip="Respuesta correcta">{{ $opciones[1]->opcion }}</p></h4>
								@else
								<h4><p class="text-danger" data-tooltip="Respuesta incorrecta">{{ $opciones[1]->opcion }}</p></h4>
								@endif
							</td>
						</tr>
						<tr>
							<td><h4><b>Puntaje o valor: </b></h4></td>
							<td><h5><p>100 puntos para la respuesta correcta, 0 puntos para la incorrecta.</p></h5>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<h1 class="page-header"></h1>
		<a href="{!! URL::to('falso-verdadero') !!}" class="btn btn-primary"> Regresar<i class="fa fa-back"></i></a><br><br><br><br>
	</div>
</div>
</div>
@include('layouts.scripts')
@endsection