@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Detalles de su respuesta</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->
<div class="panel-body">
  <div class="row">
    <div class="col-lg-10">
  <?php
    $enunciado = $pregunta[0]->enunciado;
    $categoria = $pregunta[0]->categoria;
    $subcategoria = $pregunta[0]->subcategoria;
    $created_at = $pregunta[0]->created_at;
    $updated_at = $pregunta[0]->updated_at;
    $flag="void";

  ?>
    <div class="table-responsive table-bordered">
      <table class="table">
        <thead>
          <tr>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><h4><b>Enunciado: </b></h4></td>
            <td><h5>{{ $enunciado }}</h5></td>
          </tr>
          <tr>
            <td><h4><b>Categor&iacute;a: </b></h4></td>
            <td><h5>{{ $categoria }}</h5></td>
          </tr>
          <tr>
            <td><h4><b>Sub-categor&iacute;a: </b></h4></td>
            <td><h5>{{ $subcategoria }}</h5></td>
          </tr>
            <td><h4><b>Respuesta: </b></h4></td>
            <td>
              @if($opciones[0]->correcto == 1)
                  <h4><p class="text-success" data-tooltip="Respuesta correcta">{{ $opciones[0]->opcion }}</p></h4>
              @else
                  <h4><p class="text-danger" data-tooltip="Respuesta incorrecta">{{ $opciones[0]->opcion }}</p></h4>
              @endif  
              @if($opciones[1]->correcto == 1)
                <h4><p class="text-success" data-tooltip="Respuesta correcta">{{ $opciones[1]->opcion }}</p></h4>
              @else
                <h4><p class="text-danger" data-tooltip="Respuesta incorrecta">{{ $opciones[1]->opcion }}</p></h4>
              @endif    
            </td>
          </tr>
          <tr>
            <td><h4><b>Puntaje o valor: </b></h4></td>
            <td>
              <h5>100 puntos para la respuesta correcta, 0 puntos para la incorrecta.</h5>
            </td>
          </tr>
          <tr>
            <td><h4><b>Su respuesta: </b></h4></td>
            <td>
                @if($correcto_row[0]->opcion == $respuesta->opcion_elegida)
                  <h4><p class="text-success" data-tooltip="Respuesta correcta">{{ $respuesta->opcion_elegida }}</p></h4>
                  <?php $flag = "correcto";?>
                @else
                  <h4><p class="text-danger" data-tooltip="Respuesta incorrecta">{{ $respuesta->opcion_elegida }}</p></h4>
                    <?php $flag = "incorrecto";?>
                @endif  
                @if("" == $respuesta->opcion_elegida )  
                  <h4><p class="text-danger">Sin respuesta</p></h4>
                @endif         
            </td>
          </tr>
          <tr>
            <td><h4><b>Su puntaje: </b></h4></td>
            <td>
              @if($flag == "correcto")
                  <h4><p class="text-info" data-tooltip="Respuesta correcta">{{ $respuesta->peso }}</p></h4>
                @else
                  <h4><p class="text-danger" data-tooltip="Respuesta incorrecta">{{ $respuesta->peso }}</p></h4>
                @endif 
              
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <h1 class="page-header"></h1>
    <a href="{!! URL::to('falso-verdadero') !!}" class="btn btn-primary"> Regresar<i class="fa fa-back"></i></a><br><br><br><br>
  </div>
      </div>
    </div>
    </div>
@include('layouts.scripts')
@include('layouts.scripts-sel-simple-create')
@endsection