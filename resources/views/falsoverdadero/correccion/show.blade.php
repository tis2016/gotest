@extends('layouts.principal')
@section('content')
  <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Pregunta de falso y verdadero</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- /.row -->

<div class="panel-body">
<div class="row">
    <div class="col-lg-8">

{!!Form::open(['route'=>'correccion-falso-verdadero.store', 'method'=>'POST','onsubmit'=>'return ValidationEventFV()','files' => true])!!}
{!!Form::hidden('pregunta_id', $pregunta->id)!!}
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <b>Pregunta # 1</b>
        </div>
        <div class="panel-body">
        <h3>{!! Form::label('enunciado', $pregunta->enunciado) !!}</h3>
        <div class="row"><br /></div>
        {!! Form::label('respuesta', "Su respuesta:") !!}<br/><br />
        <table align="left">
			<tr>
				<td><h5>{!! Form::label('opcion', 'Falso') !!}</h5></td>
				<td>&nbsp;&nbsp;{!! Form::radio('opcion', 'Falso') !!}</td>
			</tr>
			<tr>
				<td><h5>{!! Form::label('opcion', 'Verdadero') !!}</h5></td>
				<td>&nbsp;&nbsp;{!! Form::radio('opcion', 'Verdadero') !!}</td>
			</tr>
		</table>
        </div>
        <div class="panel-footer" align="right">
          <a href="#" class="btn btn-primary btn-circle" data-tooltip="Atras"><i class="fa fa-chevron-left "></i></a>

          <a href="#" class="btn btn-primary btn-circle" data-tooltip="Adelante"><i class="fa fa-chevron-right"></i></a>

          <a href="{!! URL::to('falso-verdadero') !!}" class="btn btn-warning btn-circle" data-tooltip="Regresar a inicio"><i class="fa fa-home "></i></a>

          <button type="submit" class="btn btn-success btn-circle" data-tooltip="Enviar para corregir"><i class="fa fa-check"></i>
          </button>
          
         
        </div>
    </div>
</div>
{!!Form::close()!!}
<!-- /.col-lg-8 -->
<!-- /.col-lg-4 -->
</div>
    </div>
    </div>
@include('layouts.scripts')
@include('layouts.scripts-sel-simple-create')
@endsection