<?php

use Illuminate\Database\Seeder;

class PreguntasTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('preguntas')->insert([
            'enunciado' =>  'El cielo es verde',
            'id_tipo_pregunta' => 4,
            'created_at' => '2016-07-01',
            'updated_at' => '2016-07-01',
        ]);
    }
}
