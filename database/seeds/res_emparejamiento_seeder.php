<?php

use Illuminate\Database\Seeder;

class res_emparejamiento_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('respuesta_emparejamiento')->truncate();    	
        DB::table('respuesta_emparejamiento')->insert([
            'preguntaorden_id' => '1',
            'primervalor' => 'Blue',
            'segundovalor' => 'Azul',
        ]);
        DB::table('respuesta_emparejamiento')->insert([
            'preguntaorden_id' => '1',
            'primervalor' => 'Red',
            'segundovalor' => 'Rojo',
        ]);
        DB::table('respuesta_emparejamiento')->insert([
            'preguntaorden_id' => '1',
            'primervalor' => 'Yellow',
            'segundovalor' => 'Amarillo',
        ]);
        DB::table('respuesta_emparejamiento')->insert([
            'preguntaorden_id' => '1',
            'primervalor' => 'Black',
            'segundovalor' => 'Negro',
        ]);
    }
}
