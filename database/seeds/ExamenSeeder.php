<?php

use Illuminate\Database\Seeder;

class ExamenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('examenes')->insert([
            'id' => 1
            'titulo' => 'Examen 1',
            'fecha_examen' => '2016-07-16 13:00:00',
            'fecha_inscripcion' => '2016-07-12 13:00:00',
            'duracion' => '15',
            'numero_intentos' => '3'
        ]);
        DB::table('intentos')->insert([
            'id' => 1,
            'id_examen' => 1,
            'fecha_inicio' => '2016-07-16 13:05:00',
        ]);
        DB::table('pregunta_intento')->insert([
            'id' => 1,
            'id_intento' => 1,
            'id_pregunta' => 1
        ]);
        DB::table('respuesta_intento_f_v')->insert([
            'id' => 1,
            'correcto' => false
        ]);
        DB::table('examen_pregunta')->insert([
            'id' => 1,
            'id_pregunta' => 1,
            'id_examen' => 1
        ]);
    }
}
