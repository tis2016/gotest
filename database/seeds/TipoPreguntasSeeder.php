<?php

use Illuminate\Database\Seeder;

class TipoPreguntasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_pregunta')->insert([
            'nombre' => 'ordenamiento',
            'alias' => 'Ordenamiento',
        ]);

        DB::table('tipo_pregunta')->insert([
            'nombre' => 'complementacion',
            'alias' => 'Complementacion',
        ]);
        DB::table('tipo_pregunta')->insert([
            'nombre' => 'abierta',
            'alias' => 'Preguntas Abiertas',
        ]);
        DB::table('tipo_pregunta')->insert([
            'nombre' => 'f_v',
            'alias' => 'Falso y Verdadero',
        ]);
        DB::table('tipo_pregunta')->insert([
            'nombre' => 'simple',
            'alias' => 'Opcion Simple',
        ]);
        DB::table('tipo_pregunta')->insert([
            'nombre' => 'multiple',
            'alias' => 'Opcion Multiple',
        ]);
        
        DB::table('tipo_pregunta')->insert([
            'nombre' => 'emparejamiento',
            'alias' => 'Emparejamiento',
        ]);
    }
}
