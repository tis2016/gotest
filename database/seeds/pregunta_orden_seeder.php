<?php

use Illuminate\Database\Seeder;

class pregunta_orden_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('preguntaorden')->insert([
            'subcategoria_id' => '1',
            'enunciado' => 'Empareja las siguientes palabras con su traduccion al español',
        ]);        
    }
}
