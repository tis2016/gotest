<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        $this->call(TipoPreguntasSeeder::class);
        $this->call(PreguntasTablaSeeder::class);
        $this->call(pregunta_orden_seeder::class);
        $this->call(res_emparejamiento_seeder::class);
    }
}
