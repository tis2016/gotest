<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestaSeleccionMultipleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuesta_sel_multiple', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('opcion_escogida_id');
          $table->string('respuesta');
          $table->timestamps();
          $table->boolean('correcto');
          $table->integer('peso');

          $table->integer('preguntas_seleccion_multiple_id')->unsigned();
          $table->foreign('preguntas_seleccion_multiple_id')->references('id')->on('preguntas_seleccion_multiple')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('respuesta_sel_multiple');
    }
}
