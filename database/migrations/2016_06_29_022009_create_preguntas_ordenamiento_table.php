<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreguntasOrdenamientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preguntas_ordenamiento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('enunciado');
            $table->timestamps();

            $table->integer('categorias_id')->unsigned();
            $table->foreign('categorias_id')->references('id')->on('categorias');
            $table->integer('sub_categorias_id')->unsigned();
            $table->foreign('sub_categorias_id')->references('id')->on('sub_categorias');
            $table->integer('tipos_pregunta_id')->unsigned()->default(5);
            $table->foreign('tipos_pregunta_id')->references('id')->on('tipos_pregunta');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('preguntas_ordenamiento');
    }
}
