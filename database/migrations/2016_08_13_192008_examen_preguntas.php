<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExamenPreguntas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('examen_preguntas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('examen_id');
            $table->string('tipo_pregunta');
            $table->integer('pregunta_id');
            $table->integer('orden');
            $table->integer('valor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
