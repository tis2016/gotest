<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeleccionMultipleOpcionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sel_multiple_opciones', function (Blueprint $table) {
              $table->increments('id');
              $table->string('opcion');
              $table->boolean('correcto')->default(false);
              $table->integer('peso');
              $table->timestamps();

              $table->integer('preguntas_seleccion_multiple_id')->unsigned();
              $table->foreign('preguntas_seleccion_multiple_id')->references('id')->on('preguntas_seleccion_multiple')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sel_multiple_opciones');
    }
}
