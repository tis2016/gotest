<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasEmparejamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('correccion_emparejamiento', function (Blueprint $table) {
          $table->increments('id');

          $table->string('primervalor_respuesta');
          $table->string('segundovalor_respuesta');
          $table->float('peso');
          $table->boolean('respondida');
          $table->integer('preguntaorden_id')->unsigned();
          $table->foreign('preguntaorden_id')->references('id')->on('preguntaorden');


          $table->timestamps();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('correccion_emparejamiento');
    }
}
