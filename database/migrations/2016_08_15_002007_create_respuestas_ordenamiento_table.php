<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasOrdenamientoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas_ordenamiento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('respuesta')->nullable()->default('Sin respuesta');
            $table->integer('peso')->nullable();
            $table->timestamps();

            $table->integer('preguntas_ordenamiento_id')->unsigned();
            $table->foreign('preguntas_ordenamiento_id')->references('id')->on('preguntas_ordenamiento')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('respuestas_ordenamiento');
    }
}
