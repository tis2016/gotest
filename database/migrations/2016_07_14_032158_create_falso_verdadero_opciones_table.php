<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFalsoVerdaderoOpcionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('falso_verdadero_opciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('opcion');
            $table->integer('correcto');
            $table->integer('peso');
            $table->timestamps();

            $table->integer('preguntas_falso_verdadero_id')->unsigned();
            $table->foreign('preguntas_falso_verdadero_id')->references('id')->on('preguntas_falso_verdadero')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('falso_verdadero_opciones');
    }
}
