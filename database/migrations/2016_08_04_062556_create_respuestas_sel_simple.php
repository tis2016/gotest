<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasSelSimple extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas_sel_simple', function (Blueprint $table) {
            $table->increments('id');
            $table->string('opcion_elegida')->nullable();
            $table->integer('correcto');
            $table->integer('peso');
            $table->timestamps();

            $table->integer('preguntas_seleccion_simple_id')->unsigned();
            $table->foreign('preguntas_seleccion_simple_id')->references('id')->on('preguntas_seleccion_simple')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('respuestas_sel_simple');
    }
}
