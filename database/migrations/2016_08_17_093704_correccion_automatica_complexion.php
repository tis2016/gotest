<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CorreccionAutomaticaComplexion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pregunta_complementacion_id')->unsigned();
            $table->foreign('pregunta_complementacion_id')->references('id')->on('pregunta_complementacion');
            $table->string('respuesta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rc_usuario');

    }
}
