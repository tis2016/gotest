<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreguntasExamenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preguntas_examen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('enunciado');
            $table->integer('tipos_pregunta_id')->unsigned();
            $table->foreign('tipos_pregunta_id')->references('id')->on('tipos_pregunta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('preguntas_examen');
    }
}
