<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeleccionSimpleOpcionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
      {
          Schema::create('seleccion_simple_opciones', function (Blueprint $table) {
              $table->increments('id');
              $table->string('opcion');
              $table->boolean('correcto')->default(false);
              $table->integer('peso');
              $table->timestamps();

              $table->integer('preguntas_seleccion_simple_id')->unsigned();
              $table->foreign('preguntas_seleccion_simple_id')->references('id')->on('preguntas_seleccion_simple')
                    ->onDelete('cascade');
          });
      }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seleccion_simple_opciones');
    }
}
