<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Examenes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examenes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('materia');
            $table->timestamp('inicio_examen');
            $table->timestamp('fin_examen');
            $table->timestamp('inicio_inscripcion');
            $table->timestamp('fin_inscripcion');
            $table->integer('porcentaje_total')->unsigned();
            $table->integer('valor_total')->unsigned();
            $table->integer('numero_intentos');
            $table->timestamps();
        });
        Schema::create('examen_pregunta', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_examen')->unsigned();
            $table->foreign('id_examen')->references('id')->on('examenes');
            $table->integer('id_pregunta')->unsigned();
            $table->foreign('id_pregunta')->references('id')->on('preguntas');
            $table->timestamps();
        });
        Schema::create('intentos', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_examen')->unsigned();
            $table->foreign('id_examen')->references('id')->on('examenes');
            $table->timestamp('fecha_inicio');
            $table->timestamps();
        });
        Schema::create('pregunta_intento', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_intento')->unsigned();
            $table->foreign('id_intento')->references('id')->on('intentos');
            $table->integer('id_pregunta')->unsigned();
            $table->foreign('id_pregunta')->references('id')->on('preguntas');
            $table->timestamps();
        });
        Schema::create('respuesta_intento_f_v', function(Blueprint $table){
            $table->increments('id');
            $table->boolean('correcta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::drop('respuesta_intento_f_v');
        Schema::drop('pregunta_intento');
        Schema::drop('intentos');
        Schema::drop('examen_pregunta');
        Schema::drop('examenes');
    }
}
