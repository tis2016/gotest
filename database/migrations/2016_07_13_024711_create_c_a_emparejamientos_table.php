<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCAEmparejamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuesta_emparejamiento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('preguntaorden_id');
            $table->string('primervalor');
            $table->string('segundovalor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('respuesta_emparejamiento');
    }
}
