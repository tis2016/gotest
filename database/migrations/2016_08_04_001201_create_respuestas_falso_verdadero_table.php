<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasFalsoVerdaderoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas_falso_verdadero', function (Blueprint $table) {
            $table->increments('id');
            $table->string('opcion_elegida')->nullable();
            $table->integer('correcto');
            $table->integer('peso');
            $table->timestamps();

            $table->integer('preguntas_falso_verdadero_id')->unsigned();
            $table->foreign('preguntas_falso_verdadero_id')->references('id')->on('preguntas_falso_verdadero')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('respuestas_falso_verdadero');
    }
}
