<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAbiertasTipoPregunta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('preguntas_abiertas', function (Blueprint $table) {
            $table->string('tipo_pregunta')->nullable()->default('Pregunta abierta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preguntas_abiertas', function (Blueprint $table) {
            $table->dropColumn('tipo_pregunta');
        });
    }
}
