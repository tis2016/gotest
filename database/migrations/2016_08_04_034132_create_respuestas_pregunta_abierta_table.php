<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestasPreguntaAbiertaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas_pregunta_abierta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('respuesta')->nullable();
            $table->integer('peso');
            $table->timestamps();

            $table->integer('preguntas_abiertas_id')->unsigned();
            $table->foreign('preguntas_abiertas_id')->references('id')->on('preguntas_abiertas')
                  ->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('respuestas_pregunta_abierta');
    }
}
