<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFalsoVerdaderoTipoPregunta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('preguntas_falso_verdadero', function (Blueprint $table) {
            $table->string('tipo_pregunta')->nullable()->default('Pregunta falso verdadero');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preguntas_falso_verdadero', function (Blueprint $table) {
            $table->dropColumn('tipo_pregunta');
        });
    }
}
