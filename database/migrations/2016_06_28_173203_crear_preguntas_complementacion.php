<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearPreguntasComplementacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('pregunta_complementacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subcategoria_id')->unsigned()->nullable();
            $table->foreign('subcategoria_id')->references('id')->on('sub_categorias');
            $table->text('enunciado');
            $table->timestamps();
            $table->integer('tipos_pregunta_id')->unsigned()->default(6);
            $table->foreign('tipos_pregunta_id')->references('id')->on('tipos_pregunta');
        });
        Schema::create('respuestas_complementacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pregunta_complementacion_id')->unsigned();
            $table->foreign('pregunta_complementacion_id')->references('id')->on('pregunta_complementacion');
            $table->string('respuesta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pregunta_complementacion');
        Schema::drop('respuestas_complementacion');
    }
}
