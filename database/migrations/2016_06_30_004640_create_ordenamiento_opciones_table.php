<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenamientoOpcionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenamiento_opciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('secuencia');
            $table->string('opcion');
            $table->timestamps();

            $table->integer('preguntas_ordenamiento_id')->unsigned();
            $table->foreign('preguntas_ordenamiento_id')->references('id')->on('preguntas_ordenamiento')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ordenamiento_opciones');
    }
}
