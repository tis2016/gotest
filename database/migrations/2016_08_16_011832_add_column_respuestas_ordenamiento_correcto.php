<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRespuestasOrdenamientoCorrecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('respuestas_ordenamiento', function (Blueprint $table) {
            $table->string('correcto')->nullable()->default('Sin respuesta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respuestas_ordenamiento', function (Blueprint $table) {
            $table->dropColumn('correcto');
        });
    }
}
