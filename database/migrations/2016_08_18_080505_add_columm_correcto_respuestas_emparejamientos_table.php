<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColummCorrectoRespuestasEmparejamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('correccion_emparejamiento', function(Blueprint $table){
          $table->boolean('correcto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('correccion_emparejamiento', function(Blueprint $table){
          $table->dropColumn('correcto');
        });
    }
}
