<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearPreguntaOrden extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preguntaorden', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subcategoria_id')->unsigned()->nullable();
            $table->foreign('subcategoria_id')->references('id')->on('sub_categorias');
            $table->text('enunciado');
            $table->timestamps();
            $table->integer('tipos_pregunta_id')->unsigned()->default(7);
            $table->foreign('tipos_pregunta_id')->references('id')->on('tipos_pregunta');
        });
        Schema::create('respuestaorden', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('preguntaorden_id')->unsigned();
            $table->foreign('preguntaorden_id')->references('id')->on('preguntaorden');
            $table->string('primervalor');
            $table->string('segundovalor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('preguntaorden');
        Schema::drop('respuestaorden');

    }
}
